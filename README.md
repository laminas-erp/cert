# Lingana ERP Cert - CertManageMonitor

Eine Laminas MVC Applikation.

**Zertifikat Management & Monitor**

Standalone Applikation und integrierbar in [Lingana ERP](https://linganaerp.de/).

Mitarbeiter Zertifikate erfassen und mit individuell konfigurierten Tabellen/Berichten monitoren.

- individuelle Zertifikate definieren
    - Name und Beschreibung
    - Laufzeit in Monaten für die Gültigkeit
    - Zertifikats-Qualifizierung (z.B. voll oder teilweise bestanden, mit Noten von 1 - 10 oder mit beliebigen Werten)
    - Zertifikats Abhängigkeiten zu anderen Zertifikaten
    - Pflicht für Mitarbeiter Gruppen
    - Warnzeit bis zum Ablauf der Gültigkeit
- individuelle Tabellen/Berichte
    - Tabellen definieren
        - Tabellenüberschrift
        - Spaltenanzahl
        - Spaltenüberschriften
        - Reihenanzahl
        - Reihenüberschriften
        - Sortierungspriorität
        - Tebellenfelder mit Zertifikaten belegen
    - Tabellen zu einem Bericht gruppieren
    - Berichte zeigen
        - in App ansehen
        - als PDF ausgeben
        - als Tabellendokument ausgeben
        - per Email versenden
        - in einem individuellen Intervall per Email versenden
- Gruppierung von
    - Zertifikaten (z.B. Human Factor)
    - Mitarbeitern (z.B. Schweißer)

## Developer

Wird eine neue Zertifikats-Definition erstellt müssen folgende Dinge angepasst werden:

- \Lerp\Cert\View\Helper\Employee\EmployeeCertRel
- \Lerp\Cert\Form\Employee\EmployeeCertRelForm
- \Lerp\Cert\Form\View\Helper\Element\Employee\EmployeeCertRelCertDefValueElement
- /view/lerp/cert//template/employee/employeeCertRelFormCertDefDefValue.phtml

### User Integration eines anderen Systems

You will overwrite the following things and implement their functions:

- \Lerp\Cert\Entity\Common\User
- \Lerp\Cert\Table\Common\UserTable
    - ServiceManager-Key: Lerp\Cert\Table\Common\User
