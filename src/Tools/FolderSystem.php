<?php

namespace Lerp\Cert\Tools;

use Laminas\Log\Logger;

/**
 * Description of FolderSystem
 *
 * @author allapow
 */
class FolderSystem
{

    const fileNamePattern = '/^[a-zA-Z0-9]+[a-zA-Z0-9_-]{0,20}[a-zA-Z0-9]+$/';

    /**
     * @var string  The document root of the underlying filesystem.
     */
    private string $documentRoot;

    /**
     * @var string The first foldername after $this->documentRoot.
     */
    private string $mainFolderRoot;

    private Logger $logger;

    /**
     * full path of the folder
     * computed
     * @var string
     */
    private string $fqfn;

    /**
     * DateTime for the full path
     *
     * @var int Unix timestamp
     */
    private int $datetime;

    private array $pres;
    private string $preSeperator = '_';
    private string $fileExtension;
    private string $filename;

    /**
     *
     * @param \Laminas\Log\Logger $logger
     * @param string $documentRoot The document root of the underlying filesystem.
     * @param string $mainFolderRoot The first foldername after $this->documentRoot.
     * @param int $datetime Unixtime
     */
    public function __construct(Logger $logger, string $documentRoot, string $mainFolderRoot = '', int $datetime = 0)
    {
        $this->logger = $logger;
        $this->documentRoot = $documentRoot;
        $this->mainFolderRoot = $mainFolderRoot;
        $this->datetime = $datetime;
    }

    /**
     *
     * @return string
     * @throws \Exception
     */
    public function computeFqfnDateAndPres(): string
    {
        if (!$this->documentRoot) {
            throw new \Exception('bad method call. documentRoot are not defined');
        }
        $this->fqfn = realpath($this->documentRoot);
        if (!$this->fqfn || !file_exists($this->fqfn) || !is_dir($this->fqfn) || !is_writable($this->fqfn)) {
            throw new \Exception('problem with document_root: ' . $this->documentRoot);
        }
        // mainFolderRoot
        $this->fqfn .= DIRECTORY_SEPARATOR . $this->mainFolderRoot;
        $this->computeAndCheckFolder($this->fqfn);

        // datetime
        if (!date('Ym', $this->datetime)) {
            throw new \Exception('bad method call. datetime makes shit');
        }
        $this->fqfn .= DIRECTORY_SEPARATOR . date('Y', $this->datetime);
        $this->computeAndCheckFolder($this->fqfn);
        $this->fqfn .= DIRECTORY_SEPARATOR . date('m', $this->datetime);
        $this->computeAndCheckFolder($this->fqfn);

        // filename
        $this->filename = $this->datetime;
        if (!empty($this->pres) && is_array($this->pres)) {
            $preCount = 1;
            $this->filename = $this->preSeperator;
            foreach ($this->pres as $pre) {
                $this->filename .= $pre . ($preCount < count($this->pres) ? $this->preSeperator : '');
                $preCount++;
            }
        }

        if (!empty($this->fileExtension)) {
            $this->filename .= '.' . $this->fileExtension;
        }
        $this->fqfn .= DIRECTORY_SEPARATOR . $this->filename;
        return $this->fqfn;
    }

    private function computeAndCheckFolder(string $folderName): void
    {
        if (!is_dir($folderName)) {
            if (!mkdir($folderName, 0755)) {
                $this->logger->log(Logger::ERR, 'problem with mkdir: ' . $folderName);
                throw new \Exception('problem with mkdir: ' . $folderName);
            }
        } else if (!is_writable($folderName)) {
            if (!chmod($folderName, 0755)) {
                $this->logger->log(Logger::ERR, 'problem with chmod: ' . $folderName);
                throw new \Exception('problem with chmod: ' . $folderName);
            }
        }
    }

    /**
     *
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setPres(array $pres)
    {
        $this->pres = $pres;
    }

    public function setDatetime(string $datetime)
    {
        $this->datetime = $datetime;
    }

    public function setFileExtension(string $fileExtension)
    {
        $this->fileExtension = $fileExtension;
    }

    /**
     *
     * @param string $certDocMainFolderRoot
     * @param int $certDocDatetime
     * @param string $certDocFilename
     * @return string
     * @throws \Exception
     */
    public static function computeRelativeFilename(string $certDocMainFolderRoot, int $certDocDatetime, string $certDocFilename): string
    {
        if (empty($certDocDatetime) || !date('Y', $certDocDatetime)) {
            throw new \Exception('problem with $certDocDatetime: ' . $certDocDatetime);
        }
        if (empty($certDocFilename)) {
            throw new \Exception('$certDocFilename is empty');
        }
        $relativeFilename = '';
        if (!empty($certDocMainFolderRoot)) {
            $relativeFilename .= DIRECTORY_SEPARATOR . $certDocMainFolderRoot;
        }

        $relativeFilename .= DIRECTORY_SEPARATOR . date('Y', $certDocDatetime)
            . DIRECTORY_SEPARATOR . date('m', $certDocDatetime)
            . DIRECTORY_SEPARATOR . $certDocFilename;
        return $relativeFilename;
    }

}
