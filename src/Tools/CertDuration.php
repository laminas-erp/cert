<?php

namespace Lerp\Cert\Tools;

class CertDuration
{

    public static function isoStringToUnixtime($timeString): int
    {
        try {
            $dateTime = new \DateTime($timeString);
        } catch (\Exception $ex) {

        }
        return $dateTime->getTimestamp();
    }

    /**
     *
     * @param int $duration Zeitspanne in Monaten
     * @param int $timeOfIssue Timestamp vom Tag der zertifikatsausstellung
     * @return \DateInterval Ist DateInterval->invert == 0 dann ist remainingTime negativ (liegt in der Vergangenheit.
     */
    public static function getRemainingTime(int $duration, int $timeOfIssue): \DateInterval
    {
        try {
            $dateTimeOfIssue = new \DateTime(date('Y-m-d', $timeOfIssue));
            $durationInterval = new \DateInterval('P' . $duration . 'M');
            $resultDateTime = $dateTimeOfIssue->add($durationInterval);
        } catch (\Exception $ex) {

        }
        return $resultDateTime->diff(new \DateTime());
    }

}
