<?php

namespace Lerp\Cert;

use Laminas\EventManager\EventInterface;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Lerp\Cert\Observer\Observer\Concrete\EmployeeCertRelAddObserver;

class Module implements BootstrapListenerInterface
{

    public function onBootstrap(EventInterface $e)
    {
        $application = $e->getApplication();
//        $logger = $application->getServiceManager()->get('logger');
//        $eventManager = $application->getEventManager(); // \Laminas\EventManager\EventManager
//        $sharedEventManager = $eventManager->getSharedManager();
//        $sharedEventManager->attach('EmployeeCertRelListener', \Lerp\Cert\Cert\CertEvent::EVENT_EMPLOYEE_CERT_REL_BEFORE, function($e) use ($logger) {
//            $logger->debug('hier funzts');
//        });
//        $employeeCertRelListener = new Listener\EmployeeCertRelListener();
//        $employeeCertRelListener->setLogger($logger);
//        $employeeCertRelListener = $application->getServiceManager()->get('EmployeeCertRelListener');
        /**
         * http://zend-eventmanager.readthedocs.io/en/latest/migration/removed/
         * so solls auch in ZF3 gehen
         */
//        $employeeCertRelListener->attach($eventManager);
//        $eventManager->trigger(\Lerp\Cert\Cert\CertEvent::EVENT_EMPLOYEE_CERT_REL_BEFORE, null, ['employee_id' => 42]); // hier funzts
//        $eventManager->attachAggregate($employeeCertRelListener); // deprecated, in ZF3 removed

        $observerManager = Observer\ObserverManager::getInstance();
        $employeeCertRelAddObserver = $application->getServiceManager()->get(EmployeeCertRelAddObserver::class);
        $observerManager->attach('employeeCertRel.add', $employeeCertRelAddObserver);
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

}
