<?php

namespace Lerp\Cert\Observer\Observer;

interface ObserverInterface
{
    public function triggered(array $data);
}
