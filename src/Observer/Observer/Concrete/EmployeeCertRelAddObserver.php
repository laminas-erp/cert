<?php

namespace Lerp\Cert\Observer\Observer\Concrete;

use Laminas\Log\Logger;
use Lerp\Cert\Observer\Observer\ObserverInterface;
use Lerp\Cert\Tablex\Cert\CertTablex;

/**
 * EmployeeCertRelAddObserver guckt bei neuen EmployeeCertRels ob diese schon existieren
 * und verschiebt sie gegebenenfalls ins Archiv.
 *
 * Ein EmployeeCertRel existiert bereits wenn in DB.employee_cert_rel ein Datensatz mit der selben
 * employee_cert_rel_cert_id
 * und
 * employee_id
 * existiert.
 *
 * @author allapow
 */
class EmployeeCertRelAddObserver implements ObserverInterface
{

    private Logger $logger;
    private CertTablex $certTablex;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setCertTablex(CertTablex $certTablex)
    {
        $this->certTablex = $certTablex;
    }

    public function triggered(array $data)
    {
        $this->logger->debug(__CLASS__ . ' triggered: ' . print_r($data, true));
        if (empty($data['employee_cert_rel_cert_id']) || empty($data['employee_id'])) {
            return;
        }
        $this->logger->debug(__CLASS__ . ' before delete ');
        if ($this->certTablex->archiveEmployeeCertRel($data['employee_id'], $data['employee_cert_rel_cert_id']) > 0) {
//            $this->certTablex->deleteEmployeeCertRel($data['employee_id'], $data['employee_cert_rel_cert_id']);
        }
    }

}
