<?php

namespace Lerp\Cert\Observer;

use Lerp\Cert\Observer\Observer\ObserverInterface;

/**
 * Bei dem ObserverManager registrieren sich die Observer fuer ein bestimmtes Event,
 * das durch eine Zeichenkette repraesentiert wird.
 *
 * @author allapow
 */
class ObserverManager
{
    private static ObserverManager $instance;

    /**
     * @var ObserverInterface[]
     */
    private array $observers = [];

    public static function getInstance(): ObserverManager
    {
        if (!isset(self::$instance)) {
            self::$instance = new ObserverManager();
        }
        return self::$instance;
    }

    public function attach($event, Observer\ObserverInterface $observer)
    {
        if (!isset($this->observers[$event])) {
            $this->observers[$event] = [];
        }
        if (!isset($this->observers[$event][get_class($observer)])) {
            $this->observers[$event][get_class($observer)] = $observer;
        }
    }

    /**
     *
     * @param string $event
     * @param array $data
     * @return boolean
     */
    public function trigger($event, array $data)
    {
        if (!isset($this->observers[$event]) || empty($this->observers[$event]) || !is_array($this->observers[$event])) {
            return false;
        }
        /**
         * @var string $fqClass
         * @var  ObserverInterface $observer
         */
        foreach ($this->observers[$event] as $fqClass => $observer) {
            if (!$observer instanceof $fqClass) {
                continue;
            }
            $observer->triggered($data);
        }
        return true;
    }
}
