<?php

namespace Lerp\Cert\Factory\Listener;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Listener\EmployeeCertRelListener;
use Psr\Container\ContainerInterface;

class EmployeeCertRelListenerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $l = new EmployeeCertRelListener();
        $l->setLogger($container->get('logger'));
        return $l;
    }
}
