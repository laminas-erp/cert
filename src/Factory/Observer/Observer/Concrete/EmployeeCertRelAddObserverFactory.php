<?php

namespace Lerp\Cert\Factory\Observer\Observer\Concrete;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Observer\Observer\Concrete\EmployeeCertRelAddObserver;
use Lerp\Cert\Tablex\Cert\CertTablex;
use Psr\Container\ContainerInterface;

class EmployeeCertRelAddObserverFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {

        $o = new EmployeeCertRelAddObserver();
        $o->setLogger($container->get('logger'));
        $o->setCertTablex($container->get(CertTablex::class));
        return $o;
    }
}
