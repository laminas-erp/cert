<?php

namespace Lerp\Cert\Factory\Service;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Service\UserService;
use Psr\Container\ContainerInterface;

class UserServiceFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        return new UserService();
    }
}
