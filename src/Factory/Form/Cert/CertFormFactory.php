<?php

namespace Lerp\Cert\Factory\Form\Cert;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Form\Cert\CertForm;
use Lerp\Cert\Table\Cert\CertDefTable;
use Lerp\Cert\Table\Cert\CertGroupTable;

class CertFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $form = new CertForm();
        $form->setLogger($container->get('logger'));
        $certGroupTable = $container->get(CertGroupTable::class);
        $form->setCertGroupsIdAssoc($certGroupTable->getCertGroupsIdAssoc());
        $certDefTable = $container->get(CertDefTable::class);
        $form->setCertDefsIdAssoc($certDefTable->getCertDefsIdAssoc());
        return $form;
    }
}
