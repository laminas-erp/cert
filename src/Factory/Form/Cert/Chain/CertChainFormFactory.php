<?php

namespace Lerp\Cert\Factory\Form\Cert\Chain;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Form\Cert\Chain\CertChainForm;

class CertChainFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $form = new CertChainForm();
        $form->setLogger($container->get('logger'));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setCertChainTypes($toolsTable->getEnumValues('cert_chain', 'cert_chain_type'));
        $form->setCertChainChainTypes($toolsTable->getEnumValues('cert_chain', 'cert_chain_chain_type'));
        return $form;
    }
}
