<?php

namespace Lerp\Cert\Factory\Form\Cert;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Form\Cert\CertTableForm;
use Lerp\Cert\Table\Cert\CertTableGroupTable;

class CertTableFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $form = new CertTableForm();
        $form->setLogger($container->get('logger'));
        $certTableGroupTable = $container->get(CertTableGroupTable::class);
        $form->setCertTableGroupsIdAssoc($certTableGroupTable->getCertTableGroupsIdAssoc());
        return $form;
    }
}
