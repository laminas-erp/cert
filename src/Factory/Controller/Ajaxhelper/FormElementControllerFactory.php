<?php

namespace Lerp\Cert\Factory\Controller\Ajaxhelper;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;
use Lerp\Cert\Controller\Ajaxhelper\FormElementController;

class FormElementControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
	{
		$controller = new FormElementController();
		$controller->setUserService($container->get(UserService::class));
        /** @var HelperPluginManager $viewHelperManager */
        $viewHelperManager = $container->get('ViewHelperManager');
        $controller->setEmployeecertRelViewHelper($viewHelperManager->get('employeeCertDefValueElement'));
		return $controller;
	}
}
