<?php

namespace Lerp\Cert\Factory\Controller\Ajaxhelper;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;
use Lerp\Cert\Controller\Ajaxhelper\FormController;
use Lerp\Cert\Form\Employee\EmployeeCertRelForm;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelDocTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;
use Lerp\Cert\Tablex\Cert\CertTablex;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;

class FormControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $controller = new FormController();
        $controller->setDocPathAbsolute($container->get('config')['lerp_cert']['doc_path_absolute']);
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setCertTablex($container->get(CertTablex::class));
        $controller->setEmployeeCertRelForm($container->get(EmployeeCertRelForm::class));
        $controller->setEmployeeCertRelTable($container->get(EmployeeCertRelTable::class));
        $controller->setEmployeeCertRelDocTable($container->get(EmployeeCertRelDocTable::class));
        $controller->setCertTable($container->get(CertTable::class));
        $controller->setEmployeeTablex($container->get(EmployeeTablex::class));
        /** @var HelperPluginManager $viewHelperManager */
        $viewHelperManager = $container->get('ViewHelperManager');
        $controller->setEmployeecertRelViewHelper($viewHelperManager->get('employeeCertRel'));
        return $controller;
    }
}
