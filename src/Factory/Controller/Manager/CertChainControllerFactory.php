<?php

namespace Lerp\Cert\Factory\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Controller\Manager\CertChainController;
use Lerp\Cert\Form\Cert\Chain\CertChainForm;
use Lerp\Cert\Table\Cert\Chain\CertChainItemTable;
use Lerp\Cert\Table\Cert\Chain\CertChainTable;

class CertChainControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $controller = new CertChainController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setCertChainTable($container->get(CertChainTable::class));
        $controller->setCertChainItemTable($container->get(CertChainItemTable::class));
        $controller->setCertChainForm($container->get(CertChainForm::class));
        return $controller;
    }
}
