<?php

namespace Lerp\Cert\Factory\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Controller\Manager\EmployeeGroupController;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeGroupCertTaskTable;
use Lerp\Cert\Table\Employee\EmployeeGroupRelTable;
use Lerp\Cert\Table\Employee\EmployeeGroupTable;
use Lerp\Cert\Tablex\Employee\EmployeeGroupTablex;

class EmployeeGroupControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $controller = new EmployeeGroupController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setEmployeeGroupCertTaskTable($container->get(EmployeeGroupCertTaskTable::class));
        $controller->setCertTable($container->get(CertTable::class));
        $controller->setEmployeeGroupTable($container->get(EmployeeGroupTable::class));
        $controller->setEmployeeGroupTablex($container->get(EmployeeGroupTablex::class));
        $controller->setEmployeeGroupRelTable($container->get(EmployeeGroupRelTable::class));
        return $controller;
    }
}
