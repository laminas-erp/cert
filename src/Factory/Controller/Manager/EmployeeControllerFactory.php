<?php

namespace Lerp\Cert\Factory\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Controller\Manager\EmployeeController;
use Lerp\Cert\Form\Employee\EmployeeForm;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeCertTableGroupAttributeDefTable;
use Lerp\Cert\Table\Employee\EmployeeTable;
use Lerp\Cert\Tablex\Employee\EmployeeCertTableGroupAttributeTablex;

class EmployeeControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $controller = new EmployeeController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setEmployeeForm($container->get(EmployeeForm::class));
        $controller->setEmployeeTable($container->get(EmployeeTable::class));
        $controller->setCertTable($container->get(CertTable::class));
        $controller->setEmployeeCertTableGroupAttributeTablex($container->get(EmployeeCertTableGroupAttributeTablex::class));
        $controller->setEmployeeCertTableGroupAttributeDefTable($container->get(EmployeeCertTableGroupAttributeDefTable::class));
        return $controller;
    }
}
