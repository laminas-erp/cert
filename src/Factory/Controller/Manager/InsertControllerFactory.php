<?php

namespace Lerp\Cert\Factory\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Controller\Manager\InsertController;
use Lerp\Cert\Form\Cert\CertForm;
use Lerp\Cert\Form\Cert\CertTableForm;
use Lerp\Cert\Form\Cert\CertTableGroupForm;
use Lerp\Cert\Form\Employee\EmployeeCertRelForm;
use Lerp\Cert\Form\Employee\EmployeeForm;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Cert\CertTableGroupTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;
use Lerp\Cert\Table\Employee\EmployeeTable;
use Lerp\Cert\Tablex\Cert\CertTableTablex;

class InsertControllerFactory implements FactoryInterface
{
    /**
     * Create an InsertController
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return InsertController
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): InsertController
    {
        $controller = new InsertController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setCertForm($container->get(CertForm::class));
        $controller->setEmployeeForm($container->get(EmployeeForm::class));
        $controller->setEmployeeTable($container->get(EmployeeTable::class));
        $controller->setEmployeeCertRelForm($container->get(EmployeeCertRelForm::class));
        $controller->setCertTable($container->get(CertTable::class));
        $controller->setEmployeeCertRelTable($container->get(EmployeeCertRelTable::class));
        $controller->setCertTableForm($container->get(CertTableForm::class));
        $controller->setCertTableTablex($container->get(CertTableTablex::class));
        $controller->setCertTableGroupForm($container->get(CertTableGroupForm::class));
        $controller->setCertTableGroupTable($container->get(CertTableGroupTable::class));
        return $controller;
    }
}
