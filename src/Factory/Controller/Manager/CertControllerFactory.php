<?php

namespace Lerp\Cert\Factory\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Controller\Manager\CertController;
use Lerp\Cert\Form\Cert\CertForm;
use Lerp\Cert\Form\Cert\CertTableForm;
use Lerp\Cert\Form\Cert\CertTableGroupForm;
use Lerp\Cert\Table\Cert\CertDefTable;
use Lerp\Cert\Table\Cert\CertGroupTable;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Cert\CertTableGroupTable;
use Lerp\Cert\Table\Cert\CertTableTable;
use Lerp\Cert\Tablex\Cert\CertTableTablex;
use Lerp\Cert\Tablex\Cert\CertTablex;

class CertControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return CertController
     * @throws ServiceNotFoundException if unable to resolve the service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): CertController
    {
        $controller = new CertController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setCertTable($container->get(CertTable::class));
        $controller->setCertTablex($container->get(CertTablex::class));
        $controller->setCertForm($container->get(CertForm::class));
        $controller->setCertTableTable($container->get(CertTableTable::class));
        $controller->setCertTableForm($container->get(CertTableForm::class));
        $controller->setCertTableTablex($container->get(CertTableTablex::class));
        $controller->setCertTableGroupTable($container->get(CertTableGroupTable::class));
        $controller->setCertTableGroupForm($container->get(CertTableGroupForm::class));
        $controller->setCertGroupTable($container->get(CertGroupTable::class));
        $controller->setCertDefTable($container->get(CertDefTable::class));
        return $controller;
    }
}
