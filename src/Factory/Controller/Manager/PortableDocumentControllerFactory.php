<?php

namespace Lerp\Cert\Factory\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Controller\Manager\PortableDocumentController;

class PortableDocumentControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
	{
		$controller = new PortableDocumentController();
        $controller->setLerpCertConfig($container->get('config')['lerp_cert']);
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
		return $controller;
	}
}
