<?php

namespace Lerp\Cert\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;
use Lerp\Cert\Controller\Rest\EmployeeCertRelDocRestController;
use Lerp\Cert\Table\Employee\EmployeeCertRelDocTable;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;
use Lerp\Cert\View\Helper\Employee\EmployeeCertRelViewHelper;

class EmployeeCertRelDocRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $controller = new EmployeeCertRelDocRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setLerpCertConfig($container->get('config')['lerp_cert']);
        $controller->setEmployeeCertRelDocTable($container->get(EmployeeCertRelDocTable::class));
        $controller->setEmployeeTablex($container->get(EmployeeTablex::class));
        /** @var HelperPluginManager $pluginManager */
        $pluginManager = $container->get('ViewHelperManager');
        $controller->setEmployeeCertRelViewHelper($pluginManager->get(EmployeeCertRelViewHelper::class));
        return $controller;
    }
}
