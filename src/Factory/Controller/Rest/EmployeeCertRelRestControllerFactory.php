<?php

namespace Lerp\Cert\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;
use Lerp\Cert\Controller\Rest\EmployeeCertRelRestController;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;
use Lerp\Cert\Tablex\Cert\CertTablex;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;
use Lerp\Cert\View\Helper\Employee\EmployeeCertRelViewHelper;

class EmployeeCertRelRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $controller = new EmployeeCertRelRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        /** @var HelperPluginManager $pluginManager */
        $pluginManager = $container->get('ViewHelperManager');
        $controller->setEmployeeCertRelViewHelper($pluginManager->get(EmployeeCertRelViewHelper::class));
        $controller->setEmployeeCertRelTable($container->get(EmployeeCertRelTable::class));
        $controller->setCertTablex($container->get(CertTablex::class));
        $controller->setEmployeeTablex($container->get(EmployeeTablex::class));
        return $controller;
    }
}
