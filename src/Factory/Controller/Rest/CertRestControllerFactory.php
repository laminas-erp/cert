<?php

namespace Lerp\Cert\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Controller\Rest\CertRestController;
use Lerp\Cert\Tablex\Cert\CertTablex;

class CertRestControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
	{
		$controller = new CertRestController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
		$controller->setCertTablex($container->get(CertTablex::class));
		return $controller;
	}
}
