<?php

namespace Lerp\Cert\Factory\View\Helper\Employee;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelDocTable;
use Lerp\Cert\View\Helper\Employee\EmployeeCertRelViewHelper;

class EmployeeCertRelViewHelperFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $helper = new EmployeeCertRelViewHelper();
        $helper->setLogger($container->get('logger'));
        $helper->setCertTable($container->get(CertTable::class));
        $helper->setEmployeeCertRelDocTable($container->get(EmployeeCertRelDocTable::class));
        return $helper;
    }
}
