<?php

namespace Lerp\Cert\Factory\View\Helper\Element\Cert;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Table\Cert\CertTableGroupTable;
use Lerp\Cert\View\Helper\Element\Cert\CertTableGroupsUrlDropdown;

class CertTableGroupsUrlDropdownFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $helper = new CertTableGroupsUrlDropdown();
        $helper->setCertTableGroupTable($container->get(CertTableGroupTable::class));
        return $helper;
    }
}
