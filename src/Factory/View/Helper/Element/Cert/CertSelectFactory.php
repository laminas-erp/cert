<?php

namespace Lerp\Cert\Factory\View\Helper\Element\Cert;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Table\Cert\CertGroupTable;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\View\Helper\Element\Cert\CertSelect;

class CertSelectFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $helper = new CertSelect();
        $helper->setCertTable($container->get(CertTable::class));
        $helper->setCertGroupTable($container->get(CertGroupTable::class));
        return $helper;
    }
}
