<?php

namespace Lerp\Cert\Factory\View\Helper\Cert;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\View\Helper\Cert\CertDocUrlViewHelper;
use Psr\Container\ContainerInterface;

class CertDocUrlViewHelperFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $helper = new CertDocUrlViewHelper();
        $helper->setLogger($container->get('logger'));
        $helper->setCertConfig($container->get('config')['lerp_cert']);
    }
}
