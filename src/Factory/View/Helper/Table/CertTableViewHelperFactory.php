<?php

namespace Lerp\Cert\Factory\View\Helper\Table;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Cert\Table\Cert\CertTableTable;
use Lerp\Cert\Tablex\Cert\CertTableTablex;
use Lerp\Cert\View\Helper\Table\CertTableViewHelper;

class CertTableViewHelperFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $helper = new CertTableViewHelper();
        $helper->setCertTableTable($container->get(CertTableTable::class));
        $helper->setCertTableTablex($container->get(CertTableTablex::class));
        return $helper;
    }
}
