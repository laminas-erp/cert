<?php

namespace Lerp\Cert\Form\Cert;

use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Logger;

/**
 * Description of CertGroup
 *
 * @author allapow
 */
class CertGroupForm extends Form implements InputFilterProviderInterface
{

    private Logger $logger;

    function __construct()
    {
        parent::__construct('cert_group_form');
        $this->setAttribute('method', 'post');

        $name = new Text('cert_group_name');
        $name->setAttributes([
            'placeholder' => 'the name'
        ]);
        $name->setOptions([
            'label'            => 'Cert Name',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($name);

        $submit = new Submit('submit');
        $submit->setValue('Cert Group speichern');
        $submit->setAttributes([
            'value' => 'Cert Group speichern',
            'class' => 'w3-button',
        ]);
        $this->add($submit);
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function getInputFilterSpecification()
    {
        return [
            'cert_group_name' => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 40,
                        ],
                    ],
                ],
            ],
        ];
    }
}
