<?php

namespace Lerp\Cert\Form\Cert;

use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Exception\InvalidArgumentException;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Logger;

/**
 * Description of CertForm
 *
 * @author allapow
 */
class CertForm extends Form implements InputFilterProviderInterface
{

    private Logger $logger;
    private array $certGroupsIdAssoc;
    private array $certDefsIdAssoc;
    private bool $certIdRequired = true;

    function __construct()
    {
        parent::__construct('cert_form');
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setCertGroupsIdAssoc(array $certGroupsIdAssoc)
    {
        $this->certGroupsIdAssoc = $certGroupsIdAssoc;
    }

    public function setCertDefsIdAssoc(array $certDefsIdAssoc)
    {
        $this->certDefsIdAssoc = $certDefsIdAssoc;
    }

    public function setCertIdRequired(bool $certIdRequired)
    {
        $this->certIdRequired = $certIdRequired;
    }

    public function init()
    {
        $this->setAttribute('method', 'post');

        $certId = new Hidden('cert_id');
        $this->add($certId);

        $duration = new Number('duration');
        $duration->setAttributes([
            'title' => '0=unendlich'
        ]);
        $duration->setValue(0);
        $duration->setOptions([
            'label'            => 'Laufzeit in Monaten',
            'label_attributes' => [
                'class' => 'w3-show-block',
                'title' => '0=unendlich']
        ]);
        $this->add($duration);

        $certGroup = new Select('cert_group_id');
        try {
            $certGroupValueOptions = $this->certGroupsIdAssoc;
            if (!$this->certIdRequired) {
                $certGroupValueOptions[0] = '';
                ksort($certGroupValueOptions);
            }
            $certGroup->setValueOptions($certGroupValueOptions);
        } catch (InvalidArgumentException $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err('Code: ' . $ex->getCode());
            $this->logger->err('File: ' . $ex->getFile());
            $this->logger->err('Line: ' . $ex->getLine());
        }
        $certGroup->setOptions([
            'label'            => 'Zertifikat Gruppe',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($certGroup);

        $certDef = new Select('cert_def_id');
        try {
            $certDefValueOptions = $this->certDefsIdAssoc;
            if (!$this->certIdRequired) {
                $certDefValueOptions[0] = '';
                ksort($certDefValueOptions);
            }
            $certDef->setValueOptions($certDefValueOptions);
        } catch (InvalidArgumentException $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err('Code: ' . $ex->getCode());
            $this->logger->err('File: ' . $ex->getFile());
            $this->logger->err('Line: ' . $ex->getLine());
        }
        $certDef->setOptions([
            'label'            => 'Zertifikat Definition',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($certDef);

        $name = new Text('cert_name');
        $name->setAttributes([
            'placeholder' => 'the name'
        ]);
        $name->setOptions([
            'label'            => 'Zertifikat Name',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($name);

        $desc = new Textarea('cert_desc');
        $desc->setAttributes([
            'placeholder' => 'the description'
        ]);
        $desc->setOptions([
            'label'            => 'Beschreibung',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($desc);

        $taskAll = new Radio('task_all');
        $taskAll->setValueOptions([
            0 => 'nein',
            1 => 'ja'
        ]);
        $taskAll->setChecked(0);
        $taskAll->setOptions([
            'label'            => 'Pflicht alle',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($taskAll);

        $order = new Number('cert_order');
        $order->setAttributes([
            'title' => 'Zur Anzeige in Übersichten'
        ]);
        $order->setValue(0);
        $order->setOptions([
            'label'            => 'order priority',
            'label_attributes' => [
                'class' => 'w3-show-block',
                'title' => 'Zur Anzeige in Übersichten']
        ]);
        $this->add($order);

        $certDefValueVisible = new Radio('cert_def_value_visible');
        $certDefValueVisible->setAttributes([
            'title' => 'In PDFs den Zertifikats-Wert anzeigen'
        ]);
        $certDefValueVisible->setValueOptions([
            0 => 'nein',
            1 => 'ja'
        ]);
        $certDefValueVisible->setChecked(1);
        $certDefValueVisible->setOptions([
            'label'            => 'Zertifikats-Wert sichtbar',
            'label_attributes' => [
                'class' => 'w3-show-block text-align-left',
                'title' => 'In PDFs den Zertifikats-Wert anzeigen']
        ]);
        $this->add($certDefValueVisible);

        $certNameVisible = new Radio('cert_name_visible');
        $certNameVisible->setAttributes([
            'title' => 'In PDFs zusätzlich zum Zertifikats-Wert (aus Zertifikat Definition) den Namen anzeigen'
        ]);
        $certNameVisible->setValueOptions([
            0 => 'nein',
            1 => 'ja'
        ]);
        $certNameVisible->setChecked(0);
        $certNameVisible->setOptions([
            'label'            => 'Name sichtbar',
            'label_attributes' => [
                'class' => 'w3-show-block text-align-left',
                'title' => 'In PDFs zusätzlich zum Zertifikats-Wert (aus Zertifikat Definition) den Namen anzeigen']
        ]);
        $this->add($certNameVisible);

        $certExpirydateVisible = new Radio('cert_expirydate_visible');
        $certExpirydateVisible->setAttributes([
            'title' => 'In PDFs zusätzlich zum Zertifikats-Wert (aus Zertifikat Definition) das Ablaufdatum anzeigen'
        ]);
        $certExpirydateVisible->setValueOptions([
            0 => 'nein',
            1 => 'ja'
        ]);
        $certExpirydateVisible->setChecked(0);
        $certExpirydateVisible->setOptions([
            'label'            => 'Ablaufdatum sichtbar',
            'label_attributes' => [
                'class' => 'w3-show-block text-align-left',
                'title' => 'In PDFs zusätzlich zum Zertifikats-Wert (aus Zertifikat Definition) das Ablaufdatum anzeigen']
        ]);
        $this->add($certExpirydateVisible);

        $certRestrictionVisible = new Radio('cert_restriction_visible');
        $certRestrictionVisible->setAttributes([
            'title' => 'In PDFs zusätzlich zum Zertifikats-Wert (aus Zertifikat Definition) die Einschränkungen anzeigen'
        ]);
        $certRestrictionVisible->setValueOptions([
            0 => 'nein',
            1 => 'ja'
        ]);
        $certRestrictionVisible->setChecked(0);
        $certRestrictionVisible->setOptions([
            'label'            => 'Einschränkungen sichtbar',
            'label_attributes' => [
                'class' => 'w3-show-block text-align-left',
                'title' => 'In PDFs zusätzlich zum Zertifikats-Wert (aus Zertifikat Definition) die Einschränkungen anzeigen']
        ]);
        $this->add($certRestrictionVisible);

        $certValueForEmptyEmployeeCertRel = new Text('cert_value_for_empty_employee_cert_rel');
        $certValueForEmptyEmployeeCertRel->setAttributes([
            'placeholder' => 'z.B. N/A'
        ]);
        $certValueForEmptyEmployeeCertRel->setOptions([
            'label'            => 'Anzeige-Wert wenn leer',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $certValueForEmptyEmployeeCertRel->setValue('-');
        $this->add($certValueForEmptyEmployeeCertRel);

        $submit = new Submit('submit');
        $submit->setValue('Zertifikat speichern');
        $submit->setOptions([
            'label'       => 'Zertifikat speichern',
        ]);
        $this->add($submit);
    }

    public function getInputFilterSpecification()
    {
        return [
            'cert_id'                                => [
                'required'   => $this->certIdRequired,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'duration'                               => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_group_id'                          => [
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->certGroupsIdAssoc),
                        ],
                    ],
                ],
            ],
            'cert_def_id'                            => [
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->certDefsIdAssoc),
                        ],
                    ],
                ],
            ],
            'cert_name'                              => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 100,
                        ],
                    ],
                ],
            ],
            'cert_desc'                              => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max' => 4000,
                        ],
                    ],
                ],
            ],
            'task_all'                               => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_order'                             => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_def_value_visible'                 => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_name_visible'                      => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_expirydate_visible'                => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_restriction_visible'               => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_value_for_empty_employee_cert_rel' => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
//                            'min' => 2,
                            'max' => 40,
                        ],
                    ],
                ],
            ],
        ];
    }

}
