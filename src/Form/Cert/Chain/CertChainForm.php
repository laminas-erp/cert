<?php

namespace Lerp\Cert\Form\Cert\Chain;

use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Exception\InvalidArgumentException;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Logger;

class CertChainForm extends Form implements InputFilterProviderInterface
{

    private Logger $logger;
    private bool $certChainIdRequired = false;
    private array $certChainTypes = [];
    private array $certChainChainTypes = [];

    function __construct()
    {
        parent::__construct('cert_chain_form');
    }

    /**
     *
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function init()
    {

        $this->setAttribute('class', 'w3-container');

        $certId = new Hidden('cert_chain_id');
        $this->add($certId);

        $certChainName = new Text('cert_chain_name');
        $certChainName->setOptions([
            'label' => 'Cert-Chain-Name',
            'class' => 'w3-input',
        ]);
        $this->add($certChainName);

        $certChainDesc = new Textarea('cert_chain_desc');
        $certChainDesc->setOptions([
            'label' => 'Cert-Chain-Beschreibung',
            'class' => 'w3-input',
        ]);
        $this->add($certChainDesc);

        $certChainType = new Select('cert_chain_type');
        try {
            $certChainType->setValueOptions($this->certChainTypes);
        } catch (InvalidArgumentException $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err('Code: ' . $ex->getCode());
            $this->logger->err('File: ' . $ex->getFile());
            $this->logger->err('Line: ' . $ex->getLine());
        }
        $certChainType->setOptions([
            'label' => 'Cert-Chain-Type',
            'class' => 'w3-input',
        ]);
        $this->add($certChainType);

        $certChainChainType = new Select('cert_chain_chain_type');
        try {
            $certChainChainType->setValueOptions($this->certChainChainTypes);
        } catch (InvalidArgumentException $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err('Code: ' . $ex->getCode());
            $this->logger->err('File: ' . $ex->getFile());
            $this->logger->err('Line: ' . $ex->getLine());
        }
        $certChainChainType->setOptions([
            'label' => 'Cert-Chain-ChainType',
            'class' => 'w3-input',
        ]);
        $this->add($certChainChainType);

        $submit = new Submit('submit');
        $submit->setValue('speichern');
        $submit->setOptions([
            'label' => 'speichern',
            'class' => 'w3-input',
        ]);
        $this->add($submit);
    }

    public function getInputFilterSpecification()
    {
        return [
            'cert_chain_id'   => [
                'required'   => $this->certChainIdRequired,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_chain_name' => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 60,
                        ],
                    ],
                ],
            ],
            'cert_chain_desc' => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 500,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     * @param boolean $certChainIdRequired
     */
    public function setCertChainIdRequired(bool $certChainIdRequired): void
    {
        $this->certChainIdRequired = $certChainIdRequired;
    }

    public function setCertChainTypes(array $certChainTypes): void
    {
        foreach ($certChainTypes as $certChainType) {
            $this->certChainTypes[$certChainType] = $certChainType;
        }
    }

    public function setCertChainChainTypes(array $certChainChainTypes): void
    {
        foreach ($certChainChainTypes as $certChainChainType) {
            $this->certChainChainTypes[$certChainChainType] = $certChainChainType;
        }
    }

}
