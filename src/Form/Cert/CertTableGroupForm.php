<?php

namespace Lerp\Cert\Form\Cert;

use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Logger;

/**
 * Description of CertTableGroupForm
 *
 * @author allapow
 */
class CertTableGroupForm extends Form implements InputFilterProviderInterface
{
    private Logger $logger;
    private bool $certTableGroupIdRequired = true;

    function __construct()
    {
        parent::__construct('cert_table_group_form');
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setCertTableGroupIdRequired(bool $certTableIdRequired)
    {
        $this->certTableGroupIdRequired = $certTableIdRequired;
    }

    public function init()
    {
        $this->setAttribute('method', 'post');

        $certId = new Hidden('cert_table_group_id');
        $this->add($certId);

        $certTableName = new Text('cert_table_group_name');
        $certTableName->setOptions([
            'label'            => 'Tabellen-Gruppen-Name',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($certTableName);

        $certTableGroupDesc = new Textarea('cert_table_group_desc');
        $certTableGroupDesc->setOptions([
            'label'            => 'Tabellen-Gruppen-Beschreibung',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($certTableGroupDesc);

        $submit = new Submit('submit');
        $submit->setValue('Tabellengruppe speichern und weiter');
        $submit->setOptions([
            'label'       => 'Tabellengruppe speichern und weiter',
        ]);
        $this->add($submit);
    }

    public function getInputFilterSpecification()
    {
        return [
            'cert_table_group_id'   => [
                'required'   => $this->certTableGroupIdRequired,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_table_group_name' => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 180,
                        ],
                    ],
                ],
            ],
            'cert_table_group_desc' => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 230,
                        ],
                    ],
                ],
            ],
//            'cert_table_group_expiry' => array(
//                'required' => false,
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    new \BitkornLib\Validator\IsoDate()
//                ),
//            ),
        ];
    }

}
