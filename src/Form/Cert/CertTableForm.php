<?php

namespace Lerp\Cert\Form\Cert;

use Bitkorn\Trinket\Validator\CsvCount;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Exception\InvalidArgumentException;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Logger;

/**
 * Description of CertTableForm
 *
 * @author allapow
 */
class CertTableForm extends Form implements InputFilterProviderInterface
{

    private Logger $logger;
    private bool $certTableIdRequired = true;
    private array $certTableGroupsIdAssoc;
    private bool $showDeleteButton = false;

    function __construct()
    {
        parent::__construct('cert_table_form');
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setCertTableIdRequired(bool $certTableIdRequired)
    {
        $this->certTableIdRequired = $certTableIdRequired;
    }

    public function setCertTableGroupsIdAssoc(array $certTableGroupsIdAssoc)
    {
        $this->certTableGroupsIdAssoc = $certTableGroupsIdAssoc;
    }

    public function setShowDeleteButton(bool $showDeleteButton)
    {
        $this->showDeleteButton = $showDeleteButton;
    }

    public function init()
    {
        $this->setAttribute('method', 'post');

        $certId = new Hidden('cert_table_id');
        $this->add($certId);

        $certTableGroup = new Select('cert_table_group_id');
        try {
            $certTableGroupValueOptions = $this->certTableGroupsIdAssoc;
            if (!$this->certTableIdRequired) {
                $certTableGroupValueOptions[0] = '';
                ksort($certTableGroupValueOptions);
            }
            $certTableGroup->setValueOptions($certTableGroupValueOptions);
        } catch (InvalidArgumentException $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err('Code: ' . $ex->getCode());
            $this->logger->err('File: ' . $ex->getFile());
            $this->logger->err('Line: ' . $ex->getLine());
        }
        $certTableGroup->setOptions([
            'label'            => 'Tabellen Gruppe',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($certTableGroup);

        $certTableRowCount = new Number('cert_table_row_count');
        $certTableRowCount->setOptions([
            'label'            => 'Reihen Anzahl',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($certTableRowCount);

        $certTableColumnCount = new Number('cert_table_column_count');
        $certTableColumnCount->setOptions([
            'label'            => 'Spalten Anzahl',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($certTableColumnCount);

        $certTableHeading = new Text('cert_table_heading');
        $certTableHeading->setAttributes([
            'placeholder' => 'hier die Tabellenüberschrift',
        ]);
        $certTableHeading->setOptions([
            'label'            => 'Tabellenüberschrift',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($certTableHeading);

        $certTableOrder = new Number('cert_table_order');
        $certTableOrder->setOptions([
            'label'            => 'Sortierungs-Priorität',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($certTableOrder);

        $certTableRowNames = new Textarea('cert_table_row_names');
        $certTableRowNames->setAttributes([
            'placeholder' => 'CSV; wenn nicht leer, muß die Anzahl CSV Felder mit der Anzahl der Reihen übereinstimmen!',
        ]);
        $certTableRowNames->setOptions([
            'label'            => 'Reihennamen',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($certTableRowNames);

        $certTableColumnNames = new Textarea('cert_table_column_names');
        $certTableColumnNames->setAttributes([
            'placeholder' => 'CSV; wenn nicht leer, muß die Anzahl CSV Felder mit der Anzahl der Spalten übereinstimmen!',
        ]);
        $certTableColumnNames->setOptions([
            'label'            => 'Spaltennamen',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($certTableColumnNames);

        $submit = new Submit('submit');
        $submit->setValue('Tabelle speichern und weiter');
        $submit->setAttributes([
            'class' => 'w3-button cert-color-primary',
        ]);
        $submit->setOptions([
            'label'            => ' ',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($submit);

        if ($this->showDeleteButton) {
            $submitDelete = new Submit('submit_delete');
            $submitDelete->setValue('komplette Tabelle löschen');
            $submitDelete->setAttributes([
                'class' => 'w3-button w3-red',
            ]);
            $submitDelete->setOptions([
                'label'            => ' ',
                'label_attributes' => ['class' => 'w3-show-block'],
            ]);
            $this->add($submitDelete);
        }
    }

    public function getInputFilterSpecification(): array
    {
        return [
            'cert_table_id'           => [
                'required'   => $this->certTableIdRequired,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'cert_table_group_id'     => [
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->certTableGroupsIdAssoc),
                        ],
                    ],
                ],
            ],
            'cert_table_row_count'    => [
                'required'   => true,
                'filters'    => [
//                    array('name' => 'Int'), // macht aus '' (leer) eine 0
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'cert_table_column_count' => [
                'required'   => true,
                'filters'    => [
//                    array('name' => 'Int'), // macht aus '' (leer) eine 0
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'cert_table_heading'      => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
//                    array(
//                        'name' => 'NotEmpty',
//                        'break_chain_on_failure' => TRUE,
//                    ),
[
    'name'    => 'StringLength',
    'options' => [
        'min' => 2,
        'max' => 200,
    ],
],
                ],
            ],
            'cert_table_order'        => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'cert_table_row_names'    => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    //                    array('name' => 'HtmlEntities'), // hiermit müssts auch bei der Ausgabe decodiert werden
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 1,
                            'max' => 2000,
                        ],
                    ],
                    new CsvCount(['countTarget' => $this->get('cert_table_row_count')->getValue()]),
                ],
            ],
            'cert_table_column_names' => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                    //                    array('name' => 'HtmlEntities'), // hiermit müssts auch bei der Ausgabe decodiert werden
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 1,
                            'max' => 2000,
                        ],
                    ],
                    new CsvCount(['countTarget' => $this->get('cert_table_column_count')->getValue()]),
                ],
            ],
        ];
    }

}
