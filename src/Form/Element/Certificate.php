<?php

namespace Lerp\Cert\Form\Element;

use Laminas\Filter\StringTrim;
use Laminas\Form\Element;
use Laminas\InputFilter\InputProviderInterface;
use Laminas\Json\Json;

class Certificate extends Element implements InputProviderInterface
{
    /**
     * the technical certificate definition witch describes the HTML element
     * ...look at /data/example/FormFieldDefinition/foo.json
     * @var Json
     */
    private Json $certificateDefinition;

    public function getInputSpecification()
    {
        $spec = [
            'name'     => $this->getName(),
            'required' => true,
            'filters'  => [
                ['name' => StringTrim::class],
            ],
        ];
//        $spec['validators'] = ['foo'];
        return $spec;
    }

    public function getCertificateDefinition(): Json
    {
        return $this->certificateDefinition;
    }

    public function setCertificateDefinition(Json $certificateDefinition): void
    {
        $this->certificateDefinition = $certificateDefinition;
    }

}
