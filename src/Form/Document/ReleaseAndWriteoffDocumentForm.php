<?php

namespace Lerp\Cert\Form\Document;

use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Exception\InvalidArgumentException;
use Laminas\Form\Form;
use Laminas\Log\Logger;

/**
 * Verarbeitet (gefuellt & gespeichert) wird das hier mit \Lerp\Cert\Entity\Document\ReleaseAndWriteoff
 *
 * Oder die Zertifikate bekommen ein eigenes Form-Element + InputFilter/Validator & Form-View-Helper
 * und der \Lerp\Cert\Entity\Document\ReleaseAndWriteoff filtert und validiert die Daten auch.
 *
 * @author allapow
 */
class ReleaseAndWriteoffDocumentForm extends Form
{

    private Logger $logger;
    private array $employeesIdAssoc;

    function __construct()
    {
        parent::__construct('release_writeoff_doc_form');
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setEmployeesIdAssoc(array $employeesIdAssoc): void
    {
        $this->employeesIdAssoc = $employeesIdAssoc;
    }

    public function init()
    {
        $this->setAttribute('method', 'post');

        $employee = new Select('employee_id');
        try {
            $employee->setValueOptions($this->employeesIdAssoc);
        } catch (InvalidArgumentException $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err('Code: ' . $ex->getCode());
            $this->logger->err('File: ' . $ex->getFile());
            $this->logger->err('Line: ' . $ex->getLine());
        }
        $employee->setOptions([
            'label'            => 'Mitarbeiter',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($employee);

        $dateValid = new Text('date_valid');
        $dateValid->setAttributes([
            'placeholder' => 'TT.MM.YYYY'
        ]);
        $dateValid->setOptions([
            'label'            => 'gültig bis',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($dateValid);

        $dateIssue = new Text('date_issue');
        $dateIssue->setAttributes([
            'placeholder' => 'TT.MM.YYYY'
        ]);
        $dateIssue->setOptions([
            'label'            => 'Ausgabedatum',
            'label_attributes' => ['class' => 'w3-show-block']
        ]);
        $this->add($dateIssue);
    }
}
