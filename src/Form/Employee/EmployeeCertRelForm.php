<?php

namespace Lerp\Cert\Form\Employee;

use Bitkorn\Trinket\Validator\IsoDate;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Exception\InvalidArgumentException;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Logger;

class EmployeeCertRelForm extends Form implements InputFilterProviderInterface
{

    private Logger $logger;
    private bool $employeeCertRelIdRequired = true;

    /**
     *
     * @var boolean Bei einer neuen EmployeeCertRel gibt es noch kein Cert, also FALSE.
     */
    private bool $mustCertIdExist = false;

    /**
     *
     * @var array All Employee IDs to validate the field
     */
    private array $employeeIds;

    /**
     *
     * @var array All Certs to validate the form and fill the select
     */
    private array $certIdsAssoc;

    /**
     * Wird $certData gesetzt ist das entsprechende Select (employee_cert_rel_cert_id) Element vorbelegt und disabled.
     * Bzw. es gibt statt des Selects ein hidden-text-field mit employee_cert_rel_cert_id
     * und ein disabled-text-field mit dem Namen des Zertifikats.
     * @var array
     */
    private array $certData;
    private bool $isCertDefValueFormElement = false;
    private string $employeeCertRelCertDefValue;

    /**
     * Wird $employeeCertRelData gesetzt dann werden von den Form Elementen die values gesetzt.
     * @var array
     */
    private array $employeeCertRelData;

    function __construct()
    {
        parent::__construct('employee_cert_rel_form');
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setEmployeeCertRelIdRequired(bool $employeeCertRelIdRequired): void
    {
        $this->employeeCertRelIdRequired = $employeeCertRelIdRequired;
    }

    public function setEmployeeIds(array $employeeIds): void
    {
        $this->employeeIds = $employeeIds;
    }

    public function setCertIdsAssoc(array $certIdsAssoc): void
    {
        $this->certIdsAssoc = $certIdsAssoc;
    }

    public function setMustCertIdExist(bool $mustCertIdExist): void
    {
        $this->mustCertIdExist = $mustCertIdExist;
    }

    /**
     * MUSS UNBEDINGT VOR init() aufgerufen werden!
     * Wird es NICHT aufgerufen gibt es das Form Element fuer DB.employee_cert_rel.cert_def_value NICHT!
     * Es ist also nur fuer eine Form bei der das Cert schon bekannt ist. Denn dort steht welche cert_def verwendet wird.
     *
     * Z.B. ist dies hier sinnvoll wenn z.B. per AJAX die Form geholt werden soll. Dann kann das hier im Controller gesetzt werden.
     * @param array $certData Cert Daten aus \Lerp\Cert\Tablex\Cert\CertTablex()
     */
    public function setCertData(array $certData): void
    {
        $this->certData = $certData;
    }

    public function setEmployeeCertRelCertDefValue(string $employeeCertRelCertDefValue): void
    {
        $this->employeeCertRelCertDefValue = $employeeCertRelCertDefValue;
    }

    public function setEmployeeCertRelData(array $employeeCertRelData): void
    {
        $this->employeeCertRelData = $employeeCertRelData;
    }

    public function init()
    {
        $this->setAttribute('method', 'post');

        $id = new Hidden('employee_cert_rel_id');
        $id->setValue($this->employeeCertRelData['employee_cert_rel_id'] ?? '');
        $this->add($id);

        $employeeId = new Hidden('employee_id');
        $employeeId->setValue($this->employeeCertRelData['employee_id'] ?? '');
        $this->add($employeeId);

        if (!$this->computeCertDefValueFormElement()) {
            $certId = new Select('employee_cert_rel_cert_id');
            try {
                $certIdValueOptions = $this->certIdsAssoc;
                if (!$this->mustCertIdExist) {
                    $reverse = array_reverse($certIdValueOptions, true);
                    array_push($reverse, [0 => '']);
                    $certIdValueOptions = array_reverse($reverse, true);
                }
                $certId->setValueOptions($certIdValueOptions);
                $certId->setValue($this->employeeCertRelData['employee_cert_rel_cert_id'] ?? '');
            } catch (InvalidArgumentException $ex) {
                $this->logger->err($ex->getMessage());
                $this->logger->err('Code: ' . $ex->getCode());
                $this->logger->err('File: ' . $ex->getFile());
                $this->logger->err('Line: ' . $ex->getLine());
            }
            $certId->setOptions([
                'label'            => 'Zertifikat',
                'column-size'      => 'sm-6',
                'label_attributes' => ['class' => 'w3-show-block'],
            ]);
            $this->add($certId, ['name' => 'employee_cert_rel_cert_id', 'priority' => 500]);
        }

        $dateOfIssue = new Text('date_of_issue');
        $dateOfIssue->setAttribute('class', 'datepicker');
        $dateOfIssue->setOptions([
            'label'            => 'Datum',
            'column-size'      => 'sm-6',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        if (!empty($this->employeeCertRelData['date_of_issue'])) {
            $dateOfIssue->setValue(date('Y-m-d', intval($this->employeeCertRelData['date_of_issue'])));
        }
        $this->add($dateOfIssue, ['name' => 'date_of_issue', 'priority' => 400]);

        $licenceNo = new Text('licence_no');
        $licenceNo->setOptions([
            'label'            => 'Lizenz Nr.',
            'column-size'      => 'sm-6',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $licenceNo->setValue($this->employeeCertRelData['licence_no'] ?? '');
        $this->add($licenceNo);

        $certRestriction = new Text('cert_restriction');
        $certRestriction->setOptions([
            'label'            => 'Einschränkungen',
            'column-size'      => 'sm-6',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $certRestriction->setValue($this->employeeCertRelData['cert_restriction'] ?? 'keine');
        $this->add($certRestriction);

        if (!$this->isCertDefValueFormElement) {
            $submit = new Submit('submit');
            $submit->setValue('Mitarbeiter Zertifikat speichern');
            $submit->setOptions([
                'label'       => 'Mitarbeiter Zertifikat speichern',
                'column-size' => 'sm-6 col-sm-offset-3',
            ]);
            $this->add($submit, ['name' => 'submit', 'priority' => -200]);
        }
    }

    private array $inputFilterSpecificationCertDefValueFormElement = [];

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification(): array
    {
        return [
            'employee_cert_rel_id'      => [
                'required'   => $this->employeeCertRelIdRequired,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'employee_id'               => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => $this->employeeIds,
                        ],
                    ],
                ],
            ],
            'employee_cert_rel_cert_id' => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => array_keys($this->certIdsAssoc),
                        ],
                    ],
                ],
            ],
            'date_of_issue'             => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    new IsoDate(),
                ],
            ],
            'licence_no'                => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 30,
                        ],
                    ],
                ],
            ],
            'cert_restriction'          => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 200,
                        ],
                    ],
                ],
            ],
            $this->inputFilterSpecificationCertDefValueFormElement,
        ];
    }

    /**
     * Wenn das Cert bei Erstellung der Form bekannt ist wird hier:
     * 1. Form Element Hidden: employee_cert_rel_cert_id
     * 2. Form Element Text: cert_name_disabled (disabled und mit value vorbelegt)
     * 3. Form Element je nach type des certs: cert_def_value
     *
     * Verwendung in /manage-employee-certs/1/ (die Ansicht mit allen certRels eines employee).
     * Dort gibt es fuer jedes certRel einen Button (entweder add oder replace etc.).
     *
     * NICHT verwendet wird das hier in /insert-employee-cert-rel/1/ (die Ansicht wo man fuer ein employee ein beliebiges certRel erstellen kann ).
     * Dort wird nach Auswahl des certs dynamisch (AJAX) das Form Element cert_def_value nachgeladen.
     * Template des dort verwendeten ViewHelper: /view/lerp/cert/template/employee/employeeCertRelFormCertDefDefValue.phtml
     *
     * Das Element cert_def_value MUSS auch die ID cert_def_value haben. Denn damit holt sich das JavaScript den value wert!!!
     * @return boolean
     * @throws \RuntimeException
     */
    private function computeCertDefValueFormElement()
    {
        if (!empty($this->certData)) {
            $certDefDef = json_decode($this->certData['cert_def_def'], true);
            if (null === $certDefDef || !is_array($certDefDef)) {
                throw new \RuntimeException('cert_def_def ist kein JSON');
            }
            $this->isCertDefValueFormElement = true;

            $certId = new Hidden('employee_cert_rel_cert_id');
            $certId->setValue($this->certData['cert_id']);
            $this->add($certId);

            $certName = new Text('cert_name_disabled');
            $certName->setOptions([
                'label'            => 'Zertifikat',
                'column-size'      => 'sm-6',
                'label_attributes' => ['class' => 'w3-show-block'],
            ]);
            $certName->setAttributes(['disabled' => 'disabled']);
            $certName->setValue($this->certData['cert_name']);
            $this->add($certName, ['name' => 'cert_name_disabled', 'priority' => 500]);

            $type = $certDefDef['type'];
            switch ($type) {
                case 'select':
                    $certDefValue = new Select('cert_def_value');
                    try {
                        $certDefValue->setValueOptions($certDefDef['values']);
                    } catch (InvalidArgumentException $ex) {
                        $this->logger->err($ex->getMessage());
                        $this->logger->err('Code: ' . $ex->getCode());
                        $this->logger->err('File: ' . $ex->getFile());
                        $this->logger->err('Line: ' . $ex->getLine());
                    }
                    $certDefValue->setOptions([
                        'label'            => 'Zertifikat Wert',
                        'column-size'      => 'sm-6',
                        'label_attributes' => ['class' => 'w3-show-block'],
                    ]);
                    $certDefValue->setAttributes(['id' => 'cert_def_value']);
                    if ($this->employeeCertRelCertDefValue) {
                        $certDefValue->setValue($this->employeeCertRelCertDefValue);
                    }
                    $this->add($certDefValue, ['name' => 'cert_def_value', 'priority' => 450]);

                    $this->inputFilterSpecificationCertDefValueFormElement = [
                        'cert_def_value' => [
                            'required'   => true,
                            'filters'    => [
                                ['name' => 'Int'],
                            ],
                            'validators' => [
                                ['name' => 'Digits'],
                                [
                                    'name'    => 'InArray',
                                    'options' => [
                                        'haystack' => array_keys($certDefDef['values']),
                                    ],
                                ],
                            ],
                        ]];
                    break;
                case 'checkbox':
                    $certDefValue = new Hidden('cert_def_value');
                    $certDefValue->setAttributes(['id' => 'cert_def_value']);
                    $certDefValue->setValue($certDefDef['value']);
                    $this->add($certDefValue);

                    $certDefValueDisabled = new Text('cert_def_value_disabled');
                    $certDefValueDisabled->setOptions([
                        'label'            => 'Zertifikat Wert',
                        'column-size'      => 'sm-6',
                        'label_attributes' => ['class' => 'w3-show-block'],
                    ]);
                    $certDefValueDisabled->setAttributes(['disabled' => 'disabled']);
                    $certDefValueDisabled->setValue($certDefDef['value']);
                    $this->add($certDefValueDisabled, ['name' => 'cert_def_value_disabled', 'priority' => 450]);

                    $this->inputFilterSpecificationCertDefValueFormElement = [
                        'cert_def_value' => [
                            'required'   => true,
                            'filters'    => [
                                ['name' => 'StripTags'],
                                ['name' => 'StringTrim'],
                            ],
                            'validators' => [
                                [
                                    'name'                   => 'NotEmpty',
                                    'break_chain_on_failure' => true,
                                ],
                                [
                                    'name'    => 'StringLength',
                                    'options' => [
                                        'min' => 1,
                                        'max' => 20,
                                    ],
                                ],
                            ],
                        ]];
                    break;
                case 'text':
                    $certDefValue = new Text('cert_def_value');
                    $certDefValue->setOptions([
                        'label'            => 'Zertifikat Wert',
                        'column-size'      => 'sm-6',
                        'label_attributes' => ['class' => 'w3-show-block'],
                    ]);
                    $certDefValue->setAttributes(['id' => 'cert_def_value']);
                    if ($this->employeeCertRelCertDefValue) {
                        $certDefValue->setValue($this->employeeCertRelCertDefValue);
                    }
                    $this->add($certDefValue, ['name' => 'cert_def_value', 'priority' => 450]);

                    $this->inputFilterSpecificationCertDefValueFormElement = [
                        'cert_def_value' => [
                            'required'   => true,
                            'filters'    => [
                                ['name' => 'StripTags'],
                                ['name' => 'StringTrim'],
                            ],
                            'validators' => [
                                [
                                    'name'                   => 'NotEmpty',
                                    'break_chain_on_failure' => true,
                                ],
                                [
                                    'name'    => 'StringLength',
                                    'options' => [
                                        'min' => 1,
                                        'max' => 200,
                                    ],
                                ],
                            ],
                        ]];
                    break;
            }
            return true;
        }
        return false;
    }

    public function setData($data): EmployeeCertRelForm
    {
        $newData = [];
        foreach ($data as $key => $value) {
            if (!empty($key) && !empty($data[$key])) {
                $newData[$key] = $value;
            }
        }
        if (!empty($newData['employee_cert_rel_cert_id'])) {
            $certDefValue = new Text('cert_def_value');
            $certDefValue->setOptions([
                'label'            => 'Zertifikat Wert',
                'column-size'      => 'sm-6',
                'label_attributes' => ['class' => 'w3-show-block'],
            ]);
            $certDefValue->setAttributes(['id' => 'cert_def_value']);
            if ($newData['cert_def_value']) {
                // wird von parent gesetzt
//                $certDefValue->setValue($newData['cert_def_value']);
            }
            $this->add($certDefValue, ['name' => 'cert_def_value', 'priority' => 450]);

            $this->inputFilterSpecificationCertDefValueFormElement = [
                'cert_def_value' => [
                    'required'   => true,
                    'filters'    => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name'                   => 'NotEmpty',
                            'break_chain_on_failure' => true,
                        ],
                        [
                            'name'    => 'StringLength',
                            'options' => [
                                'min' => 1,
                                'max' => 200,
                            ],
                        ],
                    ],
                ]];
        }
        return parent::setData($newData);
    }

}
