<?php

namespace Lerp\Cert\Form\Employee;

use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Logger;

class EmployeeForm extends Form implements InputFilterProviderInterface
{

    private Logger $logger;

    function __construct()
    {
        parent::__construct('employee_form');
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function init()
    {
        $this->setAttribute('method', 'post');

        $id = new Hidden('employee_id');
        $this->add($id);

        $name1 = new Text('name_1');
        $name1->setAttributes([
            'placeholder' => 'Max',
        ]);
        $name1->setOptions([
            'label'            => 'Vorname',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($name1);

        $name2 = new Text('name_2');
        $name2->setAttributes([
            'placeholder' => 'Mustermann',
        ]);
        $name2->setOptions([
            'label'            => 'Nachname',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($name2);

        $noTimerec = new Text('no_timerec');
        $noTimerec->setAttributes([
            'placeholder' => '12345',
        ]);
        $noTimerec->setOptions([
            'label'            => 'Nr. Zeiterfassung',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($noTimerec);

        $noGodata = new Text('no_godata_id');
        $noGodata->setAttributes([
            'placeholder' => '1234',
            'title'       => 'Adminkenntnisse',
            'id'          => 'no_godata_id', // regarding autocomplete
        ]);
        $noGodata->setOptions([
            'label'            => 'Godata ID',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($noGodata);

        $noAddison = new Text('no_addison');
        $noAddison->setAttributes([
            'placeholder' => '12345',
        ]);
        $noAddison->setOptions([
            'label'            => 'Nr. Addison FiBu',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($noAddison);

        $noExtern = new Text('no_extern');
        $noExtern->setAttributes([
            'placeholder' => '1234',
            'title'       => '',
        ]);
        $noExtern->setOptions([
            'label'            => 'Extern ID (ohne \'EXT\')',
            'label_attributes' => ['class' => 'w3-show-block'],
        ]);
        $this->add($noExtern);

        $isActive = new Radio('is_active');
        $isActive->setValueOptions([
            0 => 'nein',
            1 => 'ja',
        ]);
        $isActive->setChecked(0);
        $isActive->setOptions([
            'label'            => 'ist aktiv',
            'label_attributes' => ['class' => 'w3-show-block text-align-left'],
        ]);
        $this->add($isActive);

        $submit = new Submit('submit');
        $submit->setValue('Mitarbeiter speichern');
        $submit->setOptions([
            'label' => 'Mitarbeiter speichern',
        ]);
        $this->add($submit);
    }

    public function getInputFilterSpecification(): array
    {
        return [
            'name_1'       => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 80,
                        ],
                    ],
                ],
            ],
            'name_2'       => [
                'required'   => true,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 80,
                        ],
                    ],
                ],
            ],
            'no_timerec'   => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 30,
                        ],
                    ],
                ],
            ],
            'no_godata_id' => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            'no_addison'   => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 2,
                            'max' => 30,
                        ],
                    ],
                ],
            ],
            'no_extern'    => [
                'required'   => false,
                'filters'    => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
        ];
    }
}
