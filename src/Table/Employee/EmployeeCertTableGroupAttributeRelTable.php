<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Exception\InvalidArgumentException;

class EmployeeCertTableGroupAttributeRelTable extends AbstractLibTable
{

    protected $table = 'employee_cert_table_group_attribute_rel';

    public function getEmployeeCertTableGroupAttributeRels()
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $employeeCertTableGroupAttributeDefId
     * @param int $employeeId
     * @param string $employeeCertTableGroupAttributeValue
     * @return int
     */
    public function insertEmployeeCertTableGroupAttributeRel($employeeCertTableGroupAttributeDefId, $employeeId, $employeeCertTableGroupAttributeValue): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'employee_cert_table_group_attribute_def_id' => $employeeCertTableGroupAttributeDefId,
                'employee_id'                                => $employeeId,
                'employee_cert_table_group_attribute_value'  => $employeeCertTableGroupAttributeValue
            ]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->insertWith($insert);
    }

    /**
     *
     * @param int $employeeCertTableGroupAttributeDefId
     * @param int $employeeId
     * @param string $employeeCertTableGroupAttributeValue
     * @return int
     */
    public function updateEmployeeCertTableGroupAttributeRel($employeeCertTableGroupAttributeDefId, $employeeId, $employeeCertTableGroupAttributeValue): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['employee_cert_table_group_attribute_value' => $employeeCertTableGroupAttributeValue]);
            $update->where([
                'employee_cert_table_group_attribute_def_id' => $employeeCertTableGroupAttributeDefId,
                'employee_id'                                => $employeeId
            ]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->updateWith($update);
    }

}
