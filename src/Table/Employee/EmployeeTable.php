<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;

class EmployeeTable extends AbstractLibTable
{

    protected $table = 'employee';

    public function getEmployeeById($employeeId)
    {
        $select = $this->sql->select();
        $select->where([
            'employee_id' => $employeeId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $resultArray = $result->toArray();
            return $resultArray[0];
        }
        return false;
    }

    public function getEmployees()
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
            if ($result->valid()) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getEmployeeIds(): array
    {
        $select = $this->sql->select();
        $select->columns(['employee_id']);
        try {
            $result = $this->selectWith($select);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        if ($result->valid()) {
            $resultArr = $result->toArray();
            $ids = [];
            foreach ($resultArr as $employeeId) {
                $ids[] = $employeeId['employee_id'];
            }
            return $ids;
        }

        return [];
    }

    /**
     *
     * @param array $data
     * @return int
     */
    public function insertEmployee(array $data): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->insertWith($insert);
    }

    public function updateEmployee(array $data): int
    {
        $update = $this->sql->update();
        try {
            $update->set($data);
            $update->where(['employee_id' => $data['employee_id']]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->updateWith($update);
    }

    public function getEmployeesIdAssoc(): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $idAssoc[$row['employee_id']] = $row;
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
