<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;

class EmployeeCertTableGroupAttributeDefTable extends AbstractLibTable
{

    protected $table = 'employee_cert_table_group_attribute_def';

    public function getEmployeeCertTableGroupAttributeDefs()
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getEmployeeCertTableGroupAttributeDefsForTableGroup($certTableGroupId)
    {
        $select = $this->sql->select();
        try {
            $select->where(['cert_table_group_id' => $certTableGroupId]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return ['x' => 4];
    }

}
