<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;

class EmployeeCertRelArchiveTable extends AbstractLibTable
{
    protected $table = 'employee_cert_rel_archive';

    public function getEmployeeCertRelById($employeeCertRelId)
    {
        $select = $this->sql->select();
        $select->where([
            'employee_cert_rel_id' => $employeeCertRelId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $resultArray = $result->toArray();
            return $resultArray[0];
        }
        return false;
    }

    public function getEmployeeCertRels()
    {
        $select = $this->sql->select();
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    /**
     *
     * @param array $data
     * @return int
     */
    public function insertEmployeeCertRel(array $data): int
    {
        $insert = $this->sql->insert();
        try {
//            $this->logger->debug('insertEmployeeCertRel Data: ' . print_r($data, true));
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->insertWith($insert);
    }
}
