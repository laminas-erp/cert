<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;

class EmployeeCertRelDocTable extends AbstractLibTable
{

    protected $table = 'employee_cert_rel_doc';

    public function getEmployeeCertRelDocById($employeeCertRelDocId)
    {
        $select = $this->sql->select();
        $select->where([
            'employee_cert_rel_doc_id' => $employeeCertRelDocId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            return $result->current();
        }
        return [];
    }

    public function getEmployeeCertRelDocsByEmployeeCertRelId($employeeCertRelId)
    {
        $select = $this->sql->select();
        $select->where([
            'employee_cert_rel_id' => $employeeCertRelId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() > 0) {
            return $result->toArray();
        }
        return [];
    }

    public function countEmployeeCertRelDocsByEmployeeCertRelId($employeeCertRelId)
    {
        $select = $this->sql->select();
        $select->columns([new \Laminas\Db\Sql\Expression('COUNT(employee_cert_rel_doc_id) AS employeeCertRelDocCount')]);
        $select->where([
            'employee_cert_rel_id' => $employeeCertRelId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $result = $result->current();
            return $result['employeeCertRelDocCount'];
        }
        return [];
    }

    public function getCertDocs($order = [])
    {
        $select = $this->sql->select();
        $select->order($order);
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    public function getCertDocsIdAssoc()
    {
        $select = $this->sql->select();
        $select->columns(['employee_cert_rel_doc_id', 'employee_cert_rel_doc_name']);
        try {
            $result = $this->selectWith($select);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $cert) {
                $idAssoc[$cert['employee_cert_rel_doc_id']] = $cert['employee_cert_rel_doc_name'];
            }
            return $idAssoc;
        }
        return [];
    }

    /**
     *
     * @param array $data
     * @return int
     */
    public function insertCertDoc(array $data): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
            $this->insertWith($insert);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    public function updateCertDoc(array $data): int
    {
        $update = $this->sql->update();
        try {
            $update->set($data);
            $update->where(['employee_cert_rel_doc_id' => $data['employee_cert_rel_doc_id']]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->updateWith($update);
    }

    public function deleteEmployeecertRelDocById($employeeCertRelDoc)
    {
        $delete = $this->sql->delete();
        $delete->where(['employee_cert_rel_doc_id' => $employeeCertRelDoc]);
        return $this->deleteWith($delete);
    }

}
