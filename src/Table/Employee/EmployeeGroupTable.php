<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;

class EmployeeGroupTable extends AbstractLibTable
{

    protected $table = 'employee_group';

    public function getEmployeeGroupById($employeeGroupId)
    {
        $select = $this->sql->select();
        $select->where([
            'employee_group_id' => $employeeGroupId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $resultArray = $result->toArray();
            return $resultArray[0];
        }
        return false;
    }

    public function getEmployeeGroups()
    {
        $select = $this->sql->select();
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    /**
     *
     * @param array $data
     * @return int
     */
    public function insertEmployeeGroup(array $data): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->insertWith($insert);
    }

    public function getEmployeeGroupsIdAssoc(): array
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $employeeGroup) {
                $idAssoc[$employeeGroup['employee_group_id']] = $employeeGroup['employee_group_name'];
            }
            return $idAssoc;
        }
        return [];
    }

}
