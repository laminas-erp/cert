<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;

class EmployeeGroupRelTable extends AbstractLibTable
{

    protected $table = 'employee_group_rel';

    public function getEmployeeGroupRelById($employeeGroupRelId)
    {
        $select = $this->sql->select();
        $select->where([
            'employee_group_rel_id' => $employeeGroupRelId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $resultArray = $result->toArray();
            return $resultArray[0];
        }
        return [];
    }

    /**
     *
     * @param int $employeeGroupId
     * @param int $employeeId
     * @return boolean
     */
    public function existEmployeeGroupRel(int $employeeGroupId, int $employeeId): bool
    {
        $select = $this->sql->select();
        $select->where([
            'employee_group_id' => $employeeGroupId,
            'employee_id'       => $employeeId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() > 0) {
            return true;
        }
        return false;
    }

    public function getEmployeeGroupRelsByEmployeeGroupId($employeeGroupId)
    {
        $select = $this->sql->select();
        $select->where([
            'employee_group_id' => $employeeGroupId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() > 0) {
            $resultArray = $result->toArray();
            return $resultArray;
        }
        return [];
    }

    public function getEmployeeGroupRels()
    {
        $select = $this->sql->select();
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    /**
     *
     * @param array $data
     * @return int
     */
    public function insertEmployeeGroupRel(array $data): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->insertWith($insert);
    }

    public function deleteEmployeeGroupRel($employeeGroupId, $employeeId): int
    {
        $delete = $this->sql->delete();
        $delete->where([
            'employee_group_id' => $employeeGroupId,
            'employee_id'       => $employeeId
        ]);
        return $this->deleteWith($delete);
    }
}
