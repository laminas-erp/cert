<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Sql\Expression;

class EmployeeCertRelTable extends AbstractLibTable
{

    protected $table = 'employee_cert_rel';

    public function getEmployeeCertRelById($employeeCertRelId)
    {
        $select = $this->sql->select();
        $select->where([
            'employee_cert_rel_id' => $employeeCertRelId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $resultArray = $result->toArray();
            return $resultArray[0];
        }
        return false;
    }

    public function getEmployeeCertRels()
    {
        $select = $this->sql->select();
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    /**
     *
     * @param array $data
     * @return int
     */
    public function insertEmployeeCertRel(array $data): int
    {
        $insert = $this->sql->insert();
        if ($this->existEmployeeCertRel($data['employee_id'], $data['employee_cert_rel_cert_id'])) {
            // archive this EmployeeCertRel
            // macht jetzt \Lerp\Cert\Observer\Observer\Concrete\EmployeeCertRelAddObserver
        }
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->insertWith($insert);
    }

    public function updateEmployeeCertRel(array $data)
    {
        $update = $this->sql->update();
        try {
//            $this->logger->debug(__CLASS__ . ' ' . __FUNCTION__ . ' $data:' . print_r($data, true));
            $update->set($data);
            $update->where(['employee_cert_rel_id' => $data['employee_cert_rel_id']]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->updateWith($update);
    }

    /**
     *
     * @param int $employeeId
     * @param int $certId
     * @return boolean
     */
    public function existEmployeeCertRel(int $employeeId, int $certId): bool
    {
        $select = $this->sql->select();
        $select->where([
            'employee_id'               => $employeeId,
            'employee_cert_rel_cert_id' => $certId
        ]);
        try {
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * Mit array_exist() kann hiermit leicht festgestellt werden ob ein Employee ein bestimmtes Cert hat.
     * Mit $certIds kann die Abfrage z.B. auf Certs beschraenkt werden, die fuer alle employees Pflicht sind.
     * @param array $certIds Die certIds fuer das SQL WHERE IN.
     * @return array AssocArray with key: certId_employeeId
     */
    public function getEmployeeCertRelIdAssocExt(array $certIds = []): array
    {
        $select = $this->sql->select();
        $select->order('date_of_issue ASC');
        try {
            $select->columns([new Expression('CONCAT_WS(\'_\', employee_cert_rel_cert_id, employee_id) AS cid_eid'), '*']);
            if (!empty($certIds)) {
                $select->where->in('employee_cert_rel_cert_id', $certIds);
            }
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                $resultArr = $result->toArray();
                $idAssoc = [];
                foreach ($resultArr as $certRel) {
                    $idAssoc[$certRel['cid_eid']] = $certRel;
                }
                return $idAssoc;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

}
