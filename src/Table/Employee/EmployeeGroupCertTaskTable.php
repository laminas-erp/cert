<?php

namespace Lerp\Cert\Table\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;

class EmployeeGroupCertTaskTable extends AbstractLibTable
{

    protected $table = 'employee_group_cert_task';

    public function getEmployeeGroupCertTasks()
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $employeeGroupCertTaskId
     * @return array
     */
    public function getEmployeeGroupCertTaskById(int $employeeGroupCertTaskId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'employee_group_cert_task_id' => $employeeGroupCertTaskId
            ]);
            $result = $this->selectWith($select);
            if ($result->count() == 1) {
                $resultArray = $result->toArray();
                return $resultArray[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getEmployeeGroupCertTasksByEmployeeGroupId($employeeGroupId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'employee_group_id' => $employeeGroupId
            ]);
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                $resultArray = $result->toArray();
                return $resultArray;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existEmployeeGroupCertTask($employeeGroupId, $certId): bool
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'employee_group_id' => $employeeGroupId,
                'cert_id'           => $certId
            ]);
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     *
     * @param int $employeeGroupId
     * @param int $certId
     * @return int
     */
    public function insertEmployeeGroupCertTask(int $employeeGroupId, int $certId): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'employee_group_id' => $employeeGroupId,
                'cert_id'           => $certId
            ]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->insertWith($insert);
    }

    public function deleteEmployeeGroupCertTask($employeeGroupCertTaskId): int
    {
        $delete = $this->sql->delete();
        $delete->where(['employee_group_cert_task_id' => $employeeGroupCertTaskId]);
        return $this->deleteWith($delete);
    }
}
