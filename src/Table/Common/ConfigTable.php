<?php

namespace Lerp\Cert\Table\Common;

use Bitkorn\Trinket\Table\AbstractLibTable;

class ConfigTable extends AbstractLibTable
{

    protected $table = 'app_config';

    public function getConfigValue($configName)
    {
        $select = $this->sql->select();
        $select->where([
            'app_config_name' => $configName
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $current = $result->current();
            return $current['app_config_value'];
        }
        return '';
    }

}
