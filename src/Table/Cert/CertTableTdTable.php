<?php

namespace Lerp\Cert\Table\Cert;

use Bitkorn\Trinket\Table\AbstractLibTable;

class CertTableTdTable extends AbstractLibTable
{
    protected $table = 'cert_table_td';

    public function getCertTableTdById($certTableTdId)
    {
        $select = $this->sql->select();
        $select->where([
            'cert_table_td_id' => $certTableTdId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $resultArray = $result->toArray();
            return $resultArray[0];
        }
        return [];
    }

    public function updateCertTableTd(array $data): int
    {
        $update = $this->sql->update();
        try {
            $update->set($data);
            $update->where(['cert_table_td_id' => $data['cert_table_td_id']]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->updateWith($update);
    }

}
