<?php

namespace Lerp\Cert\Table\Cert;

use Bitkorn\Trinket\Table\AbstractLibTable;

class CertTableTable extends AbstractLibTable
{

    protected $table = 'cert_table';

    public function getCertTables()
    {
        $select = $this->sql->select();
        $select->order(['cert_table_group_id ASC', 'cert_table_order DESC']);
        $result = $this->selectWith($select);
        if ($result->count() > 0) {
            return $result->toArray();
        }
        return [];
    }

    public function getCertTableById($certTableId)
    {
        $select = $this->sql->select();
        $select->where([
            'cert_table_id' => $certTableId
        ]);
        $result = $this->selectWith($select);
        if ($result->count() == 1) {
            $resultArray = $result->toArray();
            return $resultArray[0];
        }
        return [];
    }

}
