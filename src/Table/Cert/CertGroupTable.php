<?php

namespace Lerp\Cert\Table\Cert;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Exception\RuntimeException;

class CertGroupTable extends AbstractLibTable
{
    protected $table = 'cert_group';

    public function getCertGroupById($certGroupId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cert_group_id' => $certGroupId,
            ]);
            $result = $this->selectWith($select);
            if ($result->count() == 1) {
                $resultArray = $result->toArray();
                return $resultArray[0];
            }
        } catch (\Exception $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getCertGroups()
    {
        $select = $this->sql->select();
        $select->order('cert_group_order ASC');
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    public function getCertGroupsIdAssoc(): array
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
        } catch (RuntimeException $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
            return [];
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $certGroup) {
                $idAssoc[$certGroup['cert_group_id']] = $certGroup['cert_group_name'];
            }
            return $idAssoc;
        }
        return [];
    }

}
