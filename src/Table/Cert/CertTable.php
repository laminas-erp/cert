<?php

namespace Lerp\Cert\Table\Cert;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\TableGateway\Exception\RuntimeException;

class CertTable extends AbstractLibTable
{

    protected $table = 'cert';

    public function getCertById($certId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cert_id' => $certId,
            ]);
            $result = $this->selectWith($select);
            if ($result->count() == 1) {
                $resultArray = $result->toArray();
                return $resultArray[0];
            }
        } catch (\Exception $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCerts($order = [], $onlyActiveCerts = true)
    {
        $select = $this->sql->select();
        if ($onlyActiveCerts) {
            $select->where(['cert_active' => 1]);
        }
        $select->order($order);
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    public function getCertsIdAssoc(): array
    {
        $select = $this->sql->select();
        $select->columns(['cert_id', 'cert_name']);
        $select->order('cert_order ASC');
        try {
            $result = $this->selectWith($select);
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
            return [];
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $cert) {
                $idAssoc[$cert['cert_id']] = $cert['cert_name'];
            }
            return $idAssoc;
        }
        return [];
    }

    public function getCertsIdAssocTaskAll(): array
    {
        $select = $this->sql->select();
        $select->order('cert_order ASC');
        try {
            $select->where(['task_all' => 1]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                $idAssoc = [];
                foreach ($resultArr as $cert) {
                    $idAssoc[$cert['cert_id']] = $cert;
                }
                return $idAssoc;
            }
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCertsIdAssocExt(): array
    {
        $select = $this->sql->select();
        $select->columns(['cert_id', 'cert_name', 'cert_desc', 'cert_group_id']);
        $select->order('cert_order ASC');
        try {
            $result = $this->selectWith($select);
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
            return [];
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $cert) {
                $idAssoc[$cert['cert_id']]['cert_id'] = $cert['cert_id'];
                $idAssoc[$cert['cert_id']]['cert_name'] = $cert['cert_name'];
                $idAssoc[$cert['cert_id']]['cert_desc'] = $cert['cert_desc'];
                $idAssoc[$cert['cert_id']]['cert_group_id'] = $cert['cert_group_id'];
            }
            return $idAssoc;
        }
        return [];
    }

    /**
     * @param array $data
     * @return int
     * @todo last insert ID zurueck geben um einen Button "bearbeiten" des erstellten Zertifikats anzuzeigen
     */
    public function insertCert(array $data): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
            $this->insertWith($insert);
            return $this->adapter->getDriver()->getLastGeneratedValue();
//            $select = $this->sql->select();
//            $select->columns([new \Laminas\Db\Sql\Expression('LAST_INSERT_ID() as last_id')]);
//            $lastIdResult = $this->selectWith($select);
//            if($lastIdResult->valid()) {
//                $lastIdArr = $lastIdResult->toArray();
//                return $lastIdArr['last_id'];
//            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    public function updateCert(array $data): int
    {
        $update = $this->sql->update();
        try {
            $update->set($data);
            $update->where(['cert_id' => $data['cert_id']]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $this->updateWith($update);
    }

}
