<?php

namespace Lerp\Cert\Table\Cert;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Exception\RuntimeException;

class CertTableGroupTable extends AbstractLibTable
{

    protected $table = 'cert_table_group';

    public function getCertTableGroupById($certTableGroupId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cert_table_group_id' => $certTableGroupId,
            ]);
            $result = $this->selectWith($select);
            if ($result->count() == 1) {
                $resultArray = $result->toArray();
                return $resultArray[0];
            }
        } catch (\Exception $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getCertTableGroups()
    {
        $select = $this->sql->select();
        $select->order('cert_table_group_id ASC');
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    public function getCertTableGroupsIdAssoc(): array
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
            return [];
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $certGroup) {
                $idAssoc[$certGroup['cert_table_group_id']] = $certGroup['cert_table_group_name'];
            }
            return $idAssoc;
        }
        return [];
    }

    /**
     *
     * @param array $data
     * @return int Last insert ID.
     */
    public function insertCertTableGroup(array $data): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
            return -1;
        }
        if ($this->insertWith($insert)) {
            $lastGeneratedValue = $this->adapter->getDriver()->getLastGeneratedValue();
            if ($lastGeneratedValue == (int)$lastGeneratedValue) {
                return (int)$lastGeneratedValue;
            }
        }
        return 0;
    }

    public function updateCertTableGroup(array $data): int
    {
        $update = $this->sql->update();
        try {
            $update->set($data);
            $update->where(['cert_table_group_id' => $data['cert_table_group_id']]);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
            return -1;
        }
        return $this->updateWith($update);
    }

}
