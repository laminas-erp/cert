<?php

namespace Lerp\Cert\Table\Cert\Chain;

use Bitkorn\Trinket\Table\AbstractLibTable;

class CertChainItemTable extends AbstractLibTable
{

    protected $table = 'cert_chain_item';

    public function getCertChainItemById($certChainItemId)
    {
        $select = $this->sql->select();
        try {
            $select->where(['cert_chain_item_id' => $certChainItemId]);
            $result = $this->selectWith($select);
            if ($result->count() == 1) {
                $resultArray = $result->toArray();
                return $resultArray[0];
            }
        } catch (\Exception $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getCertChainItemsByCertChainId($certChainId)
    {
        $select = $this->sql->select();
        try {
            $select->where(['cert_chain_id' => $certChainId]);
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param int $certChainItemId
     * @param string $type
     * @return int
     */
    public function changeCertChainItemType(int $certChainItemId, string $type): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['cert_chain_item_type' => $type]);
            $update->where(['cert_chain_item_id' => $certChainItemId]);
        } catch (\Exception $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
        }
        return $this->updateWith($update);
    }

}
