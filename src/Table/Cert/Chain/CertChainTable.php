<?php

namespace Lerp\Cert\Table\Cert\Chain;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Exception\RuntimeException;

class CertChainTable extends AbstractLibTable
{

    protected $table = 'cert_chain';

    public function getCertChainById($certChainId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cert_chain_id' => $certChainId,
            ]);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current() ?? [];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCertChains()
    {
        $select = $this->sql->select();
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    public function getCertChainsIdAssoc(): array
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                $resultArr = $result->toArray();
                $idAssoc = [];
                foreach ($resultArr as $certChain) {
                    $idAssoc[$certChain['cert_chain_id']] = $certChain['cert_chain_name'];
                }
                return $idAssoc;
            }
        } catch (RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param array $storage
     * @return int
     */
    public function saveCertChain(array $storage): int
    {
        $insert = $this->sql->insert();
        $insert->values($storage);
        $this->insertWith($insert);
        $certChainId = $this->adapter->getDriver()->getLastGeneratedValue();
        if (empty($certChainId)) {
            return -1;
        }
        return $certChainId;
    }

    /**
     * @param int $certChainId
     * @param array $storage
     * @return int
     */
    public function updateCertChain(int $certChainId, array $storage): int
    {
        $update = $this->sql->update();
        $update->where(['cert_chain_id' => $certChainId]);
        try {
            $update->set($storage);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
