<?php

namespace Lerp\Cert\Table\Cert;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Exception\RuntimeException;

class CertDefTable extends AbstractLibTable
{
    protected $table = 'cert_def';

    public function getCertDefById(int $certDefId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cert_def_id' => $certDefId,
            ]);
            $result = $this->selectWith($select);
            if ($result->count() == 1) {
                $resultArray = $result->toArray();
                return $resultArray[0];
            }
        } catch (\Exception $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCertDefs(): array
    {
        $select = $this->sql->select();
        $result = $this->selectWith($select);
        return $result->toArray();
    }

    public function getCertDefsIdAssoc(): array
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
        } catch (RuntimeException $e) {
            $this->log($e, __CLASS__, __FUNCTION__);
            return [];
        }
        if ($result->count() > 0) {
            $resultArr = $result->toArray();
            $idAssoc = [];
            foreach ($resultArr as $certDef) {
                $idAssoc[$certDef['cert_def_id']] = $certDef['cert_def_name'];
            }
            return $idAssoc;
        }
        return [];
    }

}
