<?php

namespace Lerp\Cert\Tablex\Cert;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;

class CertTablex extends AbstractLibTable
{
    protected $table = 'dummy';

    const QUERY_SELECT_CERT_JOIN = 'SELECT c.*, cd.*, cg.*
                        FROM cert c
                        LEFT JOIN cert_def cd ON c.cert_def_id = cd.cert_def_id
                        LEFT JOIN cert_group cg ON c.cert_group_id = cg.cert_group_id
                        WHERE c.cert_id = ?
                        ORDER BY c.cert_order ASC';

    /**
     * @param int $certId
     * @return array
     */
    public function getCertById(int $certId): array
    {
        $parameter = new ParameterContainer([$certId]);
        $stmt = $this->adapter->createStatement(self::QUERY_SELECT_CERT_JOIN, $parameter);
        $result = $stmt->execute();
        if ($result->count() > 0) {
            return $result->current();
        }
        return [];
    }

    const ARCHIVE_EMPLOYEE_CERT_REL = 'INSERT
                                        INTO
                                            employee_cert_rel_archive(
                                                SELECT
                                                    *
                                                FROM
                                                    employee_cert_rel
                                                WHERE
                                                    employee_id = ?
                                                    AND employee_cert_rel_cert_id = ?
                                            )';

    public function archiveEmployeeCertRel($employeeId, $certId)
    {
        $parameter = new ParameterContainer([$employeeId, $certId]);
        $stmt = $this->adapter->createStatement('call proc_archive_employee_cert_rel_if(?,?)', $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return $result->getAffectedRows();
        }
        return -1;
    }

    const DELETE_EMPLOYEE_CERT_REL = 'DELETE
                                        FROM
                                            employee_cert_rel
                                        WHERE
                                            employee_id = ?
                                            AND employee_cert_rel_cert_id = ?';

    public function deleteEmployeeCertRel($employeeId, $certId): int
    {
        $parameter = new ParameterContainer([$employeeId, $certId]);
        $stmt = $this->adapter->createStatement(self::DELETE_EMPLOYEE_CERT_REL, $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return $result->getAffectedRows();
        }
        return -1;
    }

    /**
     *
     * @param int $certId
     * @param CertTable $certTable
     * @return int
     * @throws \RuntimeException
     */
    public function copycert(int $certId, CertTable $certTable): int
    {
        $parameter = new ParameterContainer([$certId]);
        $stmt = $this->adapter->createStatement('SELECT * FROM cert WHERE cert_id=?', $parameter);
        $result = $stmt->execute();
        $cert = [];
        if ($result->count() == 1) {
            $cert = $result->current();
            unset($cert['cert_id']);
        } else {
            throw new \RuntimeException('cert with cert_id ' . $certId . 'not found');
        }
        if (!empty($cert)) {
            return $certTable->insertCert($cert);
        }
        return 0;
    }

    const SELECT_CERTS_WHERE_NOT_IN_CERTCHAINITEM = 'SELECT * FROM cert WHERE cert_id NOT IN (SELECT cert_id FROM cert_chain_item WHERE cert_chain_id = ?)';

    /**
     *
     * @param int $certChainId
     * @return array
     */
    public function getCertsForCertChain(int $certChainId): array
    {
        $parameter = new ParameterContainer([$certChainId]);
        $stmt = $this->adapter->createStatement(self::SELECT_CERTS_WHERE_NOT_IN_CERTCHAINITEM, $parameter);
        $result = $stmt->execute();
        $certs = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $certs[] = $result->current();
                $result->next();
            } while ($result->valid());
        }
        return $certs;
    }

    const SELECT_CERTCHAIN_TYPES_FOR_CERT = 'SELECT cc.cert_chain_id, cc.cert_chain_type, cc.cert_chain_chain_type, cci.cert_id, cci.cert_chain_item_type
                                                FROM cert_chain_item cci LEFT JOIN cert_chain cc 
                                                ON cc.cert_chain_id = cci.cert_chain_id
                                                WHERE cci.cert_id=?';

    /**
     *
     * @param int $certId Cert ID to look for certChains.
     * @return array An empty Array if there are no CertChains
     *              Array
     *                (
     *                    [0] => Array
     *                        (
     *                            [cert_chain_id] => 1
     *                            [cert_chain_type] => group
     *                            [cert_chain_chain_type] => update
     *                            [cert_id] => 40
     *                            [cert_chain_item_type] => item
     *                        )
     *                )
     */
    public function existCertChainForCert(int $certId): array
    {
        $parameter = new ParameterContainer([$certId]);
        $stmt = $this->adapter->createStatement(self::SELECT_CERTCHAIN_TYPES_FOR_CERT, $parameter);
        $result = $stmt->execute();
        $certChainTypes = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $certChainTypes[] = $result->current();
                $result->next();
            } while ($result->valid());
        }
        return $certChainTypes;
    }

    const SELECT_CERTREL_WHERE_EMPLOYEE_AND_CERTID = 'SELECT * FROM employee_cert_rel WHERE employee_cert_rel_cert_id=? AND employee_id=?';
    const SELECT_CERTIDS_WHERE_CERTCHEINITEM_CERTID = 'SELECT cert_id FROM cert_chain_item WHERE cert_chain_id = ?';
    const INSERT_CERTRELS_FROM_MASTER = 'INSERT INTO employee_cert_rel (employee_cert_rel_cert_id,employee_id,date_of_issue,cert_def_value,cert_restriction) VALUES (?,?,?,?,?)';
    const UPDATE_CERTRELS_WHERE_EMPLOYEE_AND_CERTID = 'UPDATE employee_cert_rel SET date_of_issue=?, cert_def_value=?, cert_restriction=? WHERE employee_id=? AND employee_cert_rel_cert_id=?';

    /**
     *
     * @param array $certChainTypesWithCertId Comes from $this->existCertChainForCert($certId)
     * @param int $employeeId
     * @param EmployeeCertRelTable $employeecertRelTable
     * @return int Affected rows from updated db.employee_cert_rel
     */
    public function computeCertChainTypesWithCertIdForEmployee(array                $certChainTypesWithCertId, int $employeeId,
                                                               EmployeeCertRelTable $employeecertRelTable): int
    {
        if (empty($certChainTypesWithCertId)) {
            return 0;
        }
        $updateCount = 0;
        foreach ($certChainTypesWithCertId as $certChainTypeWithCertId) {
            if (empty($certChainTypeWithCertId['cert_chain_id']) || empty($certChainTypeWithCertId['cert_chain_type']) || empty($certChainTypeWithCertId['cert_chain_item_type'])) {
                continue;
            }
            if ($certChainTypeWithCertId['cert_chain_type'] == 'master' && $certChainTypeWithCertId['cert_chain_item_type'] != 'master') {
                continue;
            }
            $parameterMaster = new ParameterContainer([$certChainTypeWithCertId['cert_id'], $employeeId]);
            $stmtMaster = $this->adapter->createStatement(self::SELECT_CERTREL_WHERE_EMPLOYEE_AND_CERTID, $parameterMaster);
            $resultMaster = $stmtMaster->execute();
            if (!$resultMaster->valid() && $resultMaster->count() != 1) {
                continue;
            }
            $master = $resultMaster->current();
            if (empty($master)) {
                return -1;
            }

            $parameterSelect = new ParameterContainer([$certChainTypeWithCertId['cert_chain_id']]);
            $stmtSelect = $this->adapter->createStatement(self::SELECT_CERTIDS_WHERE_CERTCHEINITEM_CERTID, $parameterSelect);
            $resultSelect = $stmtSelect->execute();
            if ($resultSelect->valid() || $resultSelect->count() > 0) {
                do {
                    $current = $resultSelect->current();
                    if ($current['cert_id'] == $master['employee_cert_rel_cert_id']) {
                        continue;
                    }
                    if ($certChainTypeWithCertId['cert_chain_chain_type'] == 'create_update') {
                        if (!$employeecertRelTable->existEmployeeCertRel($employeeId, $current['cert_id'])) {
                            $parameterInsert = new ParameterContainer([$current['cert_id'], $employeeId, $master['date_of_issue'], $master['cert_def_value'],
                                                                       $master['cert_restriction']]);
                            $stmtInsert = $this->adapter->createStatement(self::INSERT_CERTRELS_FROM_MASTER, $parameterInsert);
                            $resultInsert = $stmtInsert->execute();
                            if (!$resultInsert->valid()) {
                                $this->logger->warn('Query INSERT_CERTRELS_FROM_MASTER in ' . __FUNCTION__ . ' is not valid');
                            }
                        }
                    }

                    $parameterArray = [$master['date_of_issue'], $master['cert_def_value'], $master['cert_restriction'], $employeeId, $current['cert_id']];
                    $parameterUpdate = new ParameterContainer($parameterArray);
                    $stmtUpdate = $this->adapter->createStatement(self::UPDATE_CERTRELS_WHERE_EMPLOYEE_AND_CERTID, $parameterUpdate);
                    $resultUpdate = $stmtUpdate->execute();
                    $updateCount += $resultUpdate->getAffectedRows();
                    $resultSelect->next();
                } while ($resultSelect->valid());
            }
        }
        return $updateCount;
    }

}
