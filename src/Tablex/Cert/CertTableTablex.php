<?php

namespace Lerp\Cert\Tablex\Cert;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;

class CertTableTablex extends AbstractLibTable
{
    protected $table = 'dummy';

    private $queryTableTds = 'SELECT ctt.*, ct.*, c.*, cd.*
                        FROM cert_table_td ctt
                        LEFT JOIN cert_table ct ON ct.cert_table_id = ctt.cert_table_id
                        LEFT JOIN cert c ON c.cert_id = ctt.cert_id
                        LEFT JOIN cert_def cd ON cd.cert_def_id = c.cert_def_id
                        WHERE ctt.cert_table_id = ?
                        ORDER BY ctt.cert_table_id ASC, ctt.cert_table_row_index ASC, ctt.cert_table_column_index ASC';

    /**
     * @param int $certTableId
     * @return array
     */
    public function getCertTableWithTdsById(int $certTableId): array
    {
        $parameter = new ParameterContainer([$certTableId]);
        $stmt = $this->adapter->createStatement('call proc_get_certtable_with_tds(?)', $parameter);
        $result = $stmt->execute();
        if ($result->count() > 0) {
            $returnArray = [];
            while ($result->valid()) {
                $current = $result->current();
                $returnArray[] = $current;
                $result->next();
            }
            return $returnArray;
        }
        return [];
    }

    /**
     *
     * @param int $certTableGroupId
     * @param int $employeeId
     * @return array Von 0 an indexiert.
     */
    public function getCertTableTdsWithEmployeeCertRels(int $certTableGroupId, int $employeeId): array
    {
        $parameter = new ParameterContainer([$certTableGroupId, $employeeId]);
        $stmt = $this->adapter->createStatement('call proc_get_certtabletds_with_employeecertrels(?,?)', $parameter);
        $result = $stmt->execute();
        if ($result->count() > 0) {
            $returnArray = [];
            while ($result->valid()) {
                $current = $result->current();
                $returnArray[] = $current;
                $result->next();
            }
            return $returnArray;
        }
        return [];
    }

    /**
     *
     * @param array $certTableData Must be an indexed array (ongoing array keys from 0). Otherwise ParameterContainer make shit.
     * @return int
     */
    public function createCertTablePlusTds(array $certTableData): int
    {
        $parameter = new ParameterContainer($certTableData);
        $stmt = $this->adapter->createStatement('call proc_create_cert_table_plus_tds(?,?,?,?,?,?,?)', $parameter);
        $result = $stmt->execute();
        if ($result->count() > 0) {
            $current = $result->current();
            if (isset($current['insert_result'])) {
                return $current['insert_result'];
            }
        }
        return 0;
    }

    /**
     *
     * @param array $certTableData Must be an indexed array (ongoing array keys from 0). Otherwise ParameterContainer make shit.
     * @return array 0 => update count, 1 => td count
     */
    public function editCertTableIncludingDimensionPlusTds(array $certTableData): array
    {
        $parameter = new ParameterContainer($certTableData);
        $stmt = $this->adapter->createStatement('call proc_edit_cert_table_including_dimension_plus_tds(?,?,?,?,?,?,?,?)', $parameter);
        $result = $stmt->execute();
        if ($result->count() > 0) {
            $current = $result->current();
            if (isset($current['edit_result'])) {
                $editResult = explode('_', $current['edit_result']);
                return $editResult;
            }
        }
        return [];
    }

    public function deleteCertTablePlusTds($certTableId): array
    {
        $parameter = new ParameterContainer([$certTableId]);
        $stmt = $this->adapter->createStatement('call proc_delete_cert_table_plus_tds(?)', $parameter);
        $result = $stmt->execute();
        if ($result->valid()) {
            $current = $result->current();
            if (isset($current['delete_result'])) {
                return explode('_', $current['delete_result']);
            }
        }
        return [];
    }

}
