<?php

namespace Lerp\Cert\Tablex\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;

class EmployeeGroupTablex extends AbstractLibTable
{
    protected $table = 'dummy';

    const SELECT_JOIN_EMPLOYEEGROUPS = 'SELECT 
                                            egct.employee_group_cert_task_id,
                                            eg.employee_group_name,
                                            eg.employee_group_desc,
                                            c.*
                                        FROM
                                            employee_group_cert_task egct
                                                LEFT JOIN
                                            employee_group eg ON eg.employee_group_id = egct.employee_group_id
                                                LEFT JOIN
                                            cert c ON c.cert_id = egct.cert_id';

    /**
     * @return array
     */
    public function getEmployeeGroupCertTasks(): array
    {
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_EMPLOYEEGROUPS);
        $result = $stmt->execute();
        $returnArr = [];
        if ($result->count() > 0) {
            while ($result->valid()) {
                $returnArr[] = $result->current();
                $result->next();
            }
        }
        return $returnArr;
    }

    const SELECT_JOIN_EMPLOYEEGROUP_REL_CERTTASK = 'SELECT 
                                                        eg.*,
                                                        COUNT(distinct(egr.employee_group_rel_id)) AS employee_group_rel_count,
                                                        COUNT(distinct(egct.employee_group_cert_task_id)) AS employee_group_cert_task_count
                                                    FROM
                                                        employee_group eg
                                                            LEFT JOIN
                                                        employee_group_rel egr ON egr.employee_group_id = eg.employee_group_id
                                                            LEFT JOIN
                                                        employee_group_cert_task egct ON egct.employee_group_id = eg.employee_group_id
                                                    GROUP BY eg.employee_group_id';

    public function getEmployeeGroups(): array
    {
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_EMPLOYEEGROUP_REL_CERTTASK);
        $result = $stmt->execute();
        $returnArr = [];
        if ($result->count() > 0) {
            while ($result->valid()) {
                $returnArr[] = $result->current();
                $result->next();
            }
        }
        return $returnArr;
    }

    const SELECT_JOIN_EMPLOYEEGROUPREL_BY_GROUP_EMPLOYEE = 'SELECT 
                                                                egr.*, e.*
                                                            FROM
                                                                employee e
                                                                    LEFT JOIN
                                                                employee_group_rel egr ON egr.employee_id = e.employee_id
                                                            WHERE
                                                                egr.employee_group_id = ?';

    public function getEmployeeGroupRelsByEmployeeGroupId($employeeGroupId): array
    {
        $parameter = new ParameterContainer([$employeeGroupId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_EMPLOYEEGROUPREL_BY_GROUP_EMPLOYEE, $parameter);
        $result = $stmt->execute();
        $returnArr = [];
        if ($result->count() > 0) {
            while ($result->valid()) {
                $returnArr[] = $result->current();
                $result->next();
            }
        }
        return $returnArr;
    }

    public function xyz($employeeId)
    {
        $parameter = new ParameterContainer([$employeeId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_EMPLOYEEGROUP_REL_CERTTASK, $parameter);
        $result = $stmt->execute();
    }
}
