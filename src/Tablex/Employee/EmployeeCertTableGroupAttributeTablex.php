<?php

namespace Lerp\Cert\Tablex\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Lerp\Cert\Table\Employee\EmployeeCertTableGroupAttributeRelTable;

class EmployeeCertTableGroupAttributeTablex extends AbstractLibTable
{
    protected $table = 'dummy';

//    const SELECT_JOIN_TABLEGROUPATTRIBUTE = 'SELECT
//                                                    ectgad.*,
//                                                    ectgar.*
//                                            FROM
//                                                    employee_cert_table_group_attribute_def ectgad
//                                            LEFT JOIN employee_cert_table_group_attribute_rel ectgar
//                                                            USING(employee_cert_table_group_attribute_def_id)
//                                            WHERE
//                                                    ectgad.cert_table_group_id = ?';
//    
//    public function getEmployeeCertTableGroupAttributeDefsForTableGroup($certTableGroupId)
//    {
//        $parameter = new \Laminas\Db\Adapter\ParameterContainer(array($certTableGroupId));
//        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_TABLEGROUPATTRIBUTE, $parameter);
//        $result = $stmt->execute();
//        if ($result->valid() && $result->count() > 0) {
//            return  $result->getResource()->fetchAll(\PDO::FETCH_ASSOC);
//        }
//        return [];
//    }

    const SELECT_JOIN_TABLEGROUPATTRIBUTE_EMPLOYEE = 'SELECT
                                                            ectgad.*,
                                                            ectgar.employee_id, ectgar.employee_cert_table_group_attribute_value
                                                        FROM
                                                                employee_cert_table_group_attribute_def ectgad
                                                        LEFT JOIN employee_cert_table_group_attribute_rel ectgar
                                                                        USING(employee_cert_table_group_attribute_def_id)
                                                        WHERE
                                                                ectgad.cert_table_group_id = ?
                                                                AND (ectgar.employee_id IS NULL OR ectgar.employee_id = ?)';

    public function getEmployeeCertTableGroupAttributesForEmployee($certTableGroupId, $employeeId)
    {
        $parameter = new ParameterContainer([$certTableGroupId, $employeeId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_TABLEGROUPATTRIBUTE_EMPLOYEE, $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return $result->getResource()->fetchAll(\PDO::FETCH_ASSOC);
        }
        return [];
    }

    public function getEmployeeCertTableGroupAttributesForEmployeeDefIdIdAssoc($certTableGroupId, $employeeId): array
    {
        $parameter = new ParameterContainer([$certTableGroupId, $employeeId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_TABLEGROUPATTRIBUTE_EMPLOYEE, $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            $idAssoc = [];
            do {
                $current = $result->current();
                $idAssoc[$current['employee_cert_table_group_attribute_def_id']] = $current;
                $result->next();
            } while ($result->valid());
            return $idAssoc;
        }
        return [];
    }

    const SELECT_COUNT_TABLEGROUPATTRIBUTE = 'SELECT
                                                        COUNT( employee_cert_table_group_attribute_def_id ) AS rel_count
                                                FROM
                                                        employee_cert_table_group_attribute_rel
                                                WHERE
                                                        employee_cert_table_group_attribute_def_id =?
                                                        AND employee_id =?';

    const INSERT_TABLEGROUPATTRIBUTE = 'INSERT';
    const UPDATE_TABLEGROUPATTRIBUTE = '';

    public function saveOrUpdateEmployeeCertTableGroupAttribute($certTableGroupAttributeDefId, $employeeId, $certTableGroupAttributeValue, EmployeeCertTableGroupAttributeRelTable $employeeCertTableGroupAttributeRelTable): int
    {
        $parameter = new ParameterContainer([$certTableGroupAttributeDefId, $employeeId]);
        $stmt = $this->adapter->createStatement(self::SELECT_COUNT_TABLEGROUPATTRIBUTE, $parameter);
        $result = $stmt->execute();
        $relCount = 0;
        if ($result->valid() && $result->count() > 0) {
            $current = $result->current();
            $relCount = $current['rel_count'];
        }
        if ($relCount == 0) {
            return $employeeCertTableGroupAttributeRelTable->insertEmployeeCertTableGroupAttributeRel($certTableGroupAttributeDefId, $employeeId, $certTableGroupAttributeValue);
        } else {
            return $employeeCertTableGroupAttributeRelTable->updateEmployeeCertTableGroupAttributeRel($certTableGroupAttributeDefId, $employeeId, $certTableGroupAttributeValue);
        }
    }
}
