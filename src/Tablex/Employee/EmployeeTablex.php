<?php

namespace Lerp\Cert\Tablex\Employee;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;

class EmployeeTablex extends AbstractLibTable
{
    protected $table = 'dummy';

    const EMPLOYEE_TABLE = 'employee';
    const EMPLOYEE_GROUP_TABLE = 'employee_group';
    const EMPLOYEE_GROUP_REL_TABLE = 'employee_group_rel';

    /**
     * Join over employee_cert_rel, cert, employee, cert_def
     */
    const SELECT_JOIN_EMPLOYEE_CERT_RELS = 'SELECT ecr.*, e.*, c.*, cd.*
                                        FROM employee_cert_rel ecr
                                        LEFT JOIN cert c ON ecr.employee_cert_rel_cert_id = c.cert_id
                                        LEFT JOIN employee e ON ecr.employee_id = e.employee_id
                                        LEFT JOIN cert_def cd ON cd.cert_def_id = c.cert_def_id
                                        WHERE ecr.employee_id = ?
                                        ORDER BY c.cert_group_id ASC';
    const SELECT_JOIN_CERT_RELS = 'SELECT 
                                    ecr.*, e.*, c.*, cd.*
                                FROM
                                    employee_cert_rel ecr
                                        LEFT JOIN
                                    employee e ON e.employee_id = ecr.employee_id
                                        LEFT JOIN
                                    cert c ON c.cert_id = ecr.employee_cert_rel_cert_id
                                        LEFT JOIN
                                    cert_def cd ON cd.cert_def_id = c.cert_def_id
                                ORDER BY date_of_issue ASC;';

    /**
     * @param int $employeeId
     * @return array Assoziativ mit der Cert ID als key
     */
    public function getEmployeeCertRels(int $employeeId): array
    {
        $parameter = new ParameterContainer([$employeeId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_EMPLOYEE_CERT_RELS, $parameter);
        $result = $stmt->execute();
        $returnArr = [];
        if ($result->count() > 0) {
            while ($result->valid()) {
                $current = $result->current();
                $returnArr[$current['cert_id']] = $current;
                $result->next();
            }
        }
        return $returnArr;
    }

    /**
     *
     * @return int 0 on success, -1 on error
     */
    public function callProcUpdateEmployeeCertRelRemainingTimeDays(): int
    {
        $stmt = $this->adapter->createStatement('call proc_update_employee_cert_rel_remaining_time_days()');
        $result = $stmt->execute();
        if ($result->valid()) {
            return 0;
        }
        return -1;
    }

    /**
     * @return array
     */
    public function getCertRels($maxRemainingDays = 300): array
    {
        $parameter = new ParameterContainer([$maxRemainingDays]);
        $stmt = $this->adapter->createStatement('call proc_get_cert_rels(?)', $parameter);
        $result = $stmt->execute();
        $returnArr = [];
        if ($result->count() > 0) {
            while ($result->valid()) {
                $returnArr[] = $result->current();
                $result->next();
            }
        }
        return $returnArr;
    }

    public function getEmployeesWhereNotInGroup($employeeGroupId): array
    {
        $parameter = new ParameterContainer([$employeeGroupId]);
        $stmt = $this->adapter->createStatement('SELECT * FROM employee WHERE employee_id NOT IN (SELECT employee_id FROM employee_group_rel WHERE employee_group_id=?)',
            $parameter);
        $result = $stmt->execute();
        $returnArr = [];
        if ($result->count() > 0) {
            while ($result->valid()) {
                $returnArr[] = $result->current();
                $result->next();
            }
        }
        return $returnArr;
    }

    const SELECT_JOIN_EMPLOYEECERTRELS_GROUPCERTTASK = 'SELECT 
                                                            ecr.*, c.duration, c.task_all
                                                        FROM
                                                            employee_cert_rel ecr
                                                                LEFT JOIN
                                                            cert c ON c.cert_id = ecr.employee_cert_rel_cert_id
                                                        WHERE
                                                            ecr.employee_id IN (SELECT 
                                                                    employee_id
                                                                FROM
                                                                    employee_group_rel
                                                                WHERE
                                                                    employee_group_id IN (SELECT 
                                                                            employee_group_id
                                                                        FROM
                                                                            employee_group_cert_task))
                                                        ORDER BY employee_cert_rel_remaining_time_days ASC , date_of_issue DESC';

    /**
     * KAKKE weil zeigt nicht die die kein CertRel haben. Also NICHT die wo Pflicht aber kein CertRel.
     * @return array
     */
    public function getEmployeeCertRelsWhereInEmployeeGroupCertTask(): array
    {
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_EMPLOYEECERTRELS_GROUPCERTTASK);
        $result = $stmt->execute();
        $returnArr = [];
        if ($result->count() > 0) {
            while ($result->valid()) {
                $returnArr[] = $result->current();
                $result->next();
            }
        }
        return $returnArr;
    }

    const SELECT_JOIN_EMPLOYEE_GROUP_RELS = 'SELECT 
                                                CONCAT_WS(\'_\', egct.cert_id, egr.employee_id) AS cid_eid,
                                                egct.*,
                                                egr.*,
                                                e.*,
                                                c.*
                                            FROM
                                                employee_group_cert_task egct
                                                    LEFT JOIN
                                                employee_group_rel egr USING (employee_group_id)
                                                    LEFT JOIN
                                                employee e USING (employee_id)
                                                    LEFT JOIN
                                                cert c ON c.cert_id = egct.cert_id
                                            WHERE
                                                e.is_active = 1
                                            ORDER BY e.name_2 ASC , c.cert_order ASC';

    /**
     *
     * @return array AssocArray with key: certId_employeeId
     * Each entry is a task for one employee wich has a groupcerttask.
     */
    public function getEmployeeGroupCertTasksIdAssocExtCertIdEmployeeId(): array
    {
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_EMPLOYEE_GROUP_RELS);
        $result = $stmt->execute();
        $returnArr = [];
        if ($result->count() > 0) {
            while ($result->valid()) {
                $current = $result->current();
                $returnArr[$current['cid_eid']] = $current;
                $result->next();
            }
        }
        return $returnArr;
    }

//    const SELECT_JOIN_CERTRELS_CERTTASKALL = 'SELECT 
//                                                    ecr.*, e.*, c.*
//                                                FROM
//                                                    employee_cert_rel ecr
//                                                        LEFT JOIN
//                                                    employee e USING (employee_id)
//                                                        LEFT JOIN
//                                                    cert c ON c.cert_id = ecr.employee_cert_rel_cert_id
//                                                WHERE
//                                                    c.task_all = 1 AND e.is_active = 1
//                                                    ORDER BY e.name_2 ASC , c.cert_order ASC';
//    
//    public function getEmployeeCertrelsCertTaskAllIdAssoc()
//    {
//        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_CERTRELS_CERTTASKALL);
//        $result = $stmt->execute();
//        $returnArr = [];
//        if ($result->count() > 0) {
//            while ($result->valid()) {
//                $current = $result->current();
//                $returnArr[] = $current;
//                $result->next();
//            }
//        }
//        return $returnArr;
//    }

}
