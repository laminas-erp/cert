<?php

namespace Lerp\Cert\Listener;

use Laminas\EventManager\AbstractListenerAggregate;
use Laminas\EventManager\EventManagerInterface;
use Laminas\EventManager\EventInterface;
use Laminas\Log\Logger;
use Lerp\Cert\Cert\CertEvent;

/**
 * EmployeeCertRelListener funzt NICHT
 * Siehe Module.php
 * und \Lerp\Cert\Controller\Manager\ShowController()->employeeAction()
 *
 * @author allapow
 */
class EmployeeCertRelListener extends AbstractListenerAggregate
{

    protected $listeners = [];
    private Logger $logger;

    public function attach(EventManagerInterface $events, $priority = 1): void
    {
        $this->listeners[] = $events->attach(CertEvent::EVENT_EMPLOYEE_CERT_REL_BEFORE,
            [$this, 'checkDoubleEmployeeCertRels'], 100);
        $this->logger->debug('attached');
    }

    public function checkDoubleEmployeeCertRels(EventInterface $e)
    {
        $employeeId = $e->getParam('employee_id');
        $this->logger->debug('EVENT_EMPLOYEE_CERT_REL_BEFORE $employeeId: ' . $employeeId);
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

}
