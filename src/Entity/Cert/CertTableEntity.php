<?php

namespace Lerp\Cert\Entity\Cert;

use Bitkorn\Trinket\Entity\AbstractEntity;
use Laminas\Log\Logger;
use Lerp\Cert\Tablex\Cert\CertTableTablex;

/**
 * @author allapow
 */
class CertTableEntity extends AbstractEntity
{

    protected array $mapping = [
        'cert_table_id'           => 'cert_table_id',
        'cert_table_group_id'     => 'cert_table_group_id',
        'cert_table_row_count'    => 'cert_table_row_count',
        'cert_table_column_count' => 'cert_table_column_count',
        'cert_table_heading'      => 'cert_table_heading',
        'cert_table_order'        => 'cert_table_order',
        'cert_table_row_names'    => 'cert_table_row_names',
        'cert_table_column_names' => 'cert_table_column_names',
    ];

    /**
     *
     * @param CertTableTablex $certTablex
     * @param Logger $logger
     * @return int Table ID concatenated with underscore and Amount of inserted td's
     */
    public function save(CertTableTablex $certTablex, Logger $logger): int
    {
        unset($this->storage['cert_table_id']);
        return $certTablex->createCertTablePlusTds(array_values($this->storage));
    }

    public function update(CertTableTablex $certTablex, Logger $logger): array
    {
        return $certTablex->editCertTableIncludingDimensionPlusTds(array_values($this->storage));
    }

    /**
     *
     * @param array $data
     * @return array Hat sich nix geaendert: leeres Array, sonst Array keys row & column mit Weten.
     */
    public function changedTableDimension(array $data): array
    {
        if (!isset($data['cert_table_row_count']) || !isset($data['cert_table_column_count'])) {
            return [];
        }
        $returnArray = [];
        $changed = false;
        if ($data['cert_table_row_count'] != $this->storage['cert_table_row_count']) {
            $returnArray['row'] = $data['cert_table_row_count'] - $this->storage['cert_table_row_count'];
            $changed = true;
        } else {
            $returnArray['row'] = 0;
        }
        if ($data['cert_table_column_count'] != $this->storage['cert_table_column_count']) {
            $returnArray['column'] = $data['cert_table_column_count'] - $this->storage['cert_table_column_count'];
            $changed = true;
        } else {
            $returnArray['column'] = 0;
        }
        if (!$changed) {
            return [];
        }
        return $returnArray;
    }

}
