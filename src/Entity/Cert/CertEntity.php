<?php

namespace Lerp\Cert\Entity\Cert;

use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Brauch ich das hier???
 * Eigendlich reicht doch die Form!?!?
 * UNUSED
 *
 * @author allapow
 */
class CertEntity extends AbstractEntity
{

    protected array $mapping = [
        'id'                => 'id',
        'parent_id'         => 'parent_id',
        'cert_group_id'     => 'cert_group_id',
        'duration'          => 'duration',
        'cert_name'         => 'cert_name',
        'cert_desc'         => 'cert_desc',
        'task_all'          => 'task_all',
        'cert_name_visible' => 'cert_name_visible',
    ];
}
