<?php

namespace Lerp\Cert\Entity\Cert;

use Bitkorn\Trinket\Entity\AbstractEntity;
use Laminas\Log\Logger;
use Lerp\Cert\Table\Cert\CertTableGroupTable;

/**
 * @author allapow
 */
class CertTableGroupEntity extends AbstractEntity
{

    protected array $mapping = [
        'cert_table_group_id'     => 'cert_table_group_id',
        'cert_table_group_name'   => 'cert_table_group_name',
        'cert_table_group_desc'   => 'cert_table_group_desc',
        'cert_table_group_expiry' => 'cert_table_group_expiry' // UNUSED: ne tablegroup hat kein Ablaufdatum, nur ein tablegroup-attribute kann so etwas
    ];

    /**
     *
     * @param CertTableGroupTable $certTableGroupTable
     * @return int
     */
    public function save(CertTableGroupTable $certTableGroupTable, Logger $logger): int
    {
        unset($this->storage['cert_table_group_id']);
        $this->convertDateString();
        return $certTableGroupTable->insertCertTableGroup($this->storage);
    }

    public function update(CertTableGroupTable $certTableGroupTable, Logger $logger): int
    {
        $this->convertDateString();
        return $certTableGroupTable->updateCertTableGroup($this->storage);
    }

    public function getFormStorage(): array
    {
        $formStorage = $this->storage;
        if (!empty($formStorage['cert_table_group_expiry']) && is_numeric($this->storage['cert_table_group_expiry'])) {
            $formStorage['cert_table_group_expiry'] = date('Y-m-d', $formStorage['cert_table_group_expiry']);
        }
        return $formStorage;
    }

    private function convertDateString()
    {
        if (!empty($this->storage['cert_table_group_expiry']) && !is_numeric($this->storage['cert_table_group_expiry'])) {
            $expiryDatetime = new \DateTime($this->storage['cert_table_group_expiry']);
            if ($expiryDatetime) {
                $this->storage['cert_table_group_expiry'] = $expiryDatetime->getTimestamp();
            } else {
                $this->storage['cert_table_group_expiry'] = '';
                throw new \RuntimeException('Falsches Datumsformat: ' . $this->storage['cert_table_group_expiry'] . '; DateTime: ' . $expiryDatetime->getTimestamp());
            }
        }
    }

}
