<?php

namespace Lerp\Cert\Entity\Cert\Chain;

use Bitkorn\Trinket\Entity\AbstractEntity;
use Lerp\Cert\Table\Cert\Chain\CertChainTable;

/**
 * Brauch ich das hier???
 * Eigendlich reicht doch die Form!?!?
 * UNUSED
 *
 * @author allapow
 */
class CertChainEntity extends AbstractEntity
{

    protected array $mapping = [
        'cert_chain_id'         => 'cert_chain_id',
        'cert_chain_name'       => 'cert_chain_name',
        'cert_chain_desc'       => 'cert_chain_desc',
        'cert_chain_type'       => 'cert_chain_type',
        'cert_chain_chain_type' => 'cert_chain_chain_type',
    ];

    public function save(CertChainTable $certChainTable)
    {
        return $certChainTable->saveCertChain($this->storage);
    }

    public function update($certChainId, CertChainTable $certChainTable)
    {
        return $certChainTable->updateCertChain($certChainId, $this->storage);
    }
}
