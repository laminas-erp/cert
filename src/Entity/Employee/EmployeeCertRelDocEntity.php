<?php

namespace Lerp\Cert\Entity\Employee;

use Bitkorn\Trinket\Entity\AbstractEntity;
use Lerp\Cert\Table\Employee\EmployeeCertRelDocTable;
use Lerp\Cert\Tools\FolderSystem;

/**
 *
 * @author allapow
 */
class EmployeeCertRelDocEntity extends AbstractEntity
{

    protected array $mapping = [
        'employee_cert_rel_doc_id'               => 'employee_cert_rel_doc_id',
        'employee_cert_rel_doc_name'             => 'employee_cert_rel_doc_name',
        'employee_cert_rel_id'                   => 'employee_cert_rel_id',
        'employee_cert_rel_doc_main_folder_root' => 'employee_cert_rel_doc_main_folder_root',
        'employee_cert_rel_doc_datetime'         => 'employee_cert_rel_doc_datetime',
        'employee_cert_rel_doc_filename'         => 'employee_cert_rel_doc_filename',
        'employee_cert_rel_doc_fqfn'             => 'employee_cert_rel_doc_fqfn',
        'cert_id'                                => 'cert_id',
        'employee_id'                            => 'employee_id',
    ];

    public function save(EmployeeCertRelDocTable $employeecertRelDocTable)
    {
        return $employeecertRelDocTable->insertCertDoc($this->storage);
    }

    public static function delete(EmployeeCertRelDocTable $employeecertRelDocTable, $employeeCertRelDocId,
                                  $docPathAbsolute)
    {
        $employeecertRelDocData = $employeecertRelDocTable->getEmployeeCertRelDocById($employeeCertRelDocId);
        if (empty($employeecertRelDocData)) {
            throw new \RuntimeException('try to delete an employee-cert-rel-doc with bad employee_cert_rel_doc_id: ' . $employeeCertRelDocId);
        }
        if (empty($docPathAbsolute)) {
            throw new \RuntimeException('try to delete an employee-cert-rel-doc with empty $docPathAbsolute.');
        }
        $relativeFilename = FolderSystem::computeRelativeFilename($employeecertRelDocData['employee_cert_rel_doc_main_folder_root'],
            $employeecertRelDocData['employee_cert_rel_doc_datetime'],
            $employeecertRelDocData['employee_cert_rel_doc_filename']);
        if (!file_exists($docPathAbsolute . $relativeFilename) && is_file($docPathAbsolute . $relativeFilename)) {
            throw new \RuntimeException('try to delete an employee-cert-rel-doc that does not exist: ' . $docPathAbsolute . $relativeFilename);
        }
        if (!unlink($docPathAbsolute . $relativeFilename)) {
            throw new \RuntimeException('unable to delete an employee-cert-rel-doc: ' . $docPathAbsolute . $relativeFilename);
        }
        if (!$employeecertRelDocTable->deleteEmployeecertRelDocById($employeeCertRelDocId)) {
            throw new \RuntimeException('try to delete an employee-cert-rel-doc from db with employee_cert_rel_doc_id: ' . $employeeCertRelDocId);
        }
    }

}
