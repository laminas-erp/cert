<?php

namespace Lerp\Cert\Entity\Employee;

use Bitkorn\Trinket\Entity\AbstractEntity;

class EmployeeCertRelEntity extends AbstractEntity
{

    protected array $mapping = [
        'id'               => 'employee_cert_rel_id',
        //'cert_id' => 'employee_cert_rel_cert_id', // 2021-04-08 war doppelter array key
        'employee_id'      => 'employee_id',
        'date_of_issue'    => 'date_of_issue',
        'licence_no'       => 'licence_no',
        'cert_def_value'   => 'cert_def_value',
        'cert_restriction' => 'cert_restriction',
        'name_1'           => 'name_1',
        'name_2'           => 'name_2',
        'no_timerec'       => 'no_timerec',
        'no_godata_id'     => 'no_godata_id',
        'no_addison'       => 'no_addison',
        'no_extern'        => 'no_extern',
        'is_active'        => 'is_active',
        'cert_id'          => 'cert_id',
        'duration'         => 'duration',
        'cert_name'        => 'cert_name',
        'cert_desc'        => 'cert_desc',
        'task_all'         => 'task_all',
        'cert_group_id'    => 'cert_group_id',
        'cert_def_id'      => 'cert_def_id',
        'cert_group_sub'   => 'cert_group_sub',
        'cert_order'       => 'cert_order',
        'cert_active'      => 'cert_active',
        'cert_def_name'    => 'cert_def_name',
        'cert_def_def'     => 'cert_def_def',
        'cert_def_desc'    => 'cert_def_desc',
    ];

}
