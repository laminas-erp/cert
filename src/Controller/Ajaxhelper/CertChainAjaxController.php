<?php

namespace Lerp\Cert\Controller\Ajaxhelper;

use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Tablex\Cert\CertTablex;

/**
 *
 * @author bitkorn
 */
class CertChainAjaxController extends AbstractActionController
{
    protected Logger $logger;
    protected UserService $userService;
    protected CertTablex $certTablex;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setCertTablex(CertTablex $certTablex): void
    {
        $this->certTablex = $certTablex;
    }

    /**
     * @return ViewModel
     */
    public function certChainItemsFormAction(): ViewModel
    {
        $this->layout('layout/clean');
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserContainer()) {
            return $viewModel;
        }
        $certChainId = (int)$this->params('cert_chain_id');
        if (!$certChainId) {
            return $viewModel;
        }
        $certsForCertChain = $this->certTablex->getCertsForCertChain($certChainId);
        $viewModel->setVariable('certsForCertChain', $certsForCertChain);
        return $viewModel;
    }

}
