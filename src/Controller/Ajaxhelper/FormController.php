<?php

namespace Lerp\Cert\Controller\Ajaxhelper;

use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Laminas\View\Model\JsonModel;
use Lerp\Cert\Entity\Employee\EmployeeCertRelDocEntity;
use Lerp\Cert\Form\Employee\EmployeeCertRelForm;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelDocTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;
use Lerp\Cert\Tablex\Cert\CertTablex;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;
use Lerp\Cert\Tools\FolderSystem;
use Lerp\Cert\View\Helper\Employee\EmployeeCertRelViewHelper;

/**
 * Hier koennen ganze Formulare per URL Aufruf geholt werden.
 * @author bitkorn
 */
class FormController extends AbstractActionController
{
    protected string $docPathAbsolute = '';
    protected Logger $logger;
    protected UserService $userService;
    protected CertTablex $certTablex;
    protected EmployeeCertRelForm $employeeCertRelForm;
    protected EmployeeCertRelTable $employeeCertRelTable;
    protected EmployeeCertRelDocTable $employeeCertRelDocTable;
    protected CertTable $certTable;
    protected EmployeeTablex $employeeTablex;
    protected EmployeeCertRelViewHelper $employeeCertRelViewHelper;

    public function setDocPathAbsolute(string $docPathAbsolute): void
    {
        $this->docPathAbsolute = $docPathAbsolute;
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setCertTablex(CertTablex $certTablex): void
    {
        $this->certTablex = $certTablex;
    }

    public function setEmployeeCertRelForm(EmployeeCertRelForm $employeeCertRelForm): void
    {
        $this->employeeCertRelForm = $employeeCertRelForm;
    }

    public function setEmployeeCertRelTable(EmployeeCertRelTable $employeeCertRelTable): void
    {
        $this->employeeCertRelTable = $employeeCertRelTable;
    }

    public function setEmployeeCertRelDocTable(EmployeeCertRelDocTable $employeeCertRelDocTable): void
    {
        $this->employeeCertRelDocTable = $employeeCertRelDocTable;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setEmployeeTablex(EmployeeTablex $employeeTablex): void
    {
        $this->employeeTablex = $employeeTablex;
    }

    public function setEmployeecertRelViewHelper(EmployeeCertRelViewHelper $employeecertRelViewHelper): void
    {
        $this->employeecertRelViewHelper = $employeecertRelViewHelper;
    }

    /**
     *
     * In Abhaengigkeit (wegen CertDefValue) zum geklickten CertRel gibt es hier die Form fuer einen Eintrag in db.employee_cert_rel
     * @return ViewModel
     */
    public function employeeCertRelFormAddAction(): ViewModel
    {
        $this->layout('layout/clean');
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserContainer()) {
            return $viewModel;
        }

        $certId = (int)$this->params('cert_id');
        if (!$certId) {
            return $viewModel;
        }
        $certData = $this->getCertTablex()->getCertById($certId);
        if (empty($certData) || !is_array($certData)) {
            throw new \RuntimeException('Komplette EmployeeCertRelFormAdd nur mit gültiger cert_id!');
        }
        $viewModel->setVariable('certData', $certData);
        $certDefDef = json_decode($certData['cert_def_def'], true);
        if (null === $certDefDef || !is_array($certDefDef)) {
            throw new \RuntimeException('cert_def_def ist kein JSON');
        }
        $this->employeeCertRelForm->setCertData($certData);
        $this->employeeCertRelForm->init();
        $viewModel->setVariable('employeeCertRelForm', $this->employeeCertRelForm);

        return $viewModel;
    }

    public function employeeCertRelFormEditAction(): ViewModel
    {
        $this->layout('layout/clean');
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserContainer()) {
            return $viewModel;
        }

        $employeeCertRelId = (int)$this->params('employee_cert_rel_id');
        if (!$employeeCertRelId) {
            return $viewModel;
        }
        $employeeCertRelData = $this->employeeCertRelTable->getEmployeeCertRelById($employeeCertRelId);

        if (empty($employeeCertRelData) || !is_array($employeeCertRelData)) {
            throw new \RuntimeException('Komplette EmployeeCertRelFormEdit nur mit gültiger employee_cert_rel_id!');
        }
        $certData = $this->certTablex->getCertById($employeeCertRelData['employee_cert_rel_cert_id']);
        if (empty($certData)) {
            throw new \RuntimeException('Komplette EmployeeCertRelFormEdit nur mit gültiger employee_cert_rel_cert_id!');
        }
        $viewModel->setVariable('certData', $certData);
        $certDefDef = json_decode($certData['cert_def_def'], true);
        if (!is_array($certDefDef)) {
            throw new \RuntimeException('cert_def_def ist kein JSON');
        }
        $this->employeeCertRelForm->setCertData($certData);
        $this->employeeCertRelForm->setEmployeeCertRelCertDefValue($employeeCertRelData['cert_def_value']);
        $this->employeeCertRelForm->setEmployeeCertRelData($employeeCertRelData);
        $this->employeeCertRelForm->init();
        $viewModel->setVariable('employeeCertRelForm', $this->employeeCertRelForm);

        return $viewModel;
    }

    public function employeeCertRelFormDeleteAction(): ViewModel
    {
        $this->layout('layout/clean');
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserContainer()) {
            return $viewModel;
        }
        $employeeCertRelId = (int)$this->params('employee_cert_rel_id');
        if (!$employeeCertRelId) {
            return $viewModel;
        }
        $employeeCertRelData = $this->employeeCertRelTable->getEmployeeCertRelById($employeeCertRelId);

        if (empty($employeeCertRelData) || !is_array($employeeCertRelData)) {
            throw new \RuntimeException('Komplette EmployeeCertRelFormEdit nur mit gültiger employee_cert_rel_id!');
        }
        $viewModel->setVariable('employeeCertRelData', $employeeCertRelData);
        $certData = $this->certTablex->getCertById($employeeCertRelData['employee_cert_rel_cert_id']);
        if (empty($certData)) {
            throw new \RuntimeException('Komplette EmployeeCertRelFormEdit nur mit gültiger employee_cert_rel_cert_id!');
        }
        $viewModel->setVariable('certData', $certData);
        $certDefDef = json_decode($certData['cert_def_def'], true);
        if (!is_array($certDefDef)) {
            throw new \RuntimeException('cert_def_def ist kein JSON');
        }

        return $viewModel;
    }

    public function employeeCertRelFormDocumentsAction(): ViewModel
    {
        $this->layout('layout/clean');
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserContainer()) {
            return $viewModel;
        }
        $employeeCertRelId = (int)$this->params('employee_cert_rel_id');
        if (!$employeeCertRelId) {
            return $viewModel;
        }
        $employeeCertRelData = $this->employeeCertRelTable->getEmployeeCertRelById($employeeCertRelId);
        if (empty($employeeCertRelData) || !is_array($employeeCertRelData)) {
            throw new \RuntimeException('EmployeeCertRelFormDocument nur mit employee_cert_rel!');
        }
        $viewModel->setVariable('employeeCertRelData', $employeeCertRelData);
        $certDocs = $this->employeeCertRelDocTable->getEmployeeCertRelDocsByEmployeeCertRelId($employeeCertRelData['employee_cert_rel_id']);
        $viewModel->setVariable('certDocs', $certDocs);
        $certData = $this->certTable->getCertById($employeeCertRelData['employee_cert_rel_cert_id']);
        $viewModel->setVariable('certData', $certData);

        return $viewModel;
    }

    public function employeeCertRelUploadDocumentAction(): JsonModel
    {
        $this->layout('layout/clean');
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserContainer()) {
            return $jsonModel;
        }
        $employeeCertRelId = (int)$this->params('employee_cert_rel_id');
        if (!$employeeCertRelId) {
            return $jsonModel;
        }
        $employeeCertRel = $this->employeeCertRelTable->getEmployeeCertRelById($employeeCertRelId);
        $request = $this->getRequest();
        if ($request->isPost()) {
            if (isset($_FILES['employee_cert_rel_files'])) {
                $fileCount = count($_FILES['employee_cert_rel_files']['tmp_name']);
                $timestamp = time();
                $employeecertrelMainFoilderRoot = 'EmployeeCertRel';
                $folderSystem = new FolderSystem($this->logger, $this->docPathAbsolute,
                    $employeecertrelMainFoilderRoot);
                for ($i = 0; $i < $fileCount; $i++) {
                    if (strrpos($_FILES['employee_cert_rel_files']['name'][$i], '.')) {
                        $extDotPos = strrpos($_FILES['employee_cert_rel_files']['name'][$i], '.') + 1;
                        $folderSystem->setFileExtension(substr($_FILES['employee_cert_rel_files']['name'][$i], $extDotPos));
                    }
                    $fileTimestamp = $timestamp + $i;
                    $folderSystem->setDatetime($fileTimestamp);
                    $fqfn = $folderSystem->computeFqfnDateAndPres();
                    if (!move_uploaded_file($_FILES['employee_cert_rel_files']['tmp_name'][$i], $fqfn)) {
                        throw new \RuntimeException('error while file upload');
                    }
                    $employeeCertRelDocEntity = new EmployeeCertRelDocEntity();
                    $employeeCertRelDocEntity->exchangeArrayFromDatabase([
                        'employee_cert_rel_doc_name'             => $_FILES['employee_cert_rel_files']['name'][$i],
                        'employee_cert_rel_id'                   => $employeeCertRel['employee_cert_rel_id'],
                        'employee_cert_rel_doc_main_folder_root' => $employeecertrelMainFoilderRoot,
                        'employee_cert_rel_doc_datetime'         => $fileTimestamp,
                        'employee_cert_rel_doc_filename'         => $folderSystem->getFilename(),
                        'employee_cert_rel_doc_fqfn'             => $fqfn,
                        'cert_id'                                => $employeeCertRel['employee_cert_rel_cert_id'],
                        'employee_id'                            => $employeeCertRel['employee_id'],
                    ]);
                    if (!$employeeCertRelDocEntity->save($this->getEmployeeCertRelDocTable())) {
                        throw new \RuntimeException('database error while file upload');
                    }
                }
            }
        }
        $employeeCerts = $this->employeeTablex->getEmployeeCertRels($employeeCertRel['employee_id']);
        $employeecertRelHtml = $this->employeecertRelViewHelper($employeeCertRel['employee_id'], $employeeCerts,
            $employeeCertRel['employee_cert_rel_cert_id'], true);
        $this->responseArray['employeecertRelHtml'] = htmlentities($employeecertRelHtml);
        $this->responseArray['message'] = 'update success';
        return new JsonModel($this->responseArray);
    }

    /**
     * Gibt eine Liste der Employees die noch nicht in der Gruppe sind.
     * @return ViewModel
     */
    public function employeeGroupRelFormAddAction(): ViewModel
    {
        $this->layout('layout/clean');
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserContainer()) {
            return $viewModel;
        }
        $employeeGroupId = $this->params('employee_group_id');
        if (!isset($employeeGroupId)) {
            return $viewModel;
        }
        $employees = $this->employeeTablex->getEmployeesWhereNotInGroup($employeeGroupId);
        $viewModel->setVariable('employees', $employees);
        return $viewModel;
    }

}
