<?php

namespace Lerp\Cert\Controller\Ajaxhelper;

use Bitkorn\User\Service\UserService;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\View\Helper\Element\Employee\EmployeeCertRelCertDefValueElement;

/**
 *
 * @author bitkorn
 */
class FormElementController extends AbstractActionController
{
    protected UserService $userService;
    protected EmployeeCertRelCertDefValueElement $employeeCertRelCertDefValueElement;

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    /**
     * In \Lerp\Cert\Form\Employee\EmployeeCertRelForm fehlt das Element zu db.employee_cert_rel.cert_def_value,
     * darum kann es hier in Anhaengigkeit zum selektierten Cert erstellt werden.
     * @return ViewModel
     */
    public function certRelCertDefValueSelectAction(): ViewModel
    {
        $this->layout('layout/clean');
        $viewModel = new ViewModel();
        if (!$this->userService->checkUserContainer()) {
            return $viewModel;
        }
        $certId = (int)$this->params('cert_id');
        if (!$certId) {
            return $viewModel;
        }
        $employeeCertDefValueElement = $this->employeeCertRelCertDefValueElement($certId);
        $viewModel->setVariable('employeeCertDefValueElement', $employeeCertDefValueElement);;
        return $viewModel;
    }

}
