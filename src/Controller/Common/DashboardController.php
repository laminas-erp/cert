<?php

namespace  Lerp\Cert\Controller\Common;

use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;
use Lerp\Cert\Table\Employee\EmployeeTable;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;

/**
 *
 * @author bitkorn
 */
class DashboardController extends AbstractActionController
{
    protected Logger $logger;
    protected UserService $userService;
    protected EmployeeTablex $employeeTablex;
    protected EmployeeCertRelTable $employeeCertRelTable;
    protected CertTable $certTable;
    protected EmployeeTable $employeeTable;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setEmployeeTablex(EmployeeTablex $employeeTablex): void
    {
        $this->employeeTablex = $employeeTablex;
    }

    public function setEmployeeCertRelTable(EmployeeCertRelTable $employeeCertRelTable): void
    {
        $this->employeeCertRelTable = $employeeCertRelTable;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setEmployeeTable(EmployeeTable $employeeTable): void
    {
        $this->employeeTable = $employeeTable;
    }

    /**
     * Overwrite it for individual dashboard, e.g. in Application module.
     * In /config/application.config.php the next entry in 'modules' overwrites the previous one!
     * 
     * 1. aktualisiert db.employee_cert_rel.employee_cert_rel_remaining_time_days
     * 2. existierende EmployeeCertRels ...demnaechst ablaufende zuerst
     * 3. unerfuellte Gruppen-Pflichten
     * 4. unerfuellte Pflichten Aller
     * @return ViewModel
     */
    public function indexAction()
    {
        if(!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        if($this->employeeTablex->callProcUpdateEmployeeCertRelRemainingTimeDays() < 0) {
            $this->logger->err('callProcUpdateEmployeeCertRelRemainingTimeDays() gab den Wert < 0 zurück');
        }
        $viewModel = new ViewModel();
        $maxRemainingDays = (int) $this->params()->fromQuery('max-remaining-days', 300);
        $viewModel->setVariable('maxRemainingDays', $maxRemainingDays);
        $certRels = $this->employeeTablex->getCertRels($maxRemainingDays);
        $viewModel->setVariable('certRels', $certRels);

        /*
         * CertRels
         */
        $employeeCertRelIdAssocExt = $this->employeeCertRelTable->getEmployeeCertRelIdAssocExt();
        $viewModel->setVariable('employeeCertRelIdAssocExt', $employeeCertRelIdAssocExt);
        /*
         * Gruppenpflichten
         */
        $employeeGroupCertTasksIdAssocExtCertIdEmployeeId = $this->employeeTablex->getEmployeeGroupCertTasksIdAssocExtCertIdEmployeeId();
        $viewModel->setVariable('employeeGroupCertTasksIdAssocExtCertIdEmployeeId', $employeeGroupCertTasksIdAssocExtCertIdEmployeeId);
        
        /*
         * Pflichten Aller
         */
        $certsTaskAllIdAssoc = $this->certTable->getCertsIdAssocTaskAll();
        $viewModel->setVariable('certsTaskAllIdAssoc', $certsTaskAllIdAssoc);
        $employees = $this->employeeTable->getEmployees();
        $viewModel->setVariable('employees', $employees);
        
        return $viewModel;
    }

}
