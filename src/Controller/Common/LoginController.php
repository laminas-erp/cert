<?php

namespace  Lerp\Cert\Controller\Common;

use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author bitkorn
 */
class LoginController extends AbstractActionController
{
    protected Logger $logger;
    protected UserService $userService;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    /**
     * 
     * @return ViewModel
     * @todo den UserService Login machen
     */
    public function loginAction()
    {
        if ($this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_dashboard');
        }
        $viewModel = new ViewModel();

        $request = $this->getRequest();
        $loggedIn = false;
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            if (isset($postData['login']) && isset($postData['passwd'])) {
                if($this->userService->canLogin($postData['login'], $postData['passwd'])){
                    $loggedIn = true;
                }
            }
        }
        $viewModel->setVariable('loggedin', $loggedIn);
        return $viewModel;
    }
}
