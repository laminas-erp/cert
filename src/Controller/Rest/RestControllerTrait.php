<?php

namespace Lerp\Cert\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;

trait RestControllerTrait
{
    protected array $responseArray = [];
    protected Logger $logger;
    protected UserService $userService;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

}
