<?php

namespace  Lerp\Cert\Controller\Rest;

use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Laminas\Http\Response;
use Lerp\Cert\Tablex\Cert\CertTablex;

/**
 * 
 * @author allapow
 */
class CertRestController extends AbstractRestfulController
{
    use RestControllerTrait;
    protected CertTablex $certTablex;

    public function setCertTablex(CertTablex $certTablex): void
    {
        $this->certTablex = $certTablex;
    }

    public function get($id)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return new JsonModel($this->responseArray);
        }
        $this->responseArray['id'] = $id;
        $cert = $this->certTablex->getCertById($id);
        $cert['cert_def_def'] = json_decode($cert['cert_def_def'], true);
        $this->responseArray['cert'] = $cert;
        return new JsonModel($this->responseArray);
    }

    public function getList()
    {
        return new JsonModel($this->responseArray);
    }

}
