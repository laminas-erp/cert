<?php

namespace Lerp\Cert\Controller\Rest;

use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Laminas\Http\Response;
use Lerp\Cert\Observer\ObserverManager;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;
use Lerp\Cert\Tablex\Cert\CertTablex;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;
use Lerp\Cert\View\Helper\Employee\EmployeeCertRelViewHelper;

/**
 *
 * @author allapow
 */
class EmployeeCertRelRestController extends AbstractRestfulController
{
    use RestControllerTrait;

    protected EmployeeCertRelViewHelper $employeecertRelViewHelper;
    protected EmployeeCertRelTable $employeeCertRelTable;
    protected CertTablex $certTablex;
    protected EmployeeTablex $employeeTablex;

    public function setEmployeecertRelViewHelper(EmployeeCertRelViewHelper $employeecertRelViewHelper): void
    {
        $this->employeecertRelViewHelper = $employeecertRelViewHelper;
    }

    public function setEmployeeCertRelTable(EmployeeCertRelTable $employeeCertRelTable): void
    {
        $this->employeeCertRelTable = $employeeCertRelTable;
    }

    public function setCertTablex(CertTablex $certTablex): void
    {
        $this->certTablex = $certTablex;
    }

    public function setEmployeeTablex(EmployeeTablex $employeeTablex): void
    {
        $this->employeeTablex = $employeeTablex;
    }

    /**
     * GET
     * @param int $id
     * @return JsonModel
     */
    public function get($id)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return new JsonModel($this->responseArray);
        }
        $this->responseArray['id'] = $id;
        $employeeCertRel = $this->employeeCertRelTable->getEmployeeCertRelById($id);
        $this->responseArray['employeeCertRel'] = $employeeCertRel;
        return new JsonModel($this->responseArray);
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList()
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return new JsonModel($this->responseArray);
        }
        return new JsonModel($this->responseArray);
    }

    /**
     * POST maps to create().
     * The response should typically be an HTTP 201 response
     * with the Location header indicating the URI of the newly created entity
     * and the response body providing the representation.
     * @param array $data
     * @return JsonModel property id with last insert id, 0 if error
     */
    public function create($data)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return new JsonModel($this->responseArray);
        }
        if (!isset($data['certrelDetails'])) {
            $this->responseArray['message'] = 'so nich: certrelDetails erforderlich';
            return new JsonModel($this->responseArray);
        }
        $employeeId = (int)$data['certrelDetails']['employeeId'];
        $certId = (int)$data['certrelDetails']['certId'];
        $certDefValue = filter_var($data['certrelDetails']['certDefValue'], FILTER_SANITIZE_STRING);
        $dateOfIssue = filter_var($data['certrelDetails']['dateOfIssue'], FILTER_SANITIZE_STRING);
        if (!empty($dateOfIssue)) {
            try {
                $dateTime = new \DateTime($dateOfIssue);
                $dateOfIssueTimestamp = $dateTime->getTimestamp();
            } catch (\Exception $e) {
                $this->logger->err($e->getMessage());
                return new JsonModel($this->responseArray);
            }
        }
        $licenceNo = filter_var($data['certrelDetails']['licenceNo'], FILTER_SANITIZE_STRING);
        $certRestriction = filter_var($data['certrelDetails']['certRestriction'], FILTER_SANITIZE_STRING);

        /**
         * Falls es schon eins gab (replace) wirds hier archiviert
         */
        $observerManager = ObserverManager::getInstance();
        $observerManager->trigger('employeeCertRel.add', ['employee_cert_rel_cert_id' => $certId, 'employee_id' => $employeeId]);

        if (!$this->employeeCertRelTable->insertEmployeeCertRel([
            'employee_cert_rel_cert_id' => $certId,
            'employee_id'               => $employeeId,
            'date_of_issue'             => $dateOfIssueTimestamp,
            'licence_no'                => $licenceNo,
            'cert_def_value'            => $certDefValue,
            'cert_restriction'          => $certRestriction,
        ])) {
            $this->responseArray['message'] = 'Fehler beim Speichern des Mitarbeiter Zertifikats!';
            return new JsonModel($this->responseArray);
        }

        $certChainTypes = $this->certTablex->existCertChainForCert($certId);
        $affectedRows = $this->certTablex->computeCertChainTypesWithCertIdForEmployee($certChainTypes, $employeeId, $this->employeeCertRelTable);

        if ($affectedRows == 0) {
            $employeeCerts = $this->employeeTablex->getEmployeeCertRels($employeeId);
//                $employeecertRelHtml = $this->employeecertRelViewHelper($employeeId, $employeeCerts, $certId, true);
            $employeecertRelHtml = call_user_func($this->employeecertRelViewHelper, $employeeId, $employeeCerts, $certId, true);
            $this->responseArray['employeecertRelHtml'] = htmlentities($employeecertRelHtml);
        } else {
            $this->responseArray['reload'] = $affectedRows;
        }
        $this->responseArray['message'] = 'save success';
        return new JsonModel($this->responseArray);
    }

    /**
     * PUT maps to update().
     * Return either a 200 or 202 response status, as well as the representation of the entity.
     * @param int $id Query/URL Parameter
     * @param array $data Data in request body
     * @return JsonModel id and result property; result is 1 if success.
     */
    public function update($id, $data)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return new JsonModel($this->responseArray);
        }
        if (!isset($data['certrelDetails'])) {
            $this->responseArray['message'] = 'so nich: certrelDetails erforderlich';
            return new JsonModel($this->responseArray);
        }
        $employeeCertRelId = (int)$id;
        $employeeId = (int)$data['certrelDetails']['employeeId'];
        $certId = (int)$data['certrelDetails']['certId'];
        $certDefValue = filter_var($data['certrelDetails']['certDefValue'], FILTER_SANITIZE_STRING);
        $dateOfIssue = filter_var($data['certrelDetails']['dateOfIssue'], FILTER_SANITIZE_STRING);
        if (!empty($dateOfIssue)) {
            try {
                $dateTime = new \DateTime($dateOfIssue);
                $dateOfIssueTimestamp = $dateTime->getTimestamp();
            } catch (\Exception $e) {
                $this->logger->err($e->getMessage());
                return new JsonModel($this->responseArray);
            }
        }
        $licenceNo = filter_var($data['certrelDetails']['licenceNo'], FILTER_SANITIZE_STRING);
        $certRestriction = filter_var($data['certrelDetails']['certRestriction'], FILTER_SANITIZE_STRING);

        if (!is_int($this->employeeCertRelTable->updateEmployeeCertRel([
            'employee_cert_rel_id'      => $employeeCertRelId,
            'employee_cert_rel_cert_id' => $certId,
            'employee_id'               => $employeeId,
            'date_of_issue'             => $dateOfIssueTimestamp,
            'licence_no'                => $licenceNo,
            'cert_def_value'            => $certDefValue,
            'cert_restriction'          => $certRestriction,
        ]))) {
            $this->responseArray['message'] = 'Fehler beim Speichern des Mitarbeiter Zertifikats!';
            return new JsonModel($this->responseArray);
        }

        $certChainTypes = $this->certTablex->existCertChainForCert($certId);
        $affectedRows = $this->certTablex->computeCertChainTypesWithCertIdForEmployee($certChainTypes, $employeeId, $this->employeeCertRelTable);

        if ($affectedRows == 0) {
            $employeeCerts = $this->employeeTablex->getEmployeeCertRels($employeeId);
//            $employeecertRelHtml = $this->employeecertRelViewHelper($employeeId, $employeeCerts, $certId, true);
            $employeecertRelHtml = call_user_func($this->employeecertRelViewHelper, $employeeId, $employeeCerts, $certId, true);
            $this->responseArray['employeecertRelHtml'] = htmlentities($employeecertRelHtml);
        } else {
            $this->responseArray['reload'] = $affectedRows;
        }
        $this->responseArray['message'] = 'update success';
        return new JsonModel($this->responseArray);
    }

    /**
     * DELETE maps to delete().
     * Return either a 200 or 204 response status.
     * @param int $id
     * @return JsonModel id and result property; result is 1 if success.
     */
    public function delete($id)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return new JsonModel($this->responseArray);
        }
        $employeeCertRelId = (int)$id;
        $employeeCertRelData = $this->employeeCertRelTable->getEmployeeCertRelById($employeeCertRelId);
        if (!$employeeCertRelData || !$this->employeeCertRelTable->delete(['employee_cert_rel_id' => $employeeCertRelId])) {
            $this->responseArray['message'] = 'An error occurred while delete employee cert relation.';
            return new JsonModel($this->responseArray);
        }

        $employeeCerts = $this->employeeTablex->getEmployeeCertRels($employeeCertRelData['employee_id']);
//        $employeecertRelHtml = $this->employeecertRelViewHelper($employeeCertRelData['employee_id'], $employeeCerts,
//            $employeeCertRelData['employee_cert_rel_cert_id'], true);
        $employeecertRelHtml = call_user_func($this->employeecertRelViewHelper, $employeeCertRelData['employee_id'], $employeeCerts,
            $employeeCertRelData['employee_cert_rel_cert_id'], true);
        $this->responseArray['employeecertRelHtml'] = htmlentities($employeecertRelHtml);
        $this->responseArray['message'] = 'delete success';
        return new JsonModel($this->responseArray);
    }

}
