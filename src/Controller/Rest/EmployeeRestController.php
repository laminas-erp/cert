<?php

namespace Lerp\Cert\Controller\Rest;

use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Lerp\Cert\Table\Employee\EmployeeTable;

class EmployeeRestController extends AbstractRestfulController
{
    use RestControllerTrait;

    protected EmployeeTable $employeeTable;

    public function setEmployeeTable(EmployeeTable $employeeTable): void
    {
        $this->employeeTable = $employeeTable;
    }

    public function get($id)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            return new JsonModel($this->responseArray);
        }
        $this->responseArray['id'] = $id;
        $employee = $this->employeeTable->getEmployeeById($id);
        $this->responseArray['employee'] = $employee;
        return new JsonModel($this->responseArray);
    }

    public function getList()
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            return new JsonModel($this->responseArray);
        }
        return new JsonModel($this->responseArray);
    }

}
