<?php

namespace  Lerp\Cert\Controller\Rest;

use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Laminas\Http\Response;
use Lerp\Cert\Table\Cert\CertTableTdTable;

class CertTableTdRestController extends AbstractRestfulController
{
    use RestControllerTrait;
    protected CertTableTdTable $certTableTdTable;

    public function setCertTableTdTable(CertTableTdTable $certTableTdTable): void
    {
        $this->certTableTdTable = $certTableTdTable;
    }

    public function get($id)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return new JsonModel($this->responseArray);
        }
        $this->responseArray['id'] = $id;
        $certTableTd = $this->certTableTdTable->getCertTableTdById($id);
        $this->responseArray['certTableTd'] = $certTableTd;
        return new JsonModel($this->responseArray);
    }

    public function getList()
    {
        return new JsonModel($this->responseArray);
    }

    /**
     * 
     * @param int $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return new JsonModel($this->responseArray);
        }
        $this->responseArray['id'] = $id;
        $this->responseArray['certId'] = $data['certTableTdData']['certId'];
        if (!isset($data['certTableTdData'])) {
            $this->responseArray['message'] = 'Missing certTableTdData.';
            return new JsonModel($this->responseArray);
        }
        $dbCertId = $data['certTableTdData']['certId'] == 0 ? null : $data['certTableTdData']['certId'];
        $this->responseArray['updateResult'] = $this->certTableTdTable->updateCertTableTd(['cert_table_td_id' => $id, 'cert_id' => $dbCertId]);
        return new JsonModel($this->responseArray);
    }

}
