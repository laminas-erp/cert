<?php

namespace Lerp\Cert\Controller\Rest;

use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Lerp\Cert\Entity\Employee\EmployeeCertRelDocEntity;
use Lerp\Cert\Table\Employee\EmployeeCertRelDocTable;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;
use Lerp\Cert\View\Helper\Employee\EmployeeCertRelViewHelper;

/**
 * UNUSED!!!!!!!!!!!!!!!!!!!!!!!!
 *
 *
 * @author allapow
 */
class EmployeeCertRelDocRestController extends AbstractRestfulController
{
    use RestControllerTrait;

    protected array $lerpCertConfig = [];
    protected EmployeeCertRelDocTable $employeeCertRelDocTable;
    protected EmployeeCertRelViewHelper $employeecertRelViewHelper;
    protected EmployeeTablex $employeeTablex;

    public function setLerpCertConfig(array $lerpCertConfig): void
    {
        $this->lerpCertConfig = $lerpCertConfig;
    }

    public function setEmployeeCertRelDocTable(EmployeeCertRelDocTable $employeeCertRelDocTable): void
    {
        $this->employeeCertRelDocTable = $employeeCertRelDocTable;
    }

    public function setEmployeecertRelViewHelper(EmployeeCertRelViewHelper $employeecertRelViewHelper): void
    {
        $this->employeecertRelViewHelper = $employeecertRelViewHelper;
    }

    public function setEmployeeTablex(EmployeeTablex $employeeTablex): void
    {
        $this->employeeTablex = $employeeTablex;
    }

    /**
     * GET
     * @param int $id
     * @return JsonModel
     */
    public function get($id)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            return new JsonModel($this->responseArray);
        }
        $this->responseArray['id'] = $id;
        return new JsonModel($this->responseArray);
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList()
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            return new JsonModel($this->responseArray);
        }
        return new JsonModel($this->responseArray);
    }

    /**
     * POST maps to create().
     * The response should typically be an HTTP 201 response
     * with the Location header indicating the URI of the newly created entity
     * and the response body providing the representation.
     * @param array $data
     * @return JsonModel property id with last insert id, 0 if error
     */
    public function create($data)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            return new JsonModel($this->responseArray);
        }
        return new JsonModel($this->responseArray);
    }

    /**
     * PUT maps to update().
     * Return either a 200 or 202 response status, as well as the representation of the entity.
     * @param int $id Query/URL Parameter
     * @param array $data Data in request body
     * @return JsonModel id and result property; result is 1 if success.
     */
    public function update($id, $data)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            return new JsonModel($this->responseArray);
        }
        return new JsonModel($this->responseArray);
    }

    /**
     * DELETE maps to delete().
     * Return either a 200 or 204 response status.
     * @param int $id
     * @return JsonModel id and result property; result is 1 if success.
     */
    public function delete($id)
    {
        if (!$this->userService->checkUserContainer()) {
            $this->responseArray['message'] = 'You does not have permission for this url.';
            return new JsonModel($this->responseArray);
        }

        $employeeCertRelDocData = $this->employeeCertRelDocTable->getEmployeeCertRelDocById($id);
        EmployeeCertRelDocEntity::delete($this->employeeCertRelDocTable, $id, $this->lerpCertConfig['doc_path_absolute']);

        $employeeCerts = $this->employeeTablex->getEmployeeCertRels($employeeCertRelDocData['employee_id']);
//        $employeecertRelHtml = $this->employeecertRelViewHelper($employeeCertRelDocData['employee_id'], $employeeCerts, $employeeCertRelDocData['cert_id'], true);
        $employeecertRelHtml = call_user_func($this->employeecertRelViewHelper, $employeeCertRelDocData['employee_id'], $employeeCerts, $employeeCertRelDocData['cert_id'], true);
        $this->responseArray['employeecertRelHtml'] = htmlentities($employeecertRelHtml);
        $this->responseArray['message'] = 'delete success';
        return new JsonModel($this->responseArray);
    }

}
