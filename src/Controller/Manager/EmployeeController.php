<?php

namespace Lerp\Cert\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Cert\CertEvent;
use Lerp\Cert\Form\Employee\EmployeeForm;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeCertTableGroupAttributeDefTable;
use Lerp\Cert\Table\Employee\EmployeeTable;
use Lerp\Cert\Tablex\Employee\EmployeeCertTableGroupAttributeTablex;

/**
 *
 * @author bitkorn
 */
class EmployeeController extends AbstractActionController
{
    protected Logger $logger;
    protected UserService $userService;
    protected EmployeeForm $employeeForm;
    protected EmployeeTable $employeeTable;
    protected CertTable $certTable;
    protected EmployeeCertTableGroupAttributeTablex $employeeCertTableGroupAttributeTablex;
    protected EmployeeCertTableGroupAttributeDefTable $employeeCertTableGroupAttributeDefTable;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setEmployeeForm(EmployeeForm $employeeForm): void
    {
        $this->employeeForm = $employeeForm;
    }

    public function setEmployeeTable(EmployeeTable $employeeTable): void
    {
        $this->employeeTable = $employeeTable;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setEmployeeCertTableGroupAttributeTablex(EmployeeCertTableGroupAttributeTablex $employeeCertTableGroupAttributeTablex): void
    {
        $this->employeeCertTableGroupAttributeTablex = $employeeCertTableGroupAttributeTablex;
    }

    public function setEmployeeCertTableGroupAttributeDefTable(EmployeeCertTableGroupAttributeDefTable $employeeCertTableGroupAttributeDefTable): void
    {
        $this->employeeCertTableGroupAttributeDefTable = $employeeCertTableGroupAttributeDefTable;
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function employeesAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $data = $this->employeeTable->getEmployees();
        $this->getEventManager()->trigger(CertEvent::EVENT_EMPLOYEE_CERT_REL_BEFORE, null, ['user_id']);
        return new ViewModel([
            'data' => $data,
        ]);
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function employeeAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $employeeId = (int)$this->params('employee_id');
        if (!$employeeId) {
            return $this->redirect()->toRoute('lerp_cert_employee_employees');
        }
        $data = $this->employeeTable->getEmployeeById($employeeId);
        $this->employeeForm->setData($data);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            unset($postData['submit']);
            $this->employeeForm->setData($postData);
            if ($this->employeeForm->isValid()) {
                $formData = $this->employeeForm->getData();
                unset($formData['submit']);
                if ($this->employeeTable->updateEmployee($formData) > 0) {
                    $this->layout()->setVariable('message', ['level' => 'info', 'text' => 'Die &Auml;nderungen wurden gespeichert']);
                }
            }
        } // isPost
        return new ViewModel([
            'employeeForm' => $this->employeeForm,
        ]);
    }

    /**
     * show and edit all employee certs
     * @return Response|ViewModel
     */
    public function employeeCertsAllAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }

        $employeeId = (int)$this->params('employee_id');
        if (!$employeeId) {
            return $this->redirect()->toRoute('lerp_cert_cert_certs');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
        } // isPost

        return new ViewModel([
            'employeeId'  => $employeeId,
            'certIdAssoc' => $this->certTable->getCertsIdAssoc(),
        ]);
    }

    /**
     * Mit cert_table_group_id & employee_id
     * und dem ViewHelper releaseAndWriteoffTableGroup
     * Zertifikate anzeigen
     * @return Response|ViewModel
     */
    public function employeeCertsTableGroupAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }

        $certTableGroupId = (int)$this->params('cert_table_group_id');
        $employeeId = (int)$this->params('employee_id');
        if (!$certTableGroupId || !$employeeId) {
            return $this->redirect()->toRoute('lerp_cert_employee_employees');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('employeeId', $employeeId);
        $viewModel->setVariable('certTableGroupId', $certTableGroupId);
        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['employee_cert_table_group_attribute_submit'])) {
                $employeeCertTableGroupAttributeDefId = (int)$postData['employee_cert_table_group_attribute_def_id'];
                $employeeCertTableGroupAttributeValue = filter_var($postData['employee_cert_table_group_attribute_value'], FILTER_SANITIZE_STRING);
                if ($postData['employee_cert_table_group_attribute_def_type'] == 'time') {

                    try {
                        $dateTime = new \DateTime($employeeCertTableGroupAttributeValue);
                        $employeeCertTableGroupAttributeValue = $dateTime->getTimestamp();
                    } catch (\Exception $e) {
                        $this->logger->err($e->getMessage());
                        return $viewModel;
                    }
                }
                /**
                 * @todo implement some other employee_cert_table_group_attribute_def_type HTML form-input
                 */
                $this->employeeCertTableGroupAttributeTablex->saveOrUpdateEmployeeCertTableGroupAttribute($employeeCertTableGroupAttributeDefId,
                    $employeeId, $employeeCertTableGroupAttributeValue, $this->getEmployeeCertTableGroupAttributeRelTable());
            }
        }

        $tableGroupAttributes = $this->employeeCertTableGroupAttributeDefTable->getEmployeeCertTableGroupAttributeDefsForTableGroup($certTableGroupId);
        $employeeCertTableGroupAttributesIdAssoc = $this->employeeCertTableGroupAttributeTablex->getEmployeeCertTableGroupAttributesForEmployeeDefIdIdAssoc($certTableGroupId,
            $employeeId);
        $viewModel->setVariable('tableGroupAttributes', $tableGroupAttributes);
        $viewModel->setVariable('employeeCertTableGroupAttributesIdAssoc', $employeeCertTableGroupAttributesIdAssoc);
        return $viewModel;
    }

}
