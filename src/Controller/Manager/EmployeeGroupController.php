<?php

namespace Lerp\Cert\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeGroupCertTaskTable;
use Lerp\Cert\Table\Employee\EmployeeGroupRelTable;
use Lerp\Cert\Table\Employee\EmployeeGroupTable;
use Lerp\Cert\Tablex\Employee\EmployeeGroupTablex;

/**
 *
 * @author bitkorn
 */
class EmployeeGroupController extends AbstractActionController
{
    protected Logger $logger;
    protected UserService $userService;
    protected EmployeeGroupCertTaskTable $employeeGroupCertTaskTable;
    protected CertTable $certTable;
    protected EmployeeGroupTable $employeeGroupTable;
    protected EmployeeGroupTablex $employeeGroupTablex;
    protected EmployeeGroupRelTable $employeeGroupRelTable;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setEmployeeGroupCertTaskTable(EmployeeGroupCertTaskTable $employeeGroupCertTaskTable): void
    {
        $this->employeeGroupCertTaskTable = $employeeGroupCertTaskTable;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setEmployeeGroupTable(EmployeeGroupTable $employeeGroupTable): void
    {
        $this->employeeGroupTable = $employeeGroupTable;
    }

    public function setEmployeeGroupTablex(EmployeeGroupTablex $employeeGroupTablex): void
    {
        $this->employeeGroupTablex = $employeeGroupTablex;
    }

    public function setEmployeeGroupRelTable(EmployeeGroupRelTable $employeeGroupRelTable): void
    {
        $this->employeeGroupRelTable = $employeeGroupRelTable;
    }

    /**
     * Gruppen-Pflichten
     * @return Response|ViewModel
     */
    public function employeeGroupCertTasksAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $viewModel = new ViewModel();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['add_group_task'])) {
                $certId = (int)$postData['cert_id'];
                $employeeGroupId = (int)$postData['employee_group_id'];
                if (!empty($certId) && !empty($employeeGroupId)) {
                    if (!$this->employeeGroupCertTaskTable->existEmployeeGroupCertTask($employeeGroupId, $certId)) {
                        $this->employeeGroupCertTaskTable->insertEmployeeGroupCertTask($employeeGroupId, $certId);
                    }
                } else {
                    $viewModel->setVariable('inputErrorClass', 'border-risky');
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text'  => 'Zertifikat UND Gruppe müssen gewählt werden.',
                    ];
                }
            } elseif (!empty($postData['delete_group_task'])) {
                $employeeGroupCertTaskId = (int)$postData['employee_group_cert_task_id'];
                if (!empty($employeeGroupCertTaskId)) {
                    $this->employeeGroupCertTaskTable->deleteEmployeeGroupCertTask($employeeGroupCertTaskId);
                }
            }
        }

        $certsIdAssoc = $this->certTable->getCertsIdAssoc();
        $viewModel->setVariable('certsIdAssoc', $certsIdAssoc);

        $employeeGroupsIdAssoc = $this->employeeGroupTable->getEmployeeGroupsIdAssoc();
        $viewModel->setVariable('employeeGroupsIdAssoc', $employeeGroupsIdAssoc);

        $employeeGroupCertTasks = $this->employeeGroupTablex->getEmployeeGroupCertTasks();
        $viewModel->setVariable('employeeGroupCertTasks', $employeeGroupCertTasks);
        return $viewModel;
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function employeeGroupsAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $viewModel = new ViewModel();
        $employeeGroups = $this->employeeGroupTablex->getEmployeeGroups();
        $viewModel->setVariable('employeeGroups', $employeeGroups);
        return $viewModel;
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function employeeGroupAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $viewModel = new ViewModel();
        $employeeGroupId = $this->params('employee_group_id');
        if (!isset($employeeGroupId)) {
            return $this->redirect()->toRoute('lerp_cert_manage_employeegroup_employeegroups');
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['employeeforgroup_submit']) && is_array($postData['employeeforgroup'])) {
                foreach ($postData['employeeforgroup'] as $employeeId) {
                    $employeeId = (int)$employeeId;
                    if (!$this->employeeGroupRelTable->existEmployeeGroupRel($employeeGroupId, $employeeId)) {
                        $this->employeeGroupRelTable->insertEmployeeGroupRel(['employee_group_id' => $employeeGroupId, 'employee_id' => $employeeId]);
                    }
                }
            } elseif (isset($postData['remove_employee_from_group']) && !empty($postData['employee_id'])) {
                $employeeId = (int)$postData['employee_id'];
                $this->employeeGroupRelTable->deleteEmployeeGroupRel($employeeGroupId, $employeeId);
            }
        }

        $employeeGroup = $this->employeeGroupTable->getEmployeeGroupById($employeeGroupId);
        $viewModel->setVariable('employeeGroup', $employeeGroup);

        $employeeGroupRels = $this->employeeGroupTablex->getEmployeeGroupRelsByEmployeeGroupId($employeeGroupId);
        $viewModel->setVariable('employeeGroupRels', $employeeGroupRels);

        return $viewModel;
    }

}
