<?php

namespace  Lerp\Cert\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

/**
 * Description of PortableDocumentController
 *
 * @author allapow
 */
class PortableDocumentController extends AbstractActionController
{
    protected array $lerpCertConfig = [];
    protected Logger $logger;
    protected UserService $userService;

    public function setLerpCertConfig(array $lerpCertConfig): void
    {
        $this->lerpCertConfig = $lerpCertConfig;
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function tableGroupAction()
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $employeeId = (int) $this->params('employee_id');
        $certTableGroupId = (int) $this->params('cert_table_group_id');
//        $employeeCertRels = $this->getCertTableTablex()->getCertTableTdsWithEmployeeCertRels($certTableGroupId, $employeeId);
//        \Lerp\Cert\PortableDocument\TableGroupPdf::generateTableGroupPdf($employeeCertRels, 'l');
        $command = 'java -jar ' . $this->lerpCertConfig['fqn_javajar_pdf'] . ' tablegroup ' . $certTableGroupId . ' ' . $employeeId;
        exec($command);
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="Tablegroup.pdf"');
        header('Content-length: ' . filesize($this->lerpCertConfig['tmp_doc_path_absolute'] . '/lerp_cert_tablegroup.pdf'));
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        $resource = fopen('php://output', 'w');
        fwrite($resource, file_get_contents($this->lerpCertConfig['tmp_doc_path_absolute'] . '/lerp_cert_tablegroup.pdf'));
        exit();
    }

}
