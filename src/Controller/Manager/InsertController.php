<?php

namespace Lerp\Cert\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Entity\Cert\CertTableEntity;
use Lerp\Cert\Entity\Cert\CertTableGroupEntity;
use Lerp\Cert\Form\Cert\CertForm;
use Lerp\Cert\Form\Cert\CertTableForm;
use Lerp\Cert\Form\Cert\CertTableGroupForm;
use Lerp\Cert\Form\Employee\EmployeeCertRelForm;
use Lerp\Cert\Form\Employee\EmployeeForm;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Cert\CertTableGroupTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;
use Lerp\Cert\Table\Employee\EmployeeTable;
use Lerp\Cert\Tablex\Cert\CertTableTablex;
use Lerp\Cert\Tools\CertDuration;

/**
 *
 * @author bitkorn
 */
class InsertController extends AbstractActionController
{
    protected Logger $logger;
    protected UserService $userService;
    protected CertForm $certForm;
    protected EmployeeForm $employeeForm;
    protected EmployeeTable $employeeTable;
    protected EmployeeCertRelForm $employeeCertRelForm;
    protected CertTable $certTable;
    protected EmployeeCertRelTable $employeeCertRelTable;
    protected CertTableForm $certTableForm;
    protected CertTableTablex $certTableTablex;
    protected CertTableGroupForm $certTableGroupForm;
    protected CertTableGroupTable $certTableGroupTable;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setCertForm(CertForm $certForm): void
    {
        $this->certForm = $certForm;
    }

    public function setEmployeeForm(EmployeeForm $employeeForm): void
    {
        $this->employeeForm = $employeeForm;
    }

    public function setEmployeeTable(EmployeeTable $employeeTable): void
    {
        $this->employeeTable = $employeeTable;
    }

    public function setEmployeeCertRelForm(EmployeeCertRelForm $employeeCertRelForm): void
    {
        $this->employeeCertRelForm = $employeeCertRelForm;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setEmployeeCertRelTable(EmployeeCertRelTable $employeeCertRelTable): void
    {
        $this->employeeCertRelTable = $employeeCertRelTable;
    }

    public function setCertTableForm(CertTableForm $certTableForm): void
    {
        $this->certTableForm = $certTableForm;
    }

    public function setCertTableTablex(CertTableTablex $certTableTablex): void
    {
        $this->certTableTablex = $certTableTablex;
    }

    public function setCertTableGroupForm(CertTableGroupForm $certTableGroupForm): void
    {
        $this->certTableGroupForm = $certTableGroupForm;
    }

    public function setCertTableGroupTable(CertTableGroupTable $certTableGroupTable): void
    {
        $this->certTableGroupTable = $certTableGroupTable;
    }

    /**
     * Insert new certificate
     * @return Response|ViewModel
     */
    public function certAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $this->certForm->setCertIdRequired(false);
        $this->certForm->init();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $this->certForm->setData($postData);
            if ($this->certForm->isValid()) {
                $formData = $this->certForm->getData();
                unset($formData['submit']);
                if ($this->certTable->insertCert($formData) > 0) {
                    $this->layout()->setVariable('message', ['level' => 'info', 'text' => 'Das Zertifikat wurde gespeichert']);
                    $this->certForm->init();
                }
            }
        } // isPost

        return new ViewModel([
            'certForm' => $this->certForm,
        ]);
    }

    /**
     * Insert new employee
     * @return Response|ViewModel
     */
    public function employeeAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }

        $this->employeeForm->init();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            unset($postData['submit']);
            $this->employeeForm->setData($postData);
            if ($this->employeeForm->isValid()) {
                $formData = $this->employeeForm->getData();
                unset($formData['submit']);
                if ($this->employeeTable->insertEmployee($formData) > 0) {
                    $this->layout()->setVariable('message', ['level' => 'info', 'text' => 'Der Mitarbeiter wurde gespeichert']);
                    $this->employeeForm->init();
                }
            }
        } // isPost

        return new ViewModel([
            'employeeForm' => $this->employeeForm,
        ]);
    }

    /**
     * IN DER GUI DEAKTIVIERT
     * weil Form immer NICHT valid
     * @return Response|ViewModel
     */
    public function employeeCertRelAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }

        $this->employeeCertRelForm->setEmployeeCertRelIdRequired(false);
        $this->employeeCertRelForm->init();
        $employeeId = (int)$this->params('employee_id');
        if (!$employeeId) {
            return $this->redirect()->toRoute('lerp_cert_employee_employees');
        }
        $this->employeeCertRelForm->get('employee_id')->setValue($employeeId);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();

            $this->employeeCertRelForm->setData($postData);
            if ($this->employeeCertRelForm->isValid()) {
                $formData = $this->employeeCertRelForm->getData();
                unset($formData['submit']);
                $formData['date_of_issue'] = CertDuration::isoStringToUnixtime($formData['date_of_issue']);
                // dynamisches FormElement cert_def_value
                $formData['cert_def_value'] = filter_var($postData['cert_def_value'], FILTER_SANITIZE_STRING);
                if ($this->employeeCertRelTable->insertEmployeeCertRel($formData) > 0) {
                    $this->layout()->setVariable('message',
                        ['level' => 'info', 'text' => 'Das Mitarbeiter Zertifikat wurde gespeichert']);
                    $this->employeeCertRelForm->init();
                } else {
                    $this->layout()->setVariable('message',
                        ['level' => 'warn', 'text' => 'Es ist ein Fehler aufgetreten! Bitte den Administrator informieren.']);
                }
            } else {
                $this->layout()->setVariable('message',
                    ['level' => 'error', 'text' => 'Nicht alle Daten sind korrekt eingegeben worden!']);
            }
        } // isPost

        return new ViewModel([
            'employeeCertRelForm' => $this->employeeCertRelForm,
        ]);
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function certTableAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }

        $this->certTableForm->setCertTableIdRequired(false);
        $this->certTableForm->init();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $this->certTableForm->setData($postData);
            if ($this->certTableForm->isValid()) {
                $formData = $this->certTableForm->getData();
                $certTableEntity = new CertTableEntity();
                $certTableEntity->exchangeArrayFromDatabase($formData);
                $saveResult = $certTableEntity->save($this->certTableTablex, $this->logger);
                $saveResultArray = explode('_', $saveResult);
                if (isset($saveResultArray[0]) && $saveResultArray[0] > 0 && isset($saveResultArray[1]) && $saveResultArray[1] > 0) {
                    return $this->redirect()->toRoute('lerp_cert_manage_cert_table_tds', ['cert_table_id' => $saveResultArray[0]]); // Weiter zum TDs editieren
                } else {
                    $this->layout()->setVariable('message',
                        ['level' => 'error', 'text' => 'Es ist ein fehler beim Speichern der Daten aufgetreten.']);
                }
            }
        } // isPost

        return new ViewModel([
            'certTableForm' => $this->certTableForm,
        ]);
    }

    public function certTableGroupAction()
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }

        $this->certTableGroupForm->setCertTableGroupIdRequired(false);
        $this->certTableGroupForm->init();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $this->certTableGroupForm->setData($postData);
            if ($this->certTableGroupForm->isValid()) {
                $formData = $this->certTableGroupForm->getData();
                $certTableGroupEntity = new CertTableGroupEntity();
                $certTableGroupEntity->exchangeArrayFromDatabase($formData);
                $saveResult = $certTableGroupEntity->save($this->certTableGroupTable, $this->getLogger());
                if ($saveResult > 0) {
                    return $this->redirect()->toRoute('lerp_cert_show_cert_table_groups');
                } else {
                    $this->layout()->setVariable('message',
                        ['level' => 'error', 'text' => 'Es ist ein Fehler beim Speichern der Daten aufgetreten.']);
                }
            }
        } // isPost

        return new ViewModel([
            'certTableGroupForm' => $this->certTableGroupForm,
        ]);
    }

}
