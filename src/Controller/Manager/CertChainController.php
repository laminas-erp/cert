<?php

namespace Lerp\Cert\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Entity\Cert\Chain\CertChainEntity;
use Lerp\Cert\Form\Cert\Chain\CertChainForm;
use Lerp\Cert\Table\Cert\Chain\CertChainItemTable;
use Lerp\Cert\Table\Cert\Chain\CertChainTable;

/**
 * Description of CertChainController
 *
 * @author allapow
 */
class CertChainController extends AbstractActionController
{
    protected Logger $logger;
    protected UserService $userService;
    protected CertChainTable $certChainTable;
    protected CertChainItemTable $certChainItemTable;
    protected CertChainForm $certChainForm;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setCertChainTable(CertChainTable $certChainTable): void
    {
        $this->certChainTable = $certChainTable;
    }

    public function setCertChainItemTable(CertChainItemTable $certChainItemTable): void
    {
        $this->certChainItemTable = $certChainItemTable;
    }

    public function setCertChainForm(CertChainForm $certChainForm): void
    {
        $this->certChainForm = $certChainForm;
    }

    public function certChainsAction()
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $viewModel = new ViewModel();
        $certChains = $this->certChainTable->getCertChains();
        $viewModel->setVariable('certChains', $certChains);
        return $viewModel;
    }

    /**
     * @return Response|ViewModel
     */
    public function certChainAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $viewModel = new ViewModel();
        $certChainId = $this->params('cert_chain_id');
        $viewModel->setVariable('certChainId', $certChainId);

        $certChainData = [];
        $certChainItems = [];
        $certChainItemIds = [];
        if (isset($certChainId)) {
            $this->certChainForm->setCertChainIdRequired(true);
            $this->certChainForm->init();
            $certChainData = $this->certChainTable->getCertChainById($certChainId);
            $this->certChainForm->setData($certChainData);

            $certChainItems = $this->certChainItemTable->getCertChainItemsByCertChainId($certChainId);
            foreach ($certChainItems as $certChainItem) {
                $certChainItemIds[] = $certChainItem['cert_id'];
            }
        } else {
            $this->certChainForm->init();
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (isset($postData['certforcertchain_submit'])) {
                if (!empty($postData['certforcertchain']) && isset($certChainId)) {
                    foreach ($postData['certforcertchain'] as $certforcertchain) {
                        if (!in_array($certforcertchain, $certChainItemIds)) {
                            $this->certChainItemTable->insert(['cert_chain_id' => $certChainId, 'cert_id' => ((int)$certforcertchain)]);
                        }
                    }
                    $certChainItems = $this->certChainItemTable->getCertChainItemsByCertChainId($certChainId);
                }
            } elseif (isset($postData['make_certchainmaster'])) {
                $certChainItemId = (int)$postData['cert_chain_item_id'];
                $this->getCertChainItemTable()->changeCertChainItemType($certChainItemId, 'master');
                $certChainItems = $this->getCertChainItemTable()->getCertChainItemsByCertChainId($certChainId);
            } elseif (isset($postData['remove_certchainmaster'])) {
                $certChainItemId = (int)$postData['cert_chain_item_id'];
                $this->certChainItemTable->changeCertChainItemType($certChainItemId, 'item');
                $certChainItems = $this->certChainItemTable->getCertChainItemsByCertChainId($certChainId);
            } elseif (isset($postData['remove_certchainitem'])) {
                $certChainItemId = (int)$postData['cert_chain_item_id'];
                $this->certChainItemTable->delete(['cert_chain_item_id' => $certChainItemId]);
                $certChainItems = $this->certChainItemTable->getCertChainItemsByCertChainId($certChainId);
            } else {
                $this->certChainForm->setData($postData);
                if ($this->certChainForm->isValid()) {
                    $certChainFormData = $this->certChainForm->getData();
                    $certChainEntity = new CertChainEntity();
                    $certChainEntity->exchangeArrayFromDatabase($certChainFormData);
                    if (isset($certChainId)) {
                        // UPDATE
                        if ($certChainEntity->update($certChainId, $this->certChainTable) < 0) {
                            $this->layout()->message = [
                                'level' => 'error',
                                'text'  => 'Fehler beim update der CertChain.',
                            ];
                        }
                    } else {
                        // INSERT
                        $newCertChainId = $certChainEntity->save($this->certChainTable);
                        if ($newCertChainId < 1) {
                            $this->layout()->message = [
                                'level' => 'error',
                                'text'  => 'Fehler beim insert der CertChain.',
                            ];
                        } else {
                            return $this->redirect()->toRoute('lerp_cert_certchain_manage', ['cert_chain_id' => $newCertChainId]);
                        }
                    }
                    $certChainData = $this->certChainTable->getCertChainById($certChainId);
                }
            }
        }

        $viewModel->setVariable('certChainData', $certChainData);
        $viewModel->setVariable('certChainForm', $this->certChainForm);

        if (isset($certChainId) && !empty($certChainData) && $certChainData['cert_chain_type'] == 'master') {
            $existCertChainMaster = false;
            $certChainMaster = null;
            foreach ($certChainItems as $key => $certChainItem) {
                if ($certChainItem['cert_chain_item_type'] == 'master') {
                    $existCertChainMaster = true;
                    $certChainMaster = $certChainItem;
                    unset($certChainItems[$key]);
                }
            }
            $viewModel->setVariable('existCertChainMaster', $existCertChainMaster);
            $viewModel->setVariable('certChainMaster', $certChainMaster);
        }
        $viewModel->setVariable('certChainItems', $certChainItems);
        return $viewModel;
    }

}
