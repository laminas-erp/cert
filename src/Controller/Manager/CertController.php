<?php

namespace Lerp\Cert\Controller\Manager;

use Bitkorn\User\Service\UserService;
use Laminas\Http\Response;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Entity\Cert\CertTableGroupEntity;
use Lerp\Cert\Form\Cert\CertForm;
use Lerp\Cert\Form\Cert\CertTableForm;
use Lerp\Cert\Form\Cert\CertTableGroupForm;
use Lerp\Cert\Table\Cert\CertDefTable;
use Lerp\Cert\Table\Cert\CertGroupTable;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Cert\CertTableGroupTable;
use Lerp\Cert\Table\Cert\CertTableTable;
use Lerp\Cert\Tablex\Cert\CertTableTablex;
use Lerp\Cert\Tablex\Cert\CertTablex;

/**
 *
 * @author bitkorn
 */
class CertController extends AbstractActionController
{
    protected Logger $logger;
    protected UserService $userService;
    protected CertTable $certTable;
    protected CertTablex $certTablex;
    protected CertForm $certForm;
    protected CertTableTable $certTableTable;
    protected CertTableForm $certTableForm;
    protected CertTableTablex $certTableTablex;
    protected CertTableGroupTable $certTableGroupTable;
    protected CertTableGroupForm $certTableGroupForm;
    protected CertGroupTable $certGroupTable;
    protected CertDefTable $certDefTable;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setCertTablex(CertTablex $certTablex): void
    {
        $this->certTablex = $certTablex;
    }

    public function setCertForm(CertForm $certForm): void
    {
        $this->certForm = $certForm;
    }

    public function setCertTableTable(CertTableTable $certTableTable): void
    {
        $this->certTableTable = $certTableTable;
    }

    public function setCertTableForm(CertTableForm $certTableForm): void
    {
        $this->certTableForm = $certTableForm;
    }

    public function setCertTableTablex(CertTableTablex $certTableTablex): void
    {
        $this->certTableTablex = $certTableTablex;
    }

    public function setCertTableGroupTable(CertTableGroupTable $certTableGroupTable): void
    {
        $this->certTableGroupTable = $certTableGroupTable;
    }

    public function setCertTableGroupForm(CertTableGroupForm $certTableGroupForm): void
    {
        $this->certTableGroupForm = $certTableGroupForm;
    }

    public function setCertGroupTable(CertGroupTable $certGroupTable): void
    {
        $this->certGroupTable = $certGroupTable;
    }

    public function setCertDefTable(CertDefTable $certDefTable): void
    {
        $this->certDefTable = $certDefTable;
    }

    /**
     * @return Response|ViewModel
     */
    public function certsAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $certs = $this->certTable->getCerts(['cert_group_id DESC', 'cert_order ASC', 'cert_id ASC']);
        return new ViewModel([
            'certs' => $certs,
        ]);
    }

    /**
     * Manage cert.
     * Aufgerufen aus der Liste der certs
     * @return Response|ViewModel
     */
    public function certAction()
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $certId = (int)$this->params('cert_id');
        if (!$certId) {
            return $this->redirect()->toRoute('lerp_cert_cert_certs');
        }
        $certData = $this->certTable->getCertById($certId);

        $this->certForm->init();
        $this->certForm->setData($certData);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $this->certForm->setData($postData);
            if ($this->certForm->isValid()) {
                $formData = $this->certForm->getData();
                unset($formData['submit']);
                if ($this->certTable->updateCert($formData) > 0) {
                    $this->layout()->setVariable('message',
                        ['level' => 'success', 'text' => 'Die &Auml;nderungen wurden gespeichert']);
                }
            }
        } // isPost

        return new ViewModel([
            'certForm' => $this->certForm,
        ]);
    }

    public function copyCertAction()
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }

        $certId = (int)$this->params('cert_id');
        if (!$certId) {
            return $this->redirect()->toRoute('lerp_cert_cert_certs');
        }
        $lastCertId = $this->certTablex->copycert($certId, $this->getCertTable());
        if ($lastCertId > 0) {
            return $this->redirect()->toRoute('lerp_cert_manage_cert', ['cert_id' => $lastCertId]);
        } else {
            $this->layout()->setVariable('message',
                [
                    'level' => 'error',
                    'text'  => 'Fehler in der Datenbank. Last insert id: ' . $lastCertId,
                ]);
        }

        return new ViewModel([
            'certForm' => $this->certForm,
        ]);
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function certTableTdsAction()
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $certTableId = (int)$this->params('cert_table_id');
        if (!$certTableId) {
            return $this->redirect()->toRoute('lerp_cert_show_cert_tables');
        }
        return new ViewModel([
            'certTableId' => $certTableId,
        ]);
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function certTableAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $certTableId = (int)$this->params('cert_table_id');
        if (!$certTableId) {
            return $this->redirect()->toRoute('lerp_cert_show_cert_tables');
        }
        $certTableData = $this->certTableTable->getCertTableById($certTableId);
        if (empty($certTableData)) {
            return $this->redirect()->toRoute('lerp_cert_show_cert_tables');
        }
        $certTableEntity = new \Lerp\Cert\Entity\Cert\CertTableEntity();
        $certTableEntity->exchangeArrayFromDatabase($certTableData);
        $this->certTableForm->setCertTableIdRequired(true);
        $this->certTableForm->setShowDeleteButton(true);
        $this->certTableForm->init();
        $this->certTableForm->setData($certTableData);

        $changedDimension = false;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            if (empty($postData['cancel'])) {
                $this->certTableForm->setData($postData);
                if ($this->certTableForm->isValid()) {
                    $formData = $this->certTableForm->getData();
                    $changedCertTableDimension = $certTableEntity->changedTableDimension($formData);
                    if (!empty($changedCertTableDimension)) {
                        if (!empty($postData['changed_dimension'])) {
                            $certTableEntity = new \Lerp\Cert\Entity\Cert\CertTableEntity();
                            $certTableEntity->exchangeArrayFromDatabase($formData);
                            $updateresult = $certTableEntity->update($this->certTableTablex, $this->logger);
                            if (!empty($updateresult[1])) {
                                // wenn $changedCertTableDimension dann muss auch $updateresult was haben (inkl. negativ)
                                $this->layout()->setVariable('message',
                                    ['level' => 'success', 'text' => 'Die Änderungen der Zertifikat Tabelle inkl. Dimension wurden gespeichert.']);
                            } else {
                                $this->layout()->setVariable('message',
                                    ['level' => 'warning', 'text' => 'Fehler beim Speichern der Daten. Bitte den administrator kontaktieren: ' . __CLASS__ . ' -> ' . __FUNCTION__]);
                            }
                        } else {
                            $changedDimension = true;
                            $signRow = ($changedCertTableDimension['row'] > 0) ? '+' : '';
                            $signColumn = ($changedCertTableDimension['column'] > 0) ? '+' : '';
                            $this->layout()->setVariable('message',
                                ['level' => 'warn', 'text' => 'Zert.-Tabellen-Dimension geändert: Reihen: '
                                    . $signRow . $changedCertTableDimension['row'] . '; Spalten: ' . $signColumn . $changedCertTableDimension['column']
                                    . '<br>Ist die Differenz zur vorherigen Gr&ouml;&szlig;e negativ, werden die Tabellenteile gel&ouml;scht!']);
                            $this->certTableForm->get('submit')->setAttribute('class', 'btn btn-warning');
                            $this->certTableForm->get('submit')->setLabel('Tabelle mit geänderten Dimensionen speichern und weiter');
                            $this->certTableForm->add(['name' => 'changed_dimension', 'type' => 'hidden', 'attributes' => ['value' => 1]]);
                            $this->certTableForm->add([
                                'name'       => 'cancel',
                                'type'       => 'submit',
                                'attributes' => ['value' => 1, 'class' => 'w3-button w3-red'],
                                'options'    => ['label' => 'abbrechen'],
                            ]);
                        }
                    } elseif (isset($formData['submit'])) {
                        $certTableEntity = new \Lerp\Cert\Entity\Cert\CertTableEntity();
                        $certTableEntity->exchangeArrayFromDatabase($formData);
                        $updateresult = $certTableEntity->update($this->certTableTablex, $this->logger);
                        if (!empty($updateresult[0])) {
                            // wenn $changedCertTableDimension dann muss auch $updateresult was haben (inkl. negativ)
                            $this->layout()->setVariable('message',
                                ['level' => 'success', 'text' => 'Die Änderungen wurden gespeichert.']);
                        } else {
                            $this->layout()->setVariable('message',
                                ['level' => 'info', 'text' => 'Es wurden keine Änderungen vorgenommen.']);
                        }
                    } elseif (isset($formData['submit_delete'])) {
                        if (!empty($postData['delete_complete_table'])) {
                            $deleteResult = $this->certTableTablex->deleteCertTablePlusTds($formData['cert_table_id']);
                            if ($deleteResult[0] == 1) {
                                return $this->redirect()->toRoute('lerp_cert_show_cert_tables');
                            } else {
                                $this->layout()->setVariable('message',
                                    ['level' => 'warning', 'text' => 'Fehler beim Löschen der Tabelle. Bitte den administrator kontaktieren: ' . __CLASS__ . ' -> ' . __FUNCTION__]);
                            }
                        } else {
                            $this->certTableForm->get('submit_delete')->setLabel('JA, komplette Tabelle löschen');
                            $this->certTableForm->add(['name' => 'delete_complete_table', 'type' => 'hidden', 'attributes' => ['value' => 1]]);
                            $this->certTableForm->add([
                                'name'       => 'cancel',
                                'type'       => 'submit',
                                'attributes' => ['value' => 1, 'class' => 'w3-button'],
                                'options'    => ['label' => 'Tabelle löschen abbrechen'],
                            ]);
                        }
                    }
                }
            }
        } // isPost

        return new ViewModel([
            'certTableId'      => $certTableId,
            'certTableForm'    => $this->certTableForm,
            'changedDimension' => $changedDimension,
        ]);
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function certTableGroupsAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $certTableGroupssData = $this->certTableGroupTable->getCertTableGroups();
        return new ViewModel([
            'certTableGroupsData' => $certTableGroupssData,
        ]);
    }

    /**
     * Edit CertTableGroup
     * @return Response|ViewModel
     */
    public function certTableGroupAction()
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $certTableGroupId = (int)$this->params('cert_table_group_id');
        if (!$certTableGroupId) {
            return $this->redirect()->toRoute('lerp_cert_show_cert_table_groups');
        }

        $certTableGroupData = $this->certTableGroupTable->getCertTableGroupById($certTableGroupId);
        $certTableGroupEntity = new CertTableGroupEntity();
        $certTableGroupEntity->flipMapping();
        $certTableGroupEntity->exchangeArrayFromDatabase($certTableGroupData);

        $this->certTableGroupForm->setCertTableGroupIdRequired(false);
        $this->certTableGroupForm->init();
        $this->certTableGroupForm->setData($certTableGroupEntity->getFormStorage());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $this->certTableGroupForm->setData($postData);
            if ($this->certTableGroupForm->isValid()) {
                $formData = $this->certTableGroupForm->getData();
                $certTableGroupEntity = new CertTableGroupEntity();
                $certTableGroupEntity->exchangeArrayFromDatabase($formData);
                $saveResult = $certTableGroupEntity->update($this->certTableGroupTable, $this->logger);
                if ($saveResult > 0) {
                    $this->layout()->setVariable('message', ['level' => 'success', 'text' => 'Die Änderungen wurden gespeichert.']);
                } else {
                    $this->layout()->setVariable('message',
                        ['level' => 'info', 'text' => 'Es wurden keine Änderungen vorgenommen.']);
                }
            }
        }

        return new ViewModel([
            'certTableGroupId'   => $certTableGroupId,
            'certTableGroupForm' => $this->certTableGroupForm,
        ]);
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function certTablesAction()
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $certTablesData = $this->certTableTable->getCertTables();
        $certTableGroupsIdAssoc = $this->certTableGroupTable->getCertTableGroupsIdAssoc();
        return new ViewModel([
            'certTablesData'         => $certTablesData,
            'certTableGroupsIdAssoc' => $certTableGroupsIdAssoc,
        ]);
    }

    /**
     * Insert new Cert Group
     * @return Response|ViewModel
     */
    public function certGroupsAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $data = $this->certGroupTable->getCertGroups();
        return new ViewModel([
            'data' => $data,
        ]);
    }

    /**
     * Insert new Cert Group
     * coming soon
     * @return Response|ViewModel
     */
    public function certGroupAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }
        $paramsGet = $this->params()->fromQuery();
        $id = (int)$this->params('id');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
        } // isPost
        return new ViewModel([
        ]);
    }

    /**
     * Zeigt statisch den Tabelleninhalt von db.cert_def (die JSON)
     * @return Response|ViewModel
     */
    public function certDefsAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserContainer()) {
            return $this->redirect()->toRoute('lerp_cert_login');
        }

        $certDefs = $this->certDefTable->getCertDefs();

        return new ViewModel([
            'certDefs' => $certDefs,
        ]);
    }

}
