<?php

namespace Lerp\Cert\View\Helper;

use Laminas\Form\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

/**
 * Noch NICHT in Module.php vorhanden.
 * @author allapow
 */
class EmployeeCertRelForm extends AbstractHelper
{

    public function __invoke($cert_id)
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate('layout/clean');
        $viewModel->setVariable('certId', $cert_id);
        return $this->getView()->render($viewModel);
    }

}
