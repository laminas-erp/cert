<?php

namespace  Lerp\Cert\View\Helper\Table;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Cert\CertTableTdTable;

/**
 * Description of ReleaseAndWriteoff
 *
 * @author allapow
 */
class CertTableTdViewHelper extends AbstractHelper
{
    
    private string $defaultTemplate = 'table/certTableTd';

    private CertTable $certTable;
    private CertTableTdTable $certTableTdTable;

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setCertTableTdTable(CertTableTdTable $certTableTdTable): void
    {
        $this->certTableTdTable = $certTableTdTable;
    }

    public function __invoke(array $certTableTdData, $options = null)
    {
        $viewModel = new ViewModel();
        $viewModel->setVariable('certTableTdData', $certTableTdData);
        if (empty($certTableTdData['cert_id'])) {
            $viewModel->setVariable('certId', 0);
        } else {
            $certData = $this->certTable->getCertById($certTableTdData['cert_id']);
            $viewModel->setVariable('certData', $certData);
        }
        if(!isset($options['template'])) {
            $options['template'] = $this->defaultTemplate;
        }
        $viewModel->setTemplate($options['template']);
        if(!isset($options['generateForm'])) {
            $options['generateForm'] = true;
        }
        $viewModel->setVariable('generateForm', $options['generateForm']);

        return $this->getView()->render($viewModel);
    }
}
