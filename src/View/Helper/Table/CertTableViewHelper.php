<?php

namespace Lerp\Cert\View\Helper\Table;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Table\Cert\CertTableTable;
use Lerp\Cert\Tablex\Cert\CertTableTablex;

/**
 * Description of ReleaseAndWriteoff
 *
 * @author allapow
 */
class CertTableViewHelper extends AbstractHelper
{

    private $defaultTemplate = 'table/certTable';

    private \Lerp\Cert\Table\Cert\CertTable $certTable;
    private CertTableTablex $certTableTablex;
    private CertTableTable $certTableTable;

    public function setCertTable(\Lerp\Cert\Table\Cert\CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setCertTableTablex(CertTableTablex $certTableTablex): void
    {
        $this->certTableTablex = $certTableTablex;
    }

    public function setCertTableTable(CertTableTable $certTableTable): void
    {
        $this->certTableTable = $certTableTable;
    }

    public function __invoke($certTableId, $options = null)
    {
        if (empty($certTableId)) {
            return '';
        }
        $viewModel = new ViewModel();
        if (!isset($options['template'])) {
            $options['template'] = $this->defaultTemplate;
        }
        $viewModel->setTemplate($options['template']);
        if (!isset($options['generateForm'])) {
            $options['generateForm'] = true;
        }
        $viewModel->setVariable('generateForm', $options['generateForm']);

        $tableData = $this->certTableTablex->getCertTableWithTdsById($certTableId);
        $viewModel->setVariable('tableData', $tableData);

        if (count($tableData) > 0 && !empty($tableData[0]['cert_table_column_names'])) {
            $viewModel->setVariable('tableColumnNames', explode(',', $tableData[0]['cert_table_column_names']));
        } else {
            $viewModel->setVariable('tableColumnNames', '');
        }
        if (count($tableData) > 0 && !empty($tableData[0]['cert_table_row_names'])) {
            $viewModel->setVariable('tableRowNames', explode(',', $tableData[0]['cert_table_row_names']));
            $viewModel->setVariable('colspan', $tableData[0]['cert_table_column_count'] + 1);
        } else {
            $viewModel->setVariable('tableRowNames', '');
            $viewModel->setVariable('colspan', $tableData[0]['cert_table_column_count']);
        }

        return $this->getView()->render($viewModel);
    }

}
