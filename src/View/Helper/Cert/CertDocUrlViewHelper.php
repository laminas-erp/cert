<?php

namespace Lerp\Cert\View\Helper\Cert;

use Laminas\Log\Logger;
use Laminas\View\Helper\AbstractHelper;
use Lerp\Cert\Tools\FolderSystem;

class CertDocUrlViewHelper extends AbstractHelper
{

    protected Logger $logger;
    private array $certConfig;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setCertConfig($certConfig)
    {
        $this->certConfig = $certConfig;
    }

    /**
     *
     * @param string $certDocMainFolderRoot
     * @param int $certDocDatetime
     * @param string $certDocFilename
     * @return string
     * @throws \Exception
     */
    public function __invoke(string $certDocMainFolderRoot, int $certDocDatetime, string $certDocFilename)
    {
        $relativeFilename = FolderSystem::computeRelativeFilename($certDocMainFolderRoot, $certDocDatetime,
            $certDocFilename);
        return $this->certConfig['doc_path_web'] . $relativeFilename;
    }

}
