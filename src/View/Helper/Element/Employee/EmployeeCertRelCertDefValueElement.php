<?php

namespace Lerp\Cert\View\Helper\Element\Employee;

use Laminas\Log\Logger;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Tablex\Cert\CertTablex;

/**
 * Zu einer \Lerp\Cert\Form\Employee\EmployeeCertRelForm gibt es mir das passende Form Element, in Anhaengigkeit zu:
 * db.cert_def.cert_def_def darin JSON key 'values'.
 *
 * @author allapow
 */
class EmployeeCertRelCertDefValueElement extends AbstractHelper
{

    private string $template = 'employee/employeeCertRelFormCertDefDefValue';
    protected Logger $logger;
    private CertTablex $certTablex;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setCertTablex(CertTablex $certTablex)
    {
        $this->certTablex = $certTablex;
    }

    /**
     *
     * @param int $certID Die cert ID
     * @return string
     * @throws \Exception
     */
    public function __invoke(int $certID): string
    {
        $viewModel = new ViewModel();
        $certData = $this->certTablex->getCertById($certID);
        if (empty($certData['cert_def_def'])) {
            throw new \Exception('EmployeeCertRelFormCertDefDefValue Aufruf ohne cert_def_def');
        }
        $certDefinition = json_decode($certData['cert_def_def'], true);
        if (null === $certDefinition || !is_array($certDefinition)) {
            throw new \Exception('cert_def_def ist kein JSON');
        }
        $viewModel->setVariable('certDefType', $certDefinition['type']);
        if (!empty($certDefinition['value'])) {
            $viewModel->setVariable('certDefValue', $certDefinition['value']);
        }
        if (!empty($certDefinition['values'])) {
            $viewModel->setVariable('certDefValues', $certDefinition['values']);
        }

//        $viewModel->setVariable('certData', $certData);
        $viewModel->setTemplate($this->template);

        return $this->getView()->render($viewModel);
    }

}
