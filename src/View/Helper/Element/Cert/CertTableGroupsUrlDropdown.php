<?php

namespace Lerp\Cert\View\Helper\Element\Cert;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Table\Cert\CertTableGroupTable;

class CertTableGroupsUrlDropdown extends AbstractHelper
{

    private $defaultTemplate = 'cert/certTableGroupsUrlDropdown';

    private CertTableGroupTable $certTableGroupTable;

    public function setCertTableGroupTable(CertTableGroupTable $certTableGroupTable)
    {
        $this->certTableGroupTable = $certTableGroupTable;
    }

    public function __invoke($employeeId)
    {
        $viewModel = new ViewModel();
        $viewModel->setVariable('employeeId', $employeeId);

        $certsData = $this->certTableGroupTable->getCertTableGroups();
        $viewModel->setVariable('certTableGroupsData', $certsData);

        $viewModel->setTemplate($this->defaultTemplate);

        return $this->getView()->render($viewModel);
    }

}
