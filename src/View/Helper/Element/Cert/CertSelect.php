<?php

namespace Lerp\Cert\View\Helper\Element\Cert;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Table\Cert\CertGroupTable;
use Lerp\Cert\Table\Cert\CertTable;

class CertSelect extends AbstractHelper
{

    private string $template = 'cert/certSelect';

    private CertTable $certTable;
    private CertGroupTable $certGroupTable;

    public function setCertTable(CertTable $certTable)
    {
        $this->certTable = $certTable;
    }

    public function setCertGroupTable(CertGroupTable $certGroupTable)
    {
        $this->certGroupTable = $certGroupTable;
    }

    public function __invoke(array $certTableTdData)
    {
        if (empty($certTableTdData)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('certTableTdData', $certTableTdData);

        $certId = (int)$certTableTdData['cert_id'];
        $viewModel->setVariable('currentCertId', $certId);
        if ($certId > 0) {
            $selectClass = 'border-success';
        } else {
            $selectClass = 'border-warning';
        }
        $viewModel->setVariable('selectClass', $selectClass);
        $certsData = $this->certTable->getCerts();
        $viewModel->setVariable('certsData', $certsData);

        $certGroupsIdAssoc = $this->certGroupTable->getCertGroupsIdAssoc();
        $viewModel->setVariable('certGroupsIdAssoc', $certGroupsIdAssoc);

        $viewModel->setTemplate($this->template);

        return $this->getView()->render($viewModel);
    }

}
