<?php

namespace Lerp\Cert\View\Helper\Document;

use Laminas\Log\Logger;
use Laminas\View\Helper\AbstractHelper;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Cert\CertTableTable;
use Lerp\Cert\Table\Employee\EmployeeTable;
use Lerp\Cert\Tablex\Cert\CertTableTablex;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;

/**
 *
 * @author allapow
 */
class CertsTableGroupViewHelper extends AbstractHelper
{

    private string $defaultTemplate = 'document/certsTableGroup';
    private Logger $logger;
    private EmployeeTablex $employeeTablex;
    private EmployeeTable $employeeTable;
    private CertTable $certTable;
    private CertTableTablex $certTableTablex;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setEmployeeTablex(EmployeeTablex $employeeTablex): void
    {
        $this->employeeTablex = $employeeTablex;
    }

    public function setEmployeeTable(EmployeeTable $employeeTable): void
    {
        $this->employeeTable = $employeeTable;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setCertTableTablex(CertTableTablex $certTableTablex): void
    {
        $this->certTableTablex = $certTableTablex;
    }

    public function __invoke(int $certTableGroupId, int $employeeId, array $options = null)
    {
        if (empty($certTableGroupId) || empty($employeeId)) {
            return '';
        }
        $viewModel = new \Laminas\View\Model\ViewModel();
        $viewModel->setVariable('employeeId', $employeeId);
        if (!isset($options['template'])) {
            $options['template'] = $this->defaultTemplate;
        }
        if (!isset($options['generateForm'])) {
            $options['generateForm'] = false;
        }
        $viewModel->setTemplate($options['template']);
        $viewModel->setVariable('generateForm', $options['generateForm']);

        $certTableWithEmployeeCertRelsData = $this->certTableTablex->getCertTableTdsWithEmployeeCertRels($certTableGroupId, $employeeId);
        $viewModel->setVariable('tableData', $certTableWithEmployeeCertRelsData);

        $employee = $this->employeeTable->getEmployeeById($employeeId);
        $viewModel->setVariable('employee', $employee);

        $employeeCerts = $this->employeeTablex->getEmployeeCertRels($employeeId);
        $viewModel->setVariable('employeeCerts', $employeeCerts);

        $allActiveCerts = $this->certTable->getCerts();
        $viewModel->setVariable('allActiveCerts', $allActiveCerts);

        return $this->getView()->render($viewModel);
    }
}
