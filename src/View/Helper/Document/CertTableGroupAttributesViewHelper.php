<?php

namespace Lerp\Cert\View\Helper\Document;

use Laminas\Log\Logger;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

class CertTableGroupAttributesViewHelper extends AbstractHelper
{

    const TEMPLATE = 'document/certTableGroupAttributes';

    private Logger $logger;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function __invoke($employeeId, array $employeeCertTableGroupAttributesIdAssoc, array $tableGroupAttributes)
    {
        if (empty($employeeId) || empty($tableGroupAttributes)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('employeeCertTableGroupAttributesIdAssoc', $employeeCertTableGroupAttributesIdAssoc);
        $viewModel->setVariable('tableGroupAttributes', $tableGroupAttributes);

        return $this->getView()->render($viewModel);
    }

}
