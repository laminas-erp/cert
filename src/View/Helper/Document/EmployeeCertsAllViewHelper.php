<?php

namespace Lerp\Cert\View\Helper\Document;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeTable;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;

/**
 * Description of ReleaseAndWriteoff
 *
 * @author allapow
 */
class EmployeeCertsAllViewHelper extends AbstractHelper
{

    private $defaultTemplate = 'document/employeeCerts';

    private EmployeeTablex $employeeTablex;
    private EmployeeTable $employeeTable;
    private CertTable $certTable;

    public function setEmployeeTablex(EmployeeTablex $employeeTablex): void
    {
        $this->employeeTablex = $employeeTablex;
    }

    public function setEmployeeTable(EmployeeTable $employeeTable): void
    {
        $this->employeeTable = $employeeTable;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function __invoke($employeeId, $options = null)
    {
        if (empty($employeeId)) {
            return '';
        }
        if (!isset($options['template'])) {
            $options['template'] = $this->defaultTemplate;
        }
        if (!isset($options['generateForm'])) {
            $options['generateForm'] = false;
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate($options['template']);
        $viewModel->setVariable('generateForm', $options['generateForm']);

        $employee = $this->employeeTable->getEmployeeById($employeeId);
        $viewModel->setVariable('employee', $employee);

        $employeeCerts = $this->employeeTablex->getEmployeeCertRels($employeeId);
        $viewModel->setVariable('employeeCerts', $employeeCerts);

        $allActiveCerts = $this->certTable->getCerts();
        $viewModel->setVariable('allActiveCerts', $allActiveCerts);

        return $this->getView()->render($viewModel);
    }

}
