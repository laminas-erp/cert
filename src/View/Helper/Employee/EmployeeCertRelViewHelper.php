<?php

namespace Lerp\Cert\View\Helper\Employee;

use Laminas\Log\Logger;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;
use Lerp\Cert\Entity\Employee\EmployeeCertRelEntity;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelDocTable;
use \Lerp\Cert\Tools\CertDuration;

/**
 * Zeigt genau ein Zertifikat.
 *
 * Entscheidet ob die Daten nur dargestellt werden oder inklusive eines FormElements.
 * Im Falle eines FormElements werden die Daten einfach an das FormElement weiter geleitet.
 *
 * @author allapow
 */
class EmployeeCertRelViewHelper extends AbstractHelper
{

    protected Logger $logger;
    private $template = 'employee/employeeCertRel';
    private CertTable $certTable;
    private EmployeeCertRelDocTable $employeeCertRelDocTable;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setCertTable(CertTable $certTable): void
    {
        $this->certTable = $certTable;
    }

    public function setEmployeeCertRelDocTable(EmployeeCertRelDocTable $employeeCertRelDocTable): void
    {
        $this->employeeCertRelDocTable = $employeeCertRelDocTable;
    }

    /**
     *
     * @param array $allEmployeeCertData
     * @param int $certID Die cert ID
     * @param boolean $generateForm
     * @return string
     * @throws \Exception
     */
    public function __invoke($employeeId, $allEmployeeCertData, $certID, $generateForm = false): string
    {
        $viewModel = new ViewModel();
        $viewModel->setVariable('generateForm', $generateForm);
        $currentCertData = $this->certTable->getCertById($certID);
        $viewModel->setVariable('certData', $currentCertData);
        $isCert = !empty($currentCertData);
        $viewModel->setVariable('isCert', $isCert);
        $remainingTimeClass = 'remaining-none';
        if ($isCert && (empty($allEmployeeCertData[$certID]) || !is_array($allEmployeeCertData[$certID]))) {
            /*
             * NO employee cert relation
             */
            $buttonDataAction = 'add';
            $buttonTitle = '';
            $buttonLabel = 'add';
        } else if ($isCert) {
            $employeeCertData = $allEmployeeCertData[$certID];
            if (empty($employeeCertData['cert_def_def'])) {
                throw new \Exception('Aufruf ohne cert_def_def');
            }
            $employeecertRelEntity = new EmployeeCertRelEntity();
            $employeecertRelEntity->flipMapping();
            $employeecertRelEntity->exchangeArrayFromDatabase($employeeCertData);
            $viewModel->setVariable('employeecertRelEntity', $employeecertRelEntity);
            $viewModel->setVariable('employeeCertData', $employeeCertData);

            $employeeCertRelDocCount = $this->employeeCertRelDocTable->countEmployeeCertRelDocsByEmployeeCertRelId($employeecertRelEntity->getStorageValue('id'));
            $viewModel->setVariable('employeeCertRelDocCount', $employeeCertRelDocCount);

            $certDefinition = json_decode($employeeCertData['cert_def_def'], true);
            if (null === $certDefinition || !is_array($certDefinition)) {
                throw new \Exception('cert_def_def ist kein JSON');
            }
            $viewModel->setVariable('certDefType', $certDefinition['type']);
            switch ($certDefinition['type']) {
                case 'select':
                    $viewModel->setVariable('certDefValue', $certDefinition['values'][$employeeCertData['cert_def_value']]);
                    break;
                case 'checkbox':
                    $viewModel->setVariable('certDefValue', $certDefinition['value']);
                    break;
                case 'text':
                    $viewModel->setVariable('certDefValue',
                        '<span title="' . $employeeCertData['cert_def_value'] . '">'
                        . substr($employeeCertData['cert_def_value'], 0, 5) . '</span>');
                    break;
            }
//            $viewModel->setVariable('certDefValues', $certDefinition['values']);

            if ($employeeCertData['duration'] > 0) {
                $remainingDateInterval = CertDuration::getRemainingTime($employeeCertData['duration'],
                    $employeeCertData['date_of_issue']);
                $remainingTime = $remainingDateInterval->y . '-' . $remainingDateInterval->m . '-' . $remainingDateInterval->d;
                $viewModel->setVariable('remainingTime', $remainingTime);
                $nearlyDays = $remainingDateInterval->y * 12 * 30 + $remainingDateInterval->m * 30 + $remainingDateInterval->d;
                if ($remainingDateInterval->invert == 1 && $nearlyDays >= 30) {
                    $remainingTimeClass = 'remaining-ok';
                } else if ($remainingDateInterval->invert == 1 && $nearlyDays < 30) {
                    $remainingTimeClass = 'remaining-warning';
                } else if ($remainingDateInterval->invert == 0) {
                    $remainingTimeClass = 'remaining-risky';
                }
            }

            $buttonDataAction = 'replace';
            $buttonTitle = 'Das vorhandene wird archiviert';
            $buttonLabel = '&nbsp;replace&nbsp;';
        } else {
            /*
             * NO cert
             */
        }

        $viewModel->setVariable('remainingTimeClass', $remainingTimeClass);

        if ($currentCertData && $generateForm) {
            $button = '<button data-action="' . $buttonDataAction . '" data-certid="' . $certID . '"'
                . ' data-employeeid="' . $employeeId . '"'
                . ' class="w3-xbtn w3-xtiny margin-xbtn cert-bg manage-cert"'
                . ' data-toggle="modal" data-target="#employeeCertModal"'
                . ' title="' . $buttonTitle . '"'
                . '>%s</button>';
            $viewModel->setVariable('certRelCreateButton', sprintf($button, $buttonLabel));
        }

        $viewModel->setTemplate($this->template);

        return $this->getView()->render($viewModel);
    }
}
