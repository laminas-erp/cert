<?php

namespace Lerp\Cert\View\Helper\IdAssoc;

use Laminas\View\Helper\AbstractHelper;
use Lerp\Cert\Table\Cert\CertDefTable;

class IdCertDefViewHelper extends AbstractHelper
{

    private CertDefTable $certDefTable;
    private array $certDefsIdAssoc;

    public function setCertDefTable(CertDefTable $certDefTable)
    {
        $this->certDefTable = $certDefTable;
    }

    public function __invoke($certDefId)
    {
        if (!isset($this->certDefsIdAssoc)) {
            $this->certDefsIdAssoc = $this->certDefTable->getCertDefsIdAssoc();
        }
        return $this->certDefsIdAssoc[$certDefId];
    }

}
