<?php

namespace Lerp\Cert\View\Helper\IdAssoc\Select;

use Laminas\View\Helper\AbstractHelper;

/**
 * @author allapow
 */
class SimpleSelectViewHelper extends AbstractHelper
{

    public function __invoke(array $idAssoc, $currentValue = '', $formFieldName = 'cert_id', $cssClass = 'form-control', $withEmptyValue = true)
    {
        if (!$idAssoc) {
            return '';
        }
        $select = '<select name="' . $formFieldName . '" class="' . $cssClass . '">';
        if ($withEmptyValue) {
            $select .= '<option value="">-- bitte w&auml;hlen --</option>';
        }
        foreach ($idAssoc as $value => $displayValue) {
            $select .= '<option value="' . $value . '" '
                . ($currentValue == $value ? 'selected' : '')
                . '>' . $displayValue . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

}
