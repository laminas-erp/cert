<?php

namespace Lerp\Cert\View\Helper\IdAssoc;

use Laminas\View\Helper\AbstractHelper;
use Lerp\Cert\Table\Cert\CertTable;

class IdCertInfoViewHelper extends AbstractHelper
{

    private CertTable $certTable;
    private array $certIdAssocExt;

    public function setCertTable(CertTable $certTable)
    {
        $this->certTable = $certTable;
    }

    public function __invoke($certId)
    {
        if (!$this->certIdAssocExt) {
            $this->certIdAssocExt = $this->certTable->getCertsIdAssocExt();
        }
        return $this->certIdAssocExt[$certId];
    }
}
