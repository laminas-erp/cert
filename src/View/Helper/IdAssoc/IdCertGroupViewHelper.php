<?php

namespace Lerp\Cert\View\Helper\IdAssoc;

use Laminas\View\Helper\AbstractHelper;
use Lerp\Cert\Table\Cert\CertGroupTable;

class IdCertGroupViewHelper extends AbstractHelper
{

    private CertGroupTable $certGroupTable;
    private array $certGroupsIdAssoc;

    public function setCertGroupTable(CertGroupTable $certGroupTable)
    {
        $this->certGroupTable = $certGroupTable;
    }

    public function __invoke($certGroupId)
    {
        if (!isset($this->certGroupsIdAssoc)) {
            $this->certGroupsIdAssoc = $this->certGroupTable->getCertGroupsIdAssoc();
        }
        return $this->certGroupsIdAssoc[$certGroupId];
    }

}
