<?php

namespace Lerp\Cert\PortableDocument;

/**
 * https://de.wikipedia.org/wiki/A4-Format
 * Portrait: 210 x 297 mm
 * Landscape: 297 x 210 mm
 *
 * @author allapow
 */
class TableGroupPdf
{

    private static array $tableMeasure;

    /**
     *
     * @param int $marginLeft
     * @param int $marginRight
     * @param int $columnCount
     * @param string $orientation
     */
    private static function computeTableMeasure(int $marginLeft, int $marginRight, int $columnCount, string $orientation = 'p'): void
    {
        self::$tableMeasure['marginLeft'] = $marginLeft;
        self::$tableMeasure['marginRight'] = $marginRight;
        switch ($orientation) {
            case 'p':
                self::$tableMeasure['width'] = 210 - $marginLeft - $marginRight;
                self::$tableMeasure['columnWidth'] = (float)self::$tableMeasure['width'] / $columnCount;
                break;
            case 'l':
                self::$tableMeasure['width'] = 297 - $marginLeft - $marginRight;
                self::$tableMeasure['columnWidth'] = (float)self::$tableMeasure['width'] / $columnCount;
                break;
        }
    }

    /**
     *
     * @param array $employeeCertRels
     * @param string $orientation
     */
    public static function generateTableGroupPdf(array $employeeCertRels, string $orientation = 'p'): void
    {
        self::computeTableMeasure(20, 20, 1, $orientation); // hier ist columnCount egal
        $pdf = new \TCPDF($orientation);
        $pdf->SetCreator('Lingana ERP');
        $pdf->SetAuthor('Bitkorn');
        $pdf->SetTitle('Bitkorn - Mitarbeiter Zertifizierungen');
        $pdf->SetSubject('Flugzeug Gruppierung');
        $pdf->SetHeaderData('', 0, 'Bitkorn - Mitarbeiter Zertifizierungen', 'Eisenwerkstraße 9; D-58332 Schwelm');
        $pdf->SetMargins(self::$tableMeasure['marginLeft'], 20, self::$tableMeasure['marginRight']); // left, top, right for content (not for header)
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(0);
        $pdf->SetAutoPageBreak(true, 30); // 2. margin-bottom
        $pdf->SetFontSize(8);
        $pdf->AddPage();
        $pdf->writeHTML('<p>der erste Absatz</p>');
        $pdf->Ln(); // Performs a line break

        foreach ($employeeCertRels as $key => $employeeCertRel) {

            if ($employeeCertRel['cert_table_row_index'] == 1 && $employeeCertRel['cert_table_column_index'] == 1) {
                self::computeTableMeasure(20, 20, $employeeCertRel['cert_table_column_count'], $orientation);
                // table header
                if (!empty($employeeCertRel['cert_table_heading'])) {
                    $pdf->SetFillColor(122, 30, 77);
                    $pdf->SetTextColor(255);
                    $pdf->SetFont('', 'B');
                    $pdf->Cell(self::$tableMeasure['width'], 6, $employeeCertRel['cert_table_heading'], 1, 0, 'L', true);
                    $pdf->Ln();
                }
                if (!empty($employeeCertRel['cert_table_column_names'])) {
                    $columnNames = explode(',', $employeeCertRel['cert_table_column_names']);
                    if (count($columnNames) != $employeeCertRel['cert_table_column_count']) {
                        throw new \RuntimeException('column names are not equal with column count');
                    }
                    foreach ($columnNames as $columnName) {
                        $pdf->SetFillColor(122, 30, 77);
                        $pdf->SetTextColor(255);
                        $pdf->SetFont('', 'B');
                        $pdf->Cell(self::$tableMeasure['columnWidth'], 6, $columnName, 1, 0, 'C', true);
                    }
                    $pdf->Ln();
                }
            }

//            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('', '');
            $pdf->MultiCell(self::$tableMeasure['columnWidth'], 6, $employeeCertRel['cert_name'], 1, 'C', false, 0);

            if ($employeeCertRel['cert_table_column_index'] == $employeeCertRel['cert_table_column_count']) {
                $pdf->Ln();
            }
        }

        $pdf->Output('FlugzeugGruppierung.pdf', 'I'); // dev
//        $pdf->Output('LieferantenBewertungQM.pdf', 'D');
    }

}
