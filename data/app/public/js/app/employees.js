
$(document).ready(function () {

    $(document).on("change", "select.cert-table-group-id-select", function () {
        /*
         * UNUSED!!!!!!!
         */
        var tableGroupId = $(this).val();
        var employeeId = $(this).data("employeeid");
//        alert("tableGroupId: " + tableGroupId + "; employeeId: " + employeeId);
        if (tableGroupId != 0) {
            window.location = "/show-employee-certs-table-group/" + tableGroupId + "/" + employeeId;
        }
    });
    
    $("#no_godata_id").autocomplete({
        source: function (request, response) {

            $.ajax({
                url: "/godata-users-jqueryui-autocomplete",
                dataType: "json",
                method: "get",
                success: function (data) {
                    
                }
            }).done(function (data) {
            });
        }
    });
});