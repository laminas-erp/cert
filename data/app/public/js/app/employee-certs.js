
$(document).ready(function () {
    $('#employeeCertModal').on('hide.bs.modal', function (e) {
        $("div#certrel_form_dyn").html("");
    });

    /*
     * MUSS hier damit es in mehreren EventHandlern (on & submit) sichtbar ist
     */
    var certId = 0;
    var employeeId = 0;
    var certRelId = 0;
    var buttonAction = "";

    /*
     * Hier MUSS es
     * a) document; b) on()
     * sein. Sonst triggern die dynamisch generierten Button nicht.
     */
    $(document).on("click", "button.manage-cert", function () {
        certId = $(this).data("certid");
        employeeId = $(this).data("employeeid");
        certRelId = $(this).data("certrelid");

//            alert("certId: " + certId + "; employeeId: " + employeeId);

        /*
         * Employee Namen holen fuer Modal Ueberschrift
         */
        $.ajax({
            url: "/rest-employee/" + employeeId,
            dataType: "json"
        }).done(function (data) {
            $("span#employee_name_dyn").html(data.employee.name_1 + " " + data.employee.name_2);
        });

        /*
         * CertRelForm holen und ins Modal packen
         */
        buttonAction = $(this).data("action");
//            alert("buttonAction: " + buttonAction);
        var employeeCertRelFormUrl = "";
        var submitButtonText = "";
        switch (buttonAction) {
            case "add":
                employeeCertRelFormUrl = "/employee-cert-rel-form-add/" + certId;
                submitButtonText = "Mitarbeiter Zertifikat erstellen";
                $("button#modal-submit-button").show();
                $("button#modal-cancel-button").text("abbrechen");
                break;
            case "replace":
                employeeCertRelFormUrl = "/employee-cert-rel-form-add/" + certId;
                submitButtonText = "Mitarbeiter Zertifikat ersetzen";
                $("button#modal-submit-button").show();
                $("button#modal-cancel-button").text("abbrechen");
                break;
            case "edit":
                employeeCertRelFormUrl = "/employee-cert-rel-form-edit/" + certRelId;
                submitButtonText = "Änderungen am Zertifikat speichern";
                $("button#modal-submit-button").show();
                $("button#modal-cancel-button").text("abbrechen");
                break;
            case "delete":
                employeeCertRelFormUrl = "/employee-cert-rel-form-delete/" + certRelId;
                submitButtonText = "Mitarbeiter Zertifikat löschen";
                $("button#modal-submit-button").show();
                $("button#modal-cancel-button").text("abbrechen");
                break;
            case "documents":
                employeeCertRelFormUrl = "/employee-cert-rel-form-documents/" + certRelId;
                submitButtonText = "upload";
//                $("button#modal-submit-button").hide();
                $("button#modal-cancel-button").text("schließen");
                break;
        }
        $("#modal-submit-button").text(submitButtonText);
        $.ajax({
            url: employeeCertRelFormUrl,
            dataType: "html",
            success: function (data) {
                $("div#certrel_form_dyn").append(data);
            }
        }).done(function (data) {
            /*
             * Weil das globale NICHT funzt mit dynamisch (spaeter) generiertem Code!!!
             */
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                firstDay: 1
            });
        });
    });

//        $("form[name='employee_cert_rel_form']").submit(function (event) {
    $("button#modal-submit-button").click(function () {
        var certDefValue = $("#cert_def_value").val();
        var dateOfIssue = $("input[name='date_of_issue']").val();
        var licenceNo = $("input[name='licence_no']").val();
        var certRestriction = $("input[name='cert_restriction']").val();
        var employeeCertRelId = $("input[name='employee_cert_rel_id']").val();

        var submitAjaxUrl = null;
        var submitAjaxMethod = null;
        var submitAjaxDataType = null;
        var submitAjaxProcessData = true;
        var submitAjaxContentType = "application/x-www-form-urlencoded; charset=UTF-8";
        var submitAjaxData = null;
        switch (buttonAction) {
            case "add":
            case "replace":
                submitAjaxUrl = "/rest-employee-cert-rel";
                submitAjaxMethod = "post";
                submitAjaxDataType = "json";
                submitAjaxData = {certrelDetails: {"employeeId": employeeId, "certId": certId, "certDefValue": certDefValue, "dateOfIssue": dateOfIssue, "licenceNo": licenceNo, "certRestriction": certRestriction}};
                break;
            case "edit":
                submitAjaxUrl = "/rest-employee-cert-rel/" + employeeCertRelId;
                submitAjaxMethod = "put";
                submitAjaxDataType = "json";
                submitAjaxData = {certrelDetails: {"employeeCertRelId": employeeCertRelId, "employeeId": employeeId, "certId": certId, "certDefValue": certDefValue, "dateOfIssue": dateOfIssue, "licenceNo": licenceNo, "certRestriction": certRestriction}};
                break;
            case "delete":
                submitAjaxMethod = "delete";
                submitAjaxDataType = "json";
                submitAjaxUrl = "/rest-employee-cert-rel/" + employeeCertRelId;
                break;
            case "documents":
                submitAjaxMethod = "post";
                submitAjaxDataType = "json";
                submitAjaxProcessData = false; // Don't process the files
                submitAjaxUrl = "/employee-cert-rel-upload-document/" + certRelId; // hier AJAX mit certRelId aus button.manage-cert
                submitAjaxData = getUploadFormData();
                submitAjaxContentType = false; // Set content type to false as jQuery will tell the server its a query string request
                break;
        }
        
        $.ajax({
            url: submitAjaxUrl,
            method: submitAjaxMethod,
            dataType: submitAjaxDataType,
            processData: submitAjaxProcessData,
            contentType: submitAjaxContentType,
            data: submitAjaxData,
            success: function (data) {
//                console.log(JSON.stringify(data));
                if(data.employeecertRelHtml) {
                    $("div.employeeid_" + employeeId + "_certid_" + certId).html(htmlDecode(data.employeecertRelHtml));
                } else {
                    document.location.reload();
                }
            },
            error: function () {
                alert("Fehler beim Speichern. Bitte den Administrator benachrichtigen.");
            }
        }).done(function (data) {
            $("#employeeCertModal").modal('toggle');
            certId = 0;
            employeeId = 0;
            certRelId = 0;
            buttonAction = "";
        });


        /*
         * aufhalten des form submits
         */
//            event.preventDefault();
//            return false;
    });


    $(document).on("click", "button.delete-employee-cert-rel-doc", function () {
        var employeeCertRelDocId = $(this).data("employeecertreldocid");
//        console.log("employeeCertRelDocId: " + employeeCertRelDocId);

        $.ajax({
            url: "/rest-employee-cert-rel-doc/" + employeeCertRelDocId,
            method: "delete",
            dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                $("div.employeeid_" + employeeId + "_certid_" + certId).html(htmlDecode(data.employeecertRelHtml));
            },
            error: function () {
                alert("Fehler beim Löschen. Bitte den Administrator benachrichtigen.");
            }
        }).done(function (data) {
            $("#employeeCertModal").modal('toggle');
            certId = 0;
            employeeId = 0;
            certRelId = 0;
            buttonAction = "";
        });
    });
});

function getUploadFormData() {
    var inputElement = document.getElementById("employeeCertReldoc");
    var formdata = new FormData();
    for (var i = 0; i < inputElement.files.length; i++) {
        formdata.append("employee_cert_rel_files[]", inputElement.files[i]);
    }
    return formdata;
}