function toggleMenu(id) {
    var e = document.getElementById(id);
    if (e.className.indexOf('w3-show') === -1) {
        e.className += ' w3-show';
    } else {
        e.className = e.className.replace(' w3-show', '');
    }
}

function toggleW3(id) {
    var toggleme = $("#" + id);
    toggleme.css("z-index", 5);
    if (!toggleme.hasClass("w3-show")) {
        $("#" + id).addClass("w3-show");
        $("#" + id).css("z-index", "1");
        $("#" + id).removeClass("w3-hide");
        var elementId = "";
        $(".w3-dropdown-content").each(function (index, element) {
            elementId = $(element).attr("id");
            if (elementId != id) {
                $(element).addClass("w3-hide");
                $(element).removeClass("w3-show");
            }
        });
    } else {
        $("#" + id).addClass("w3-hide");
        $("#" + id).removeClass("w3-show");
    }
}

$(document).ready(function () {
    $(document).on("click", ".togglefield", function () {
//        console.log("togglefield");
        $(".w3-dropdown-content").each(function (index, element) {
            $(element).addClass("w3-hide");
            $(element).removeClass("w3-show");
        });
    });
});

$(window).scroll(function () {
//    $("#debug").text($(window).scrollTop());
    var currentScrollTop = $(window).scrollTop();
    if (currentScrollTop >= 70) {
        $("#topnav").css("position", "fixed");
        $("#topnav").css("width", $("#overall").css("width"));
    } else if (currentScrollTop < 53) { // Hoehe der nav
        $("#topnav").css("position", "relative");
    }
});
$(window).resize(function () {
    $("#topnav").css("width", $("#overall").css("width"));
});
