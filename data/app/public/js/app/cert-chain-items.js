function toggleCheckboxes(source, inputName) {
    checkboxes = document.getElementsByName(inputName);
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}

$(document).ready(function () {

    var certChainId = 0;

    $(document).on("click", "button#cert_chain_item_modal_button", function () {
        certChainId = $(this).data("certchainid");

        $.ajax({
            url: '/certchainitemsform/' + certChainId,
            dataType: "html",
            method: "post",
            success: function (data) {
                $("div#cert_chain_item_modal_content").html(data);
            }
        }).done(function (data) {

        });
    });

});