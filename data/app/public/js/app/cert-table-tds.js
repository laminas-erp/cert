
$(document).ready(function () {

    /*
     * MUSS hier damit es in mehreren EventHandlern (on & submit) sichtbar ist
     */
    var tabletdid = 0;
    var certId = 0;

    /*
     * Hier MUSS es
     * a) document; b) on()
     * sein. Sonst triggern die dynamisch generierten Button nicht.
     */
    $(document).on("change", "select[name='cert_id']", function () {
        tabletdid = $(this).data("tabletdid");
        certId = $(this).val();

        var thisSelect = $(this);
//        alert("tabletdid: " + tabletdid + "; certId: " + certId);

        /*
         * CertTableTd updaten
         */
        $.ajax({
            url: '/rest-cert-table-td/' + tabletdid,
            dataType: "json",
            method: "put",
            data: {certTableTdData: {"certId": certId}},
            success: function (data) {
                if(data.certId == 0) {
                    thisSelect.removeClass("border-success");
                    thisSelect.addClass("border-warning");
                } else {
                    thisSelect.removeClass("border-warning");
                    thisSelect.addClass("border-success");
                }
            }
        }).done(function (data) {
            
        });
    });


//    $.ajax({
//        url: restUrl,
//        dataType: "json",
//        method: restMethod,
//        data: restData,
//        success: function (data) {
//            $("div#" + employeeId + "_" + certId).html(htmlDecode(data.employeecertRelHtml));
//        },
//        error: function () {
//            alert("Fehler beim Speichern des Mitarbeiter Zertifikats. Bitte den Administrator benachrichtigen.");
//        }
//    }).done(function (data) {
//        console.log(data);
//        $("#employeeCertModal").modal('toggle');
//        certId = 0;
//        employeeId = 0;
//        certRelId = 0;
//        buttonAction = "";
//    });

    /*
     * aufhalten des form submits
     */
//            event.preventDefault();
//            return false;
});