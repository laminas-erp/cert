-- zeigt NUR existierende certRels
SELECT 
    ecr.*, c.duration, c.task_all, e.name_1
FROM
    employee e
        LEFT JOIN
    employee_cert_rel ecr ON ecr.employee_id = e.employee_id
        LEFT JOIN
    cert c ON c.cert_id = ecr.employee_cert_rel_cert_id
WHERE
    ecr.employee_id IN (SELECT 
            employee_id
        FROM
            employee_group_rel
        WHERE
            employee_group_id IN (SELECT 
                    employee_group_id
                FROM
                    employee_group_cert_task))
ORDER BY employee_cert_rel_remaining_time_days ASC , date_of_issue DESC;