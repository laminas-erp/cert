CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_get_certtable_with_employeecertrels`(p_cert_table_id int, p_employee_id int)
BEGIN
SELECT ctt.*, ct.*, c.*, cd.*, ecr.*
FROM cert_table_td ctt
LEFT JOIN cert_table ct ON ct.cert_table_id = ctt.cert_table_id
LEFT JOIN cert c ON c.cert_id = ctt.cert_id
LEFT JOIN cert_def cd ON cd.cert_def_id = c.cert_def_id
LEFT JOIN employee_cert_rel ecr ON ecr.employee_id = p_employee_id AND ecr.employee_cert_rel_cert_id = c.cert_id
WHERE ct.cert_table_id = p_cert_table_id
ORDER BY ctt.cert_table_id ASC, ctt.cert_table_row_index ASC, ctt.cert_table_column_index ASC;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_get_certtable_with_employeecertrels`(p_cert_table_group_id int, p_employee_id int)
BEGIN
SELECT ctt.*, ct.*, ctg.*, c.*, cd.*, ecr.*
FROM cert_table_td ctt
LEFT JOIN cert_table ct ON ct.cert_table_id = ctt.cert_table_id
LEFT JOIN cert_table_group ctg ON ctg.cert_table_group_id = ct.cert_table_group_id
LEFT JOIN cert c ON c.cert_id = ctt.cert_id
LEFT JOIN cert_def cd ON cd.cert_def_id = c.cert_def_id
LEFT JOIN employee_cert_rel ecr ON ecr.employee_id = p_employee_id AND ecr.employee_cert_rel_cert_id = c.cert_id
WHERE ct.cert_table_group_id = p_cert_table_group_id
ORDER BY ct.cert_table_order ASC, ctt.cert_table_row_index ASC, ctt.cert_table_column_index ASC;
END