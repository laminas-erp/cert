SELECT 
    ecr.*, e.*, c.*
FROM
    employee_cert_rel ecr
        LEFT JOIN
    employee e USING (employee_id)
        LEFT JOIN
    cert c ON c.cert_id = ecr.employee_cert_rel_cert_id
WHERE
    c.task_all = 1 AND e.is_active = 1
    ORDER BY e.name_2 ASC , c.cert_order ASC;