SELECT ctt.*, ct.*, c.*, cd.*
FROM cert_table_td ctt
LEFT JOIN cert_table ct ON ct.cert_table_id = ctt.cert_table_id
LEFT JOIN cert c ON c.cert_id = ctt.cert_id
LEFT JOIN cert_def cd ON cd.cert_def_id = c.cert_def_id
WHERE ct.cert_table_id = 1
ORDER BY ctt.cert_table_id ASC, ctt.cert_table_row_index ASC, ctt.cert_table_column_index ASC;