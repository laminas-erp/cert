SELECT 
    CONCAT_WS('_', egct.cert_id, egr.employee_id) AS cid_eid,
    egct.*,
    egr.*,
    e.*,
    c.*
FROM
    employee_group_cert_task egct
        LEFT JOIN
    employee_group_rel egr USING (employee_group_id)
        LEFT JOIN
    employee e USING (employee_id)
        LEFT JOIN
    cert c ON c.cert_id = egct.cert_id
WHERE
    e.is_active = 1
ORDER BY e.name_2 ASC , c.cert_order ASC;