-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: gfm_staff
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_config`
--

DROP TABLE IF EXISTS `app_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_config` (
  `app_config_id` int unsigned NOT NULL AUTO_INCREMENT,
  `app_config_name` varchar(100) NOT NULL,
  `app_config_desc` text,
  `app_config_value` text,
  PRIMARY KEY (`app_config_id`),
  UNIQUE KEY `app_config_name_UNIQUE` (`app_config_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_config`
--

LOCK TABLES `app_config` WRITE;
/*!40000 ALTER TABLE `app_config` DISABLE KEYS */;
INSERT INTO `app_config` VALUES (1,'overview_emails','Emailadressen als CSV zu denen die Emails aus dem Cron Job overview_email gesendet werden','amandia@t-brieskorn.de'),(2,'pdf_header_topline','Die oberste Zeile des PDF Headers in kleiner Schrift','Diese Freigabe und Abschreibeberechtigung ist nur im Ramen der EASA Teil 21G, Teil 145 und Teil MG Genehmigung mit den zugehörigen scope of work gültig.');
/*!40000 ALTER TABLE `app_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cert`
--

DROP TABLE IF EXISTS `cert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cert` (
  `cert_id` int unsigned NOT NULL AUTO_INCREMENT,
  `duration` int NOT NULL DEFAULT '0' COMMENT 'Laufzeit in Monaten; 0=unendlich',
  `cert_name` tinytext,
  `cert_desc` text NOT NULL,
  `task_all` int NOT NULL DEFAULT '0' COMMENT 'Pflicht fuer alle Mitarbeiter; 0=nein; 1=ja',
  `cert_group_id` int unsigned NOT NULL COMMENT 'Ein Zertifikat gehört nur zu einer Gruppe',
  `cert_def_id` int unsigned NOT NULL,
  `cert_group_sub` varchar(45) NOT NULL DEFAULT '' COMMENT 'wenn, dann nur fuer Schweissscheine benoetigt',
  `cert_order` int NOT NULL DEFAULT '0' COMMENT 'order priority',
  `cert_active` int NOT NULL DEFAULT '1',
  `cert_name_visible` int unsigned NOT NULL DEFAULT '0' COMMENT 'z.B. in PDFs wenn 1 dann cert_name in TDs',
  `cert_def_value_visible` int NOT NULL DEFAULT '1',
  `cert_expirydate_visible` int NOT NULL DEFAULT '0',
  `cert_restriction_visible` int NOT NULL DEFAULT '0',
  `cert_value_for_empty_employee_cert_rel` varchar(45) NOT NULL DEFAULT '-',
  PRIMARY KEY (`cert_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cert`
--

LOCK TABLES `cert` WRITE;
/*!40000 ALTER TABLE `cert` DISABLE KEYS */;
INSERT INTO `cert` VALUES (5,12,'Gabelstapler','',0,100,1,'',30,1,1,1,0,0,'-'),(6,12,'Arbeitssicherheit','',1,100,1,'',40,1,0,1,0,0,'-'),(7,24,'Sehtest','',0,100,1,'',0,1,1,0,1,1,'N/A'),(8,24,'Ersthelfer','',0,100,1,'',0,1,0,1,0,0,'-'),(9,24,'Human Factor','',1,100,1,'',20,1,0,1,0,0,'-'),(10,24,'Continuation Training','',0,100,1,'',60,1,0,1,0,0,'-'),(11,0,'Wareneingang','',0,100,3,'',10,1,0,1,0,0,'-'),(12,0,'Sonstiges','',0,100,4,'',70,1,0,1,0,0,'-'),(13,0,'CAMO Engineering','',0,100,1,'',80,1,0,1,0,0,'-'),(14,0,'MPI','',0,4,1,'',90,1,0,1,0,0,'-'),(15,0,'MPL','',0,4,1,'',100,1,0,1,0,0,'-'),(16,0,'LEB','',0,4,1,'',110,1,0,1,0,0,'-'),(17,0,'MPI FW/TW','',0,4,1,'',120,1,0,1,0,0,'-'),(18,0,'MPL FW/TW','',0,4,1,'',130,1,0,1,0,0,'-'),(19,0,'MPL Stellvertreter','',0,4,1,'',140,1,0,1,0,0,'-'),(20,0,'LEB Stellvertreter','',0,4,1,'',150,1,0,1,0,0,'-'),(21,0,'Elektrik / Avionik','',0,4,1,'',160,1,0,1,0,0,'-'),(22,24,'Schweiß-Lizenz','Einzelheiten zu den jeweiligen Zertifikaten stehen in der Rubrik Schweiß-Lizenzen',0,100,1,'',50,1,0,1,0,0,'-'),(23,12,'21G Mech. Metall','21G Betrieb CS-KH Mech. Metall',0,3,3,'',0,1,0,1,0,0,'-'),(24,12,'21G Mech. Holz','21G Betrieb CS-KH Mech. Holz',0,3,3,'',0,1,0,1,0,0,'-'),(25,12,'21G Mech. Gemischt','21G Betrieb CS-KH Mech. Gemischt',0,3,3,'',0,1,0,1,0,0,'-'),(29,12,'21G Mech. FVK','21G Betrieb CS-KH Mech. FVK',0,3,3,'',0,1,0,1,0,0,'-'),(30,12,'21G Mech. Hydraulik','21G Betrieb CS-KH Mech. Hydraulik',0,3,3,'',0,1,0,1,0,0,'-'),(31,12,'21G Mech. Fahrwerk','21G Betrieb CS-KH Mech. Fahrwerk',0,3,3,'',0,1,0,1,0,0,'-'),(32,12,'21G Mech. Schalldämpfer','21G Betrieb CS-KH Mech. Schalldämpfer',0,3,3,'',0,1,0,1,0,0,'-'),(33,12,'21G Mech. allg. Baugruppen','21G Betrieb CS-KH Mech. allg. Baugruppen',0,3,3,'',0,1,0,1,0,0,'-'),(34,12,'21G Mech. Flugzeug Struktur','21G Betrieb CS-KH Mech. Flugzeug Struktur',0,3,3,'',0,1,0,1,0,0,'-'),(35,12,'21G Mech. Hubschrauber Struktur','21G Betrieb CS-KH Mech. Hubschrauber Struktur',0,3,3,'',0,1,0,1,0,0,'-'),(36,12,'21G Mech. Calif','21G Betrieb CS-KH Mech. Calif',0,3,3,'',0,1,0,1,0,0,'-'),(37,12,'21G Mech. C-1 Rating (limitiert CS-KH)','21G Betrieb CS-KH Mech. C-1 Rating (limitiert CS-KH)',0,3,3,'',0,1,0,1,0,0,'-'),(38,12,'21G Mech. C-2 Rating (limitiert CS-KH)','21G Betrieb CS-KH Mech. C-2 Rating (limitiert CS-KH)',0,3,3,'',0,1,0,1,0,0,'-'),(39,12,'21G Schweißer Metall','21G Betrieb CS-KH Schweißer Metall',0,3,3,'',0,1,0,1,0,0,'-'),(40,12,'21G Schweißer Holz','21G Betrieb CS-KH Schweißer Holz',0,3,3,'',0,1,0,1,0,0,'-'),(41,12,'21G Schweißer Gemischt','21G Betrieb CS-KH Schweißer Gemischt',0,3,3,'',0,1,0,1,0,0,'-'),(42,12,'21G Schweißer FVK','21G Betrieb CS-KH Schweißer FVK',0,3,3,'',0,1,0,1,0,0,'-'),(43,12,'21G Schweißer Hydraulik','21G Betrieb CS-KH Schweißer Hydraulik',0,3,3,'',0,1,0,1,0,0,'-'),(44,12,'21G Schweißer Fahrwerk','21G Betrieb CS-KH Schweißer Fahrwerk',0,3,3,'',0,1,0,1,0,0,'-'),(45,12,'21G Schweißer Schalldämpfer','21G Betrieb CS-KH Schweißer Schalldämpfer',0,3,3,'',0,1,0,1,0,0,'-'),(46,12,'21G Schweißer allg. Baugruppen','21G Betrieb CS-KH Schweißer allg. Baugruppen',0,3,3,'',0,1,0,1,0,0,'-'),(47,12,'21G Schweißer Flugzeug Struktur','21G Betrieb CS-KH Schweißer Flugzeug Struktur',0,3,3,'',0,1,0,1,0,0,'-'),(48,12,'21G Schweißer Hubschrauber Struktur','21G Betrieb CS-KH Schweißer Hubschrauber Struktur',0,3,3,'',0,1,0,1,0,0,'-'),(49,12,'21G Schweißer Calif','21G Betrieb CS-KH Schweißer Calif',0,3,3,'',0,1,0,1,0,0,'-'),(50,12,'21G Schweißer C-1 Rating (limitiert CS-KH)','21G Betrieb CS-KH Schweißer C-1 Rating (limitiert CS-KH)',0,3,3,'',0,1,0,1,0,0,'-'),(51,12,'21G Schweißer C-2 Rating (limitiert CS-KH)','21G Betrieb CS-KH Schweißer C-2 Rating (limitiert CS-KH)',0,3,3,'',0,1,0,1,0,0,'-'),(52,2,'testZertifikat','eine Beschreibung fürs Testzert',0,100,3,'',30,1,0,1,0,0,'-'),(53,12,'21G Schweißer C-2 Rating (limitiert CS-KH)','21G Betrieb CS-KH Schweißer C-2 Rating (limitiert CS-KH)',0,3,3,'',0,1,0,1,0,0,'-'),(54,12,'21G Schweißer Metall','21G Betrieb CS-KH Schweißer Metall',0,3,3,'',0,1,0,1,0,0,'-'),(55,0,'C1','',0,2,5,'',0,1,0,1,0,0,'-'),(56,0,'C4','',0,2,5,'',0,1,0,1,0,0,'-'),(57,0,'C6','',0,2,5,'',0,1,0,1,0,0,'-'),(58,0,'C7','',0,2,5,'',0,1,0,1,0,0,'-'),(59,0,'C8','',0,2,5,'',0,1,0,1,0,0,'-'),(60,0,'C9','',0,2,5,'',0,1,0,1,0,0,'-'),(61,0,'C12','',0,2,5,'',0,1,0,1,0,0,'-'),(62,0,'C14','',0,2,5,'',0,1,0,1,0,0,'-'),(63,0,'C15','',0,2,5,'',0,1,0,1,0,0,'-'),(64,0,'C17','',0,2,5,'',0,1,0,1,0,0,'-'),(65,0,'C20','',0,2,5,'',0,1,0,1,0,0,'-');
/*!40000 ALTER TABLE `cert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cert_chain`
--

DROP TABLE IF EXISTS `cert_chain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cert_chain` (
  `cert_chain_id` int unsigned NOT NULL AUTO_INCREMENT,
  `cert_chain_name` varchar(80) NOT NULL,
  `cert_chain_desc` tinytext,
  `cert_chain_type` enum('group','master') NOT NULL DEFAULT 'group' COMMENT 'group=if any then all too; master=if master then the other too',
  `cert_chain_chain_type` enum('update','create_update') NOT NULL DEFAULT 'update',
  PRIMARY KEY (`cert_chain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cert_chain`
--

LOCK TABLES `cert_chain` WRITE;
/*!40000 ALTER TABLE `cert_chain` DISABLE KEYS */;
INSERT INTO `cert_chain` VALUES (1,'erster Schweißer','der erste Schweißer mit Doppelten zum zweiten','group','create_update');
/*!40000 ALTER TABLE `cert_chain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cert_chain_item`
--

DROP TABLE IF EXISTS `cert_chain_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cert_chain_item` (
  `cert_chain_item_id` int unsigned NOT NULL AUTO_INCREMENT,
  `cert_chain_id` int unsigned NOT NULL,
  `cert_id` int unsigned NOT NULL,
  `cert_chain_item_type` enum('item','master') NOT NULL DEFAULT 'item',
  PRIMARY KEY (`cert_chain_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cert_chain_item`
--

LOCK TABLES `cert_chain_item` WRITE;
/*!40000 ALTER TABLE `cert_chain_item` DISABLE KEYS */;
INSERT INTO `cert_chain_item` VALUES (27,1,39,'item'),(29,1,41,'item'),(30,1,42,'item'),(31,1,43,'item'),(32,1,44,'item'),(33,1,45,'item'),(34,1,46,'item');
/*!40000 ALTER TABLE `cert_chain_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cert_def`
--

DROP TABLE IF EXISTS `cert_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cert_def` (
  `cert_def_id` int unsigned NOT NULL AUTO_INCREMENT,
  `cert_def_name` tinytext NOT NULL,
  `cert_def_def` text NOT NULL COMMENT 'JSON',
  `cert_def_desc` text,
  PRIMARY KEY (`cert_def_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cert_def`
--

LOCK TABLES `cert_def` WRITE;
/*!40000 ALTER TABLE `cert_def` DISABLE KEYS */;
INSERT INTO `cert_def` VALUES (1,'voll oder teilweise','{\"type\": \"select\",\"values\":{\"1\":\"voll\",\"2\":\"teilw\"}}',''),(2,'K, KT oder ja','{\"type\": \"select\",\"values\": {\"1\": \"Ja\",\"2\": \"Kolbentriebwerk\",\"3\": \"Kolbentriebwerk auch mit Turbolader\"}}',NULL),(3,'bestanden Ja | nein','{\"type\": \"checkbox\",\"value\":\"ok\"}',''),(4,'sonstiges','{\"type\": \"text\"}',NULL),(5,'145 Komp. Rep.','{\"type\": \"select\",\"values\": {\"1\": \"X\",\"2\": \"CS-KI\"}}',NULL);
/*!40000 ALTER TABLE `cert_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cert_group`
--

DROP TABLE IF EXISTS `cert_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cert_group` (
  `cert_group_id` int unsigned NOT NULL AUTO_INCREMENT,
  `cert_group_name` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `cert_group_order` int DEFAULT NULL,
  PRIMARY KEY (`cert_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cert_group`
--

LOCK TABLES `cert_group` WRITE;
/*!40000 ALTER TABLE `cert_group` DISABLE KEYS */;
INSERT INTO `cert_group` VALUES (1,'Teil 145 A-Rating Betrieb','Abschreibeberechtigung Teil 145 Betrieb A-Rating',90),(2,'Teil 145 C-Rating Betrieb','Abschreibeberechtigung Teil 145 Betrieb C-Rating',95),(3,'EASA Teil 21G Betrieb','Abschreibeberechtigung im EASA Teil 21G Betrieb (Herstellung)',80),(4,'EASA Teil 21J Betrieb','EASA Teil-21J Betrieb (Entwicklung)',75),(5,'EASA Teil MG','Berechtigung zur Durchführung der Prüfung zur Aufrechterhaltung der Lufttüchtigkeit von Luftfahrzeugen gemäß EASA Teil MG DE.MG.0169. (CAMO)',50),(6,'Schweißen','Einzelheiten zu den Schweißlizenzen siehe Schweißlizenzen',72),(7,'LBA 145 C-Rating Freigabe','Freigabeberechtigung im Rahmen der LBA 145 Genehmigung LBA.145.0169 gemäß dem C - Rating und der Cap. List \"LBA.145.0169-Bauteile\"',40),(100,'Allgemein','Allgemeine Zulassungen - gültig siehe Datum',70),(101,'LBA 145 Freigabe','Freigabeberechtigung im Rahmen der LBA 145 Genehmigung LBA.145.0169 und der Cap. List \"LBA.145.0169-Flugzeuge\"',35),(102,'LBA 21G Freigabe','Freigabeberechtigung im Rahmen der LBA 21G Genehmigung  LBA.21G.0045 und der Cap. List \"LBA.21G.0045-Herstellung-Bauteile\"',30),(103,'EASA Teil 21G C-Rating','Freigabeberechtigung im Rahmen der EASA Teil 21G Genehmigung DE.21G.0045 und der Cap. List \"DE.21G.0045-C-Rating\"',20),(104,'Teil 145 C-Rating Freigabe','Freigabeberechtigung im Rahmen des Teil 145 Betriebs DE.145.0169 gemäß dem C - Rating und der Cap. List \"DE.145.0169-C-Rating\"',15),(105,'Teil 145 A-Rating Freigabe','Freigabeberechtigung im Rahmen des Teil 145 Betriebs DE.145.0169 gemäß dem A - Rating und dem scope of work der GFM',10);
/*!40000 ALTER TABLE `cert_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cert_table`
--

DROP TABLE IF EXISTS `cert_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cert_table` (
  `cert_table_id` int unsigned NOT NULL AUTO_INCREMENT,
  `cert_table_group_id` int unsigned DEFAULT NULL,
  `cert_table_row_count` int unsigned NOT NULL DEFAULT '1',
  `cert_table_column_count` int unsigned NOT NULL DEFAULT '1',
  `cert_table_heading` tinytext NOT NULL,
  `cert_table_order` int DEFAULT NULL,
  `cert_table_row_names` tinytext COMMENT 'CSV, if NOT empty, CSV entries must count the same as cert_table_row_count',
  `cert_table_column_names` tinytext COMMENT 'CSV, if NOT empty, CSV entries must count the same as cert_table_column_count',
  PRIMARY KEY (`cert_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cert_table`
--

LOCK TABLES `cert_table` WRITE;
/*!40000 ALTER TABLE `cert_table` DISABLE KEYS */;
INSERT INTO `cert_table` VALUES (11,1,3,4,'Allgemeine Zulassungen',100,'',''),(12,1,4,13,'EASA Teil 21G Betrieb',90,'Mechaniker,Schweißer,Zerspaner,Laserschneider','Metall,Holz,Gemischt,FVK,Hydraulik,Fahrwerk,Schalldämpfer,allg. Baugruppen,Flugzeug Struktur,Hubschrauber Struktur,Segelf. Calif,C-1 Rating, C-2 Rating'),(13,1,2,4,'EASA Teil 21J Betrieb',95,'',''),(14,2,2,3,'TestÜberschrift',20,'','col-1,col-2,col-3'),(15,1,1,11,'Component Repair',0,'C-Rating','');
/*!40000 ALTER TABLE `cert_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cert_table_group`
--

DROP TABLE IF EXISTS `cert_table_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cert_table_group` (
  `cert_table_group_id` int unsigned NOT NULL AUTO_INCREMENT,
  `cert_table_group_name` varchar(200) NOT NULL,
  `cert_table_group_desc` tinytext,
  `cert_table_group_expiry` int unsigned NOT NULL DEFAULT '0' COMMENT 'UNUSED: ne tablegroup hat kein Ablaufdatum, nur ein tablegroup-attribute kann so etwas',
  PRIMARY KEY (`cert_table_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cert_table_group`
--

LOCK TABLES `cert_table_group` WRITE;
/*!40000 ALTER TABLE `cert_table_group` DISABLE KEYS */;
INSERT INTO `cert_table_group` VALUES (1,'Freigabe und Abschreibeberechtigung','so wie früher',0),(2,'TestGruppe','eine beschreibung für TestGruppe',0);
/*!40000 ALTER TABLE `cert_table_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cert_table_td`
--

DROP TABLE IF EXISTS `cert_table_td`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cert_table_td` (
  `cert_table_td_id` int unsigned NOT NULL AUTO_INCREMENT,
  `cert_table_id` int unsigned NOT NULL,
  `cert_table_row_index` int unsigned NOT NULL COMMENT 'fortlaufend beginnend mit 1; <= cert_table_row_count',
  `cert_table_column_index` int unsigned NOT NULL COMMENT 'fortlaufend beginnend mit 1; <= cert_table_column_count',
  `cert_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`cert_table_td_id`),
  KEY `fk_cert_table_td_cert_table_idx` (`cert_table_id`),
  KEY `fk_cert_table_td_cert_idx` (`cert_id`),
  CONSTRAINT `fk_cert_table_td_cert` FOREIGN KEY (`cert_id`) REFERENCES `cert` (`cert_id`),
  CONSTRAINT `fk_cert_table_td_cert_table` FOREIGN KEY (`cert_table_id`) REFERENCES `cert_table` (`cert_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=652 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cert_table_td`
--

LOCK TABLES `cert_table_td` WRITE;
/*!40000 ALTER TABLE `cert_table_td` DISABLE KEYS */;
INSERT INTO `cert_table_td` VALUES (523,11,1,1,11),(524,11,1,2,5),(525,11,1,3,12),(526,11,1,4,7),(527,11,2,1,9),(528,11,2,2,6),(529,11,2,3,13),(530,11,2,4,8),(531,11,3,1,10),(532,11,3,2,22),(533,11,3,3,NULL),(534,11,3,4,NULL),(539,12,1,1,23),(540,12,1,2,24),(541,12,1,3,25),(542,12,1,4,29),(543,12,1,5,30),(544,12,1,6,31),(545,12,1,7,32),(546,12,1,8,33),(547,12,1,9,34),(548,12,1,10,35),(549,12,1,11,36),(550,12,1,12,37),(551,12,1,13,38),(553,12,2,1,39),(554,12,2,2,NULL),(555,12,2,3,41),(556,12,2,4,42),(557,12,2,5,43),(558,12,2,6,44),(559,12,2,7,45),(560,12,2,8,46),(561,12,2,9,47),(562,12,2,10,48),(563,12,2,11,49),(564,12,2,12,50),(565,12,2,13,51),(567,12,3,1,NULL),(568,12,3,2,NULL),(569,12,3,3,NULL),(570,12,3,4,NULL),(571,12,3,5,NULL),(572,12,3,6,NULL),(573,12,3,7,NULL),(574,12,3,8,NULL),(575,12,3,9,NULL),(576,12,3,10,NULL),(577,12,3,11,NULL),(578,12,3,12,NULL),(579,12,3,13,NULL),(595,13,1,1,17),(596,13,1,2,18),(597,13,1,3,NULL),(598,13,1,4,21),(599,13,2,1,NULL),(600,13,2,2,19),(601,13,2,3,20),(602,13,2,4,NULL),(616,12,4,1,NULL),(617,12,4,2,NULL),(618,12,4,3,NULL),(619,12,4,4,NULL),(620,12,4,5,NULL),(621,12,4,6,NULL),(622,12,4,7,NULL),(623,12,4,8,NULL),(624,12,4,9,NULL),(625,12,4,10,NULL),(626,12,4,11,NULL),(627,12,4,12,NULL),(628,12,4,13,NULL),(629,14,1,1,NULL),(630,14,1,2,15),(631,14,1,3,5),(636,15,1,1,55),(637,15,1,2,56),(638,15,1,3,57),(639,15,1,4,58),(640,15,1,5,59),(641,15,1,6,60),(642,15,1,7,61),(643,15,1,8,62),(644,15,1,9,63),(645,15,1,10,64),(646,15,1,11,65),(649,14,2,1,NULL),(650,14,2,2,NULL),(651,14,2,3,NULL);
/*!40000 ALTER TABLE `cert_table_td` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `employee_id` int unsigned NOT NULL AUTO_INCREMENT,
  `name_1` varchar(100) NOT NULL,
  `name_2` varchar(100) DEFAULT NULL,
  `no_timerec` varchar(45) DEFAULT NULL,
  `no_godata_id` int NOT NULL DEFAULT '0',
  `no_addison` varchar(45) DEFAULT NULL,
  `no_extern` int NOT NULL DEFAULT '0',
  `is_active` int NOT NULL DEFAULT '1' COMMENT '0=nein; 1=ja',
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Jon','Doe','',42,'',7827363,1),(2,'Karl','Dall','',27,'',7827363,1),(25,'Frank','Biedebach',NULL,1,NULL,0,1),(26,'Gaby','Halbach',NULL,8,NULL,0,1),(27,'Bernd','Ahlert',NULL,11,NULL,0,1),(28,'Robert','Bussart',NULL,73,NULL,0,1),(29,'Karsten','Schimmel',NULL,14,NULL,0,1),(30,'Hans Peter','Gomolzig',NULL,19,NULL,0,1),(31,'Kurt','Dahlmann',NULL,22,NULL,0,1),(33,'Achim','Kocks',NULL,23,NULL,0,1),(34,'Siggi','Werft Siegerl.',NULL,25,NULL,0,1),(35,'Fernando Toribio','Solis',NULL,26,NULL,0,1),(36,'Manuel','Putz',NULL,27,NULL,0,1),(37,'Peter','Kindermann',NULL,28,NULL,0,1),(38,'Dirk','Siepmann',NULL,56,NULL,0,1),(39,'Karsten','Pollmann',NULL,55,NULL,0,1),(40,'Helmut','Gomolzig',NULL,33,NULL,0,1),(41,'Andreas','Goi',NULL,34,NULL,0,1),(42,'Marvin','Balogun',NULL,35,NULL,0,1),(43,'Steffen','Gomolzig',NULL,72,NULL,0,1),(44,'Nicole','Siegemund',NULL,40,NULL,0,1),(45,'Marcus','Maul',NULL,45,NULL,0,1),(46,'Sabrina','Schuermann',NULL,57,NULL,0,1),(47,'Pia','Wiegand',NULL,60,NULL,0,1),(49,'Wilfried','Broemmelmeyer',NULL,70,NULL,0,1),(50,'Torsten','Brieskorn',NULL,71,NULL,0,1),(51,'Jürgen','Deeg',NULL,75,NULL,0,1),(52,'Carmen','Franke',NULL,77,NULL,0,1);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_cert_rel`
--

DROP TABLE IF EXISTS `employee_cert_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_cert_rel` (
  `employee_cert_rel_id` int unsigned NOT NULL AUTO_INCREMENT,
  `employee_cert_rel_cert_id` int unsigned NOT NULL,
  `employee_id` int unsigned NOT NULL,
  `date_of_issue` int NOT NULL COMMENT 'Ausstellungsdatum',
  `licence_no` varchar(100) DEFAULT NULL COMMENT 'Muß hier weg weil Jeder Prüfling seine eigene licence-no hat??? Aber doch für jedes Zertifikat eine eigene, also hier richtig!?!?',
  `cert_def_value` tinytext NOT NULL COMMENT 'db.cert_def.cert_def_def der ValueKey aus "values" ...siehe JSON',
  `cert_restriction` tinytext,
  `employee_cert_rel_remaining_time_days` int DEFAULT NULL COMMENT 'computed at http request',
  PRIMARY KEY (`employee_cert_rel_id`),
  KEY `fk_employee_cert_rel_employee_id_idx` (`employee_id`),
  KEY `fk_employee_cert_rel_cert_id_idx` (`employee_cert_rel_cert_id`),
  CONSTRAINT `fk_employee_cert_rel_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_cert_rel`
--

LOCK TABLES `employee_cert_rel` WRITE;
/*!40000 ALTER TABLE `employee_cert_rel` DISABLE KEYS */;
INSERT INTO `employee_cert_rel` VALUES (15,39,2,1456873200,'','ok','fast keinex',-23),(16,41,2,1456873200,'','ok','fast keinex',-23),(18,43,2,1456873200,NULL,'ok','fast keinex',-23),(19,44,2,1456873200,NULL,'ok','fast keinex',-23),(20,45,2,1456873200,NULL,'ok','fast keinex',-23),(21,40,2,1456873200,NULL,'ok','fast keinex',-23),(22,25,2,1450911600,'','ok','keine',-91),(23,30,2,1478732400,'','ok','keine',230),(24,7,2,1481842800,'','1','keine',631),(25,6,2,1482361200,'','1','keine',272),(28,22,2,1484780400,'','1','keine',665),(29,9,2,1464732000,'','1','keine',433),(30,6,1,1484780400,'','1','keine',300);
/*!40000 ALTER TABLE `employee_cert_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_cert_rel_archive`
--

DROP TABLE IF EXISTS `employee_cert_rel_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_cert_rel_archive` (
  `employee_cert_rel_id` int unsigned NOT NULL,
  `employee_cert_rel_cert_id` int unsigned NOT NULL,
  `employee_id` int unsigned NOT NULL,
  `date_of_issue` int NOT NULL COMMENT 'Ausstellungsdatum',
  `licence_no` varchar(100) DEFAULT NULL COMMENT 'Muß hier weg weil Jeder Prüfling seine eigene licence-no hat??? Aber doch für jedes Zertifikat eine eigene, also hier richtig!?!?',
  `cert_def_value` tinytext NOT NULL COMMENT 'db.cert_def.cert_def_def der ValueKey aus "values" ...siehe JSON',
  `cert_restriction` tinytext,
  `employee_cert_rel_remaining_time_days` int DEFAULT NULL,
  PRIMARY KEY (`employee_cert_rel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_cert_rel_archive`
--

LOCK TABLES `employee_cert_rel_archive` WRITE;
/*!40000 ALTER TABLE `employee_cert_rel_archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_cert_rel_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_cert_rel_doc`
--

DROP TABLE IF EXISTS `employee_cert_rel_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_cert_rel_doc` (
  `employee_cert_rel_doc_id` int unsigned NOT NULL AUTO_INCREMENT,
  `employee_cert_rel_doc_name` tinytext,
  `employee_cert_rel_id` int unsigned NOT NULL,
  `employee_cert_rel_doc_main_folder_root` varchar(100) DEFAULT NULL,
  `employee_cert_rel_doc_datetime` int unsigned DEFAULT NULL,
  `employee_cert_rel_doc_filename` varchar(100) DEFAULT NULL,
  `employee_cert_rel_doc_fqfn` text COMMENT 'nur zur Kontrolle, um zu sehen wo es bei Erstellung abgelegt wurde',
  `cert_id` int unsigned DEFAULT NULL COMMENT 'muss auch immer da sein',
  `employee_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`employee_cert_rel_doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_cert_rel_doc`
--

LOCK TABLES `employee_cert_rel_doc` WRITE;
/*!40000 ALTER TABLE `employee_cert_rel_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_cert_rel_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_cert_table_group_attribute_def`
--

DROP TABLE IF EXISTS `employee_cert_table_group_attribute_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_cert_table_group_attribute_def` (
  `employee_cert_table_group_attribute_def_id` int unsigned NOT NULL AUTO_INCREMENT,
  `employee_cert_table_group_attribute_def_key` varchar(45) NOT NULL,
  `employee_cert_table_group_attribute_def_type` enum('string','time') NOT NULL DEFAULT 'string' COMMENT 'how to interpret the value',
  `employee_cert_table_group_attribute_def_name` varchar(60) NOT NULL,
  `employee_cert_table_group_attribute_def_dec` tinytext NOT NULL,
  `cert_table_group_id` int unsigned NOT NULL,
  PRIMARY KEY (`employee_cert_table_group_attribute_def_id`),
  UNIQUE KEY `employee_cert_table_group_attribute_def_key_UNIQUE` (`employee_cert_table_group_attribute_def_key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_cert_table_group_attribute_def`
--

LOCK TABLES `employee_cert_table_group_attribute_def` WRITE;
/*!40000 ALTER TABLE `employee_cert_table_group_attribute_def` DISABLE KEYS */;
INSERT INTO `employee_cert_table_group_attribute_def` VALUES (4,'tablegroup_releasewriteoff_expiry','time','Tabellengruppen-Ablaufdatum','so eine Tabellengruppe ist meist nur ein Jahr gültig ...je employee',1);
/*!40000 ALTER TABLE `employee_cert_table_group_attribute_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_cert_table_group_attribute_rel`
--

DROP TABLE IF EXISTS `employee_cert_table_group_attribute_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_cert_table_group_attribute_rel` (
  `employee_cert_table_group_attribute_rel_id` int unsigned NOT NULL AUTO_INCREMENT,
  `employee_cert_table_group_attribute_def_id` int unsigned NOT NULL,
  `employee_id` int unsigned NOT NULL,
  `employee_cert_table_group_attribute_value` varchar(200) NOT NULL,
  PRIMARY KEY (`employee_cert_table_group_attribute_rel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_cert_table_group_attribute_rel`
--

LOCK TABLES `employee_cert_table_group_attribute_rel` WRITE;
/*!40000 ALTER TABLE `employee_cert_table_group_attribute_rel` DISABLE KEYS */;
INSERT INTO `employee_cert_table_group_attribute_rel` VALUES (1,4,2,'1514674800'),(2,4,1,'1514588400'),(3,4,25,'1511996400');
/*!40000 ALTER TABLE `employee_cert_table_group_attribute_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_group`
--

DROP TABLE IF EXISTS `employee_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_group` (
  `employee_group_id` int unsigned NOT NULL AUTO_INCREMENT,
  `employee_group_name` varchar(45) DEFAULT NULL,
  `employee_group_desc` tinytext,
  PRIMARY KEY (`employee_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_group`
--

LOCK TABLES `employee_group` WRITE;
/*!40000 ALTER TABLE `employee_group` DISABLE KEYS */;
INSERT INTO `employee_group` VALUES (1,'CS','certified staff'),(2,'Schweißer',''),(3,'Externer',''),(4,'Büro','z.B. keine pflicht für Human Factor'),(5,'Mechaniker','z.B. Human Factor Pflicht'),(6,'Wareneingang','wegen der Wareneingangszertifizierung'),(7,'Gabelstapler','wegen der Gabelstaplerprüfung');
/*!40000 ALTER TABLE `employee_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_group_cert_task`
--

DROP TABLE IF EXISTS `employee_group_cert_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_group_cert_task` (
  `employee_group_cert_task_id` int unsigned NOT NULL AUTO_INCREMENT,
  `employee_group_id` int unsigned NOT NULL,
  `cert_id` int unsigned NOT NULL,
  PRIMARY KEY (`employee_group_cert_task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_group_cert_task`
--

LOCK TABLES `employee_group_cert_task` WRITE;
/*!40000 ALTER TABLE `employee_group_cert_task` DISABLE KEYS */;
INSERT INTO `employee_group_cert_task` VALUES (1,1,10),(3,1,7),(4,1,9),(5,5,9);
/*!40000 ALTER TABLE `employee_group_cert_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_group_rel`
--

DROP TABLE IF EXISTS `employee_group_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_group_rel` (
  `employee_group_rel_id` int unsigned NOT NULL AUTO_INCREMENT,
  `employee_group_id` int unsigned NOT NULL,
  `employee_id` int unsigned NOT NULL,
  PRIMARY KEY (`employee_group_rel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_group_rel`
--

LOCK TABLES `employee_group_rel` WRITE;
/*!40000 ALTER TABLE `employee_group_rel` DISABLE KEYS */;
INSERT INTO `employee_group_rel` VALUES (1,1,1),(4,1,2);
/*!40000 ALTER TABLE `employee_group_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` int unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `email` varchar(80) NOT NULL,
  `group` enum('admin','manager','user') NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'allapow','172dc98f926c2d66413486c4ff128b263184ed38b53ef19b7884a391fba39983','mail@t-brieskorn.de','admin'),(2,'kurt','172dc98f926c2d66413486c4ff128b263184ed38b53ef19b7884a391fba39983','kurt.dahlmann@t-online.de','user'),(3,'pgo','172dc98f926c2d66413486c4ff128b263184ed38b53ef19b7884a391fba39983','hp.gomolzig@gomolzig.de','user');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'gfm_staff'
--
/*!50003 DROP FUNCTION IF EXISTS `func_get_duration` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_get_duration`(p_cert_id INT(10)) RETURNS int
BEGIN
DECLARE v_duration INT(4) default 0;
SELECT duration INTO v_duration FROM cert WHERE cert_id = p_cert_id;
RETURN v_duration;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_archive_employee_cert_rel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_archive_employee_cert_rel`(p_employee_id int, p_cert_id int)
BEGIN
Declare count_certrel INT(11) default 0;
SELECT 
    COUNT(employee_cert_rel_id)
INTO count_certrel FROM
    employee_cert_rel
WHERE
    employee_id = p_employee_id
        AND employee_cert_rel_cert_id = p_cert_id;

if count_certrel > 0
then
	INSERT INTO employee_cert_rel_archive (
		SELECT * FROM employee_cert_rel
        WHERE employee_id = p_employee_id
        AND employee_cert_rel_cert_id = p_cert_id
	);
	DELETE FROM employee_cert_rel 
WHERE
    employee_id = p_employee_id
    AND employee_cert_rel_cert_id = p_cert_id;
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_archive_employee_cert_rel_if` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_archive_employee_cert_rel_if`(p_employee_id int, p_cert_id int)
BEGIN
Declare count_certrel INT(11) default 0;
SELECT 
    COUNT(employee_cert_rel_id)
INTO count_certrel FROM
    employee_cert_rel
WHERE
    employee_id = p_employee_id
        AND employee_cert_rel_cert_id = p_cert_id;

if count_certrel > 0
then
	INSERT INTO employee_cert_rel_archive (
		SELECT * FROM employee_cert_rel
        WHERE employee_id = p_employee_id
        AND employee_cert_rel_cert_id = p_cert_id
	);
	DELETE FROM employee_cert_rel 
WHERE
    employee_id = p_employee_id
    AND employee_cert_rel_cert_id = p_cert_id;
end if;
SELECT count_certrel;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_create_cert_table_plus_tds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_create_cert_table_plus_tds`(
p_cert_table_group_id int, p_cert_table_row_count int, p_cert_table_column_count int,
p_cert_table_heading tinytext, p_cert_table_order int,
p_cert_table_row_names tinytext, cert_table_column_names tinytext)
BEGIN

DECLARE v_last_table_id INT(10) default 0;
DECLARE v_row INT(10) default 0;
DECLARE v_column INT(10) default 0;
DECLARE v_td_count INT(11) default 0;

INSERT INTO cert_table (`cert_table_id`,
`cert_table_group_id`,
`cert_table_row_count`,
`cert_table_column_count`,
`cert_table_heading`,
`cert_table_order`,
`cert_table_row_names`,
`cert_table_column_names`)
VALUES
(null,
p_cert_table_group_id,
p_cert_table_row_count,
p_cert_table_column_count,
p_cert_table_heading,
p_cert_table_order,
p_cert_table_row_names,
cert_table_column_names);

SELECT LAST_INSERT_ID() INTO v_last_table_id;

SET v_row = 1;
while v_row <= p_cert_table_row_count do
	SET v_column = 1;
	while v_column <= p_cert_table_column_count do
		INSERT INTO cert_table_td (`cert_table_id`,`cert_table_row_index`,`cert_table_column_index`) VALUES (v_last_table_id, v_row,v_column);
		set v_column = v_column + 1;
        set v_td_count = v_td_count + 1;
	END while;
	set v_row = v_row + 1;
END while;
SELECT CONCAT_WS('_',v_last_table_id, v_td_count) as insert_result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_delete_cert_table_plus_tds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_delete_cert_table_plus_tds`(p_cert_table_id INT(10))
BEGIN
DECLARE v_table_count INT(10) default 0;
DECLARE v_td_count INT(10) default 0;
DELETE FROM cert_table_td 
WHERE
    cert_table_id = p_cert_table_id;
SELECT ROW_COUNT() INTO v_td_count;
DELETE FROM cert_table 
WHERE
    cert_table_id = p_cert_table_id;
SELECT ROW_COUNT() INTO v_table_count;
SELECT CONCAT_WS('_', v_table_count, v_td_count) AS delete_result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_edit_cert_table_including_dimension_plus_tds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_edit_cert_table_including_dimension_plus_tds`(
		p_cert_table_id int,
		p_cert_table_group_id int,
		p_cert_table_row_count int,
		p_cert_table_column_count int,
		p_cert_table_heading tinytext,
		p_cert_table_order int,
		p_cert_table_row_names tinytext,
		cert_table_column_names tinytext
	)
BEGIN
DECLARE v_last_cert_table_row_count INT(5) default 0;

DECLARE v_last_cert_table_column_count INT(5) default 0;

DECLARE v_difference_cert_table_row_count INT(5) default 0;

DECLARE v_difference_cert_table_column_count INT(5) default 0;

DECLARE v_row INT(10) default 0;

DECLARE v_column INT(10) default 0;

DECLARE v_td_count INT(11) default 0;

DECLARE v_td_count_tmp INT(11) default 0;

DECLARE v_update_count INT(11) default 0;

SELECT
	cert_table_row_count,
	cert_table_column_count INTO
		v_last_cert_table_row_count,
		v_last_cert_table_column_count
	FROM
		cert_table
	WHERE
		cert_table_id = p_cert_table_id;



SET
v_difference_cert_table_row_count = p_cert_table_row_count - v_last_cert_table_row_count;
SET
v_difference_cert_table_column_count = p_cert_table_column_count - v_last_cert_table_column_count;



IF v_difference_cert_table_column_count > 0 THEN 

SET
v_row = 1;

while v_row <= v_last_cert_table_row_count do
SET
v_column = v_last_cert_table_column_count + 1;

while v_column <= p_cert_table_column_count do INSERT
	INTO
		cert_table_td(
			`cert_table_id`,
			`cert_table_row_index`,
			`cert_table_column_index`
		)
	VALUES(
		p_cert_table_id,
		v_row,
		v_column
	);
set
v_column = v_column + 1;
set
v_td_count = v_td_count + 1;
END while;
SET
v_row = v_row + 1;
END while;

ELSEIF v_difference_cert_table_column_count < 0 THEN 

SET
v_td_count_tmp = 0;

DELETE
FROM
	cert_table_td
WHERE
	`cert_table_column_index` > p_cert_table_column_count
	AND cert_table_id = p_cert_table_id;

SELECT
	ROW_COUNT() INTO
		v_td_count_tmp;
SET
v_td_count = v_td_count - v_td_count_tmp;
END IF;



if v_difference_cert_table_row_count > 0 THEN 

SET
v_row = v_last_cert_table_row_count + 1;

while v_row <= p_cert_table_row_count do
SET
v_column = 1;

while v_column <= p_cert_table_column_count do INSERT
	INTO
		cert_table_td(
			`cert_table_id`,
			`cert_table_row_index`,
			`cert_table_column_index`
		)
	VALUES(
		p_cert_table_id,
		v_row,
		v_column
	);
set
v_column = v_column + 1;
set
v_td_count = v_td_count + 1;
END while;
set
v_row = v_row + 1;
END while;

ELSEIF v_difference_cert_table_row_count < 0 THEN 

SET
v_td_count_tmp = 0;

DELETE
FROM
	cert_table_td
WHERE
	`cert_table_row_index` > p_cert_table_row_count
	AND cert_table_id = p_cert_table_id;

SELECT
	ROW_COUNT() INTO
		v_td_count_tmp;
SET
v_td_count = v_td_count - v_td_count_tmp;
END IF;



UPDATE
	`cert_table`
SET
	`cert_table_group_id` = p_cert_table_group_id,
	`cert_table_row_count` = p_cert_table_row_count,
	`cert_table_column_count` = p_cert_table_column_count,
	`cert_table_heading` = p_cert_table_heading,
	`cert_table_order` = p_cert_table_order,
	`cert_table_row_names` = p_cert_table_row_names,
	`cert_table_column_names` = cert_table_column_names
WHERE
	`cert_table_id` = p_cert_table_id;

SELECT
	ROW_COUNT() INTO
		v_update_count;

SELECT
	CONCAT_WS( '_', v_update_count, v_td_count ) as edit_result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_get_certtabletds_with_employeecertrels` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_get_certtabletds_with_employeecertrels`(p_cert_table_group_id int, p_employee_id int)
BEGIN

SELECT ctt.*, ct.*, ctg.*, c.*, cd.*, ecr.*
FROM cert_table_td ctt
LEFT JOIN cert_table ct ON ct.cert_table_id = ctt.cert_table_id
LEFT JOIN cert_table_group ctg ON ctg.cert_table_group_id = ct.cert_table_group_id
LEFT JOIN cert c ON c.cert_id = ctt.cert_id
LEFT JOIN cert_def cd ON cd.cert_def_id = c.cert_def_id
LEFT JOIN employee_cert_rel ecr ON ecr.employee_id = p_employee_id AND ecr.employee_cert_rel_cert_id = c.cert_id
WHERE ct.cert_table_group_id = p_cert_table_group_id
ORDER BY ct.cert_table_order DESC, ct.cert_table_id ASC, ctt.cert_table_row_index ASC, ctt.cert_table_column_index ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_get_certtable_with_employeecertrels` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_get_certtable_with_employeecertrels`(p_cert_table_group_id int, p_employee_id int)
BEGIN
SELECT ctt.*, ct.*, ctg.*, c.*, cd.*, ecr.*
FROM cert_table_td ctt
LEFT JOIN cert_table ct ON ct.cert_table_id = ctt.cert_table_id
LEFT JOIN cert_table_group ctg ON ctg.cert_table_group_id = ct.cert_table_group_id
LEFT JOIN cert c ON c.cert_id = ctt.cert_id
LEFT JOIN cert_def cd ON cd.cert_def_id = c.cert_def_id
LEFT JOIN employee_cert_rel ecr ON ecr.employee_id = p_employee_id AND ecr.employee_cert_rel_cert_id = c.cert_id
WHERE ct.cert_table_group_id = p_cert_table_group_id
ORDER BY ct.cert_table_order DESC, ct.cert_table_id ASC, ctt.cert_table_row_index ASC, ctt.cert_table_column_index ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_get_certtable_with_tds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_get_certtable_with_tds`(p_cert_table_id int)
BEGIN
SELECT ctt.*, ct.*, ctg.*, c.*, cd.*
FROM cert_table_td ctt
LEFT JOIN cert_table ct ON ct.cert_table_id = ctt.cert_table_id
LEFT JOIN cert_table_group ctg ON ctg.cert_table_group_id = ct.cert_table_group_id
LEFT JOIN cert c ON c.cert_id = ctt.cert_id
LEFT JOIN cert_def cd ON cd.cert_def_id = c.cert_def_id
WHERE ctt.cert_table_id = p_cert_table_id
ORDER BY ct.cert_table_order DESC, ct.cert_table_id ASC, ctt.cert_table_row_index ASC, ctt.cert_table_column_index ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_get_cert_rels` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_get_cert_rels`(p_max_remaining_time_days INT(10))
BEGIN



SELECT 
    ecr.*, e.*, c.*, cd.*
FROM
    employee_cert_rel ecr
        LEFT JOIN
    employee e ON e.employee_id = ecr.employee_id
        LEFT JOIN
    cert c ON c.cert_id = ecr.employee_cert_rel_cert_id
        LEFT JOIN
    cert_def cd ON cd.cert_def_id = c.cert_def_id
WHERE
    employee_cert_rel_remaining_time_days <= p_max_remaining_time_days
ORDER BY employee_cert_rel_remaining_time_days ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_update_employee_cert_rel_remaining_time_days` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_update_employee_cert_rel_remaining_time_days`()
BEGIN
UPDATE employee_cert_rel 
SET 
    employee_cert_rel_remaining_time_days = DATEDIFF(DATE_ADD(FROM_UNIXTIME(date_of_issue, '%Y-%m-%d'),
                INTERVAL FUNC_GET_DURATION(employee_cert_rel_cert_id) MONTH),
            CURDATE());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-08 14:44:37
