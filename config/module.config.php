<?php

namespace Lerp\Cert;

use Bitkorn\User\Service\UserService;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Cert\Controller\Ajaxhelper\CertChainAjaxController;
use Lerp\Cert\Controller\Ajaxhelper\FormController;
use Lerp\Cert\Controller\Ajaxhelper\FormElementController;
use Lerp\Cert\Controller\Common\DashboardController;
use Lerp\Cert\Controller\Common\LoginController;
use Lerp\Cert\Controller\Manager\CertChainController;
use Lerp\Cert\Controller\Manager\CertController;
use Lerp\Cert\Controller\Manager\EmployeeController;
use Lerp\Cert\Controller\Manager\EmployeeGroupController;
use Lerp\Cert\Controller\Manager\InsertController;
use Lerp\Cert\Controller\Manager\PortableDocumentController;
use Lerp\Cert\Controller\Rest\CertRestController;
use Lerp\Cert\Controller\Rest\CertTableTdRestController;
use Lerp\Cert\Controller\Rest\EmployeeCertRelDocRestController;
use Lerp\Cert\Controller\Rest\EmployeeCertRelRestController;
use Lerp\Cert\Controller\Rest\EmployeeRestController;
use Lerp\Cert\Factory\Controller\Ajaxhelper\CertChainAjaxControllerFactory;
use Lerp\Cert\Factory\Controller\Ajaxhelper\FormControllerFactory;
use Lerp\Cert\Factory\Controller\Ajaxhelper\FormElementControllerFactory;
use Lerp\Cert\Factory\Controller\Common\DashboardControllerFactory;
use Lerp\Cert\Factory\Controller\Common\LoginControllerFactory;
use Lerp\Cert\Factory\Controller\Manager\CertChainControllerFactory;
use Lerp\Cert\Factory\Controller\Manager\CertControllerFactory;
use Lerp\Cert\Factory\Controller\Manager\EmployeeControllerFactory;
use Lerp\Cert\Factory\Controller\Manager\EmployeeGroupControllerFactory;
use Lerp\Cert\Factory\Controller\Manager\InsertControllerFactory;
use Lerp\Cert\Factory\Controller\Manager\PortableDocumentControllerFactory;
use Lerp\Cert\Factory\Controller\Rest\CertTableTdRestControllerFactory;
use Lerp\Cert\Factory\Controller\Rest\EmployeeCertRelDocRestControllerFactory;
use Lerp\Cert\Factory\Controller\Rest\EmployeeCertRelRestControllerFactory;
use Lerp\Cert\Factory\Controller\Rest\EmployeeRestControllerFactory;
use Lerp\Cert\Factory\Form\Cert\CertFormFactory;
use Lerp\Cert\Factory\Form\Cert\CertGroupFormFactory;
use Lerp\Cert\Factory\Form\Cert\CertTableFormFactory;
use Lerp\Cert\Factory\Form\Cert\CertTableGroupFormFactory;
use Lerp\Cert\Factory\Form\Cert\Chain\CertChainFormFactory;
use Lerp\Cert\Factory\Form\Employee\EmployeeCertRelFormFactory;
use Lerp\Cert\Factory\Form\Employee\EmployeeFormFactory;
use Lerp\Cert\Factory\Listener\EmployeeCertRelListenerFactory;
use Lerp\Cert\Factory\Observer\Observer\Concrete\EmployeeCertRelAddObserverFactory;
use Lerp\Cert\Factory\Service\UserServiceFactory;
use Lerp\Cert\Factory\Table\Cert\CertDefTableFactory;
use Lerp\Cert\Factory\Table\Cert\CertGroupTableFactory;
use Lerp\Cert\Factory\Table\Cert\CertTableFactory;
use Lerp\Cert\Factory\Table\Cert\CertTableGroupTableFactory;
use Lerp\Cert\Factory\Table\Cert\Chain\CertChainItemTableFactory;
use Lerp\Cert\Factory\Table\Cert\Chain\CertChainTableFactory;
use Lerp\Cert\Factory\Table\LerpCertTableFactory;
use Lerp\Cert\Factory\View\Helper\Cert\CertDocUrlViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\Document\CertsTableGroupViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\Document\CertTableGroupAttributesViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\Document\EmployeeCertsAllViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\Element\Cert\CertSelectFactory;
use Lerp\Cert\Factory\View\Helper\Element\Cert\CertTableGroupsUrlDropdownFactory;
use Lerp\Cert\Factory\View\Helper\Element\Employee\EmployeeCertRelCertDefValueElementFactory;
use Lerp\Cert\Factory\View\Helper\Employee\EmployeeCertRelViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\IdAssoc\IdCertDefViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\IdAssoc\IdCertGroupViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\IdAssoc\IdCertInfoViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\Table\CertTableTdViewHelperFactory;
use Lerp\Cert\Factory\View\Helper\Table\CertTableViewHelperFactory;
use Lerp\Cert\Form\Cert\CertForm;
use Lerp\Cert\Form\Cert\CertGroupForm;
use Lerp\Cert\Form\Cert\CertTableForm;
use Lerp\Cert\Form\Cert\CertTableGroupForm;
use Lerp\Cert\Form\Cert\Chain\CertChainForm;
use Lerp\Cert\Form\Employee\EmployeeCertRelForm;
use Lerp\Cert\Form\Employee\EmployeeForm;
use Lerp\Cert\Listener\EmployeeCertRelListener;
use Lerp\Cert\Observer\Observer\Concrete\EmployeeCertRelAddObserver;
use Lerp\Cert\Table\Cert\CertDefTable;
use Lerp\Cert\Table\Cert\CertGroupTable;
use Lerp\Cert\Table\Cert\CertTable;
use Lerp\Cert\Table\Cert\CertTableGroupTable;
use Lerp\Cert\Table\Cert\CertTableTable;
use Lerp\Cert\Table\Cert\CertTableTdTable;
use Lerp\Cert\Table\Cert\Chain\CertChainItemTable;
use Lerp\Cert\Table\Cert\Chain\CertChainTable;
use Lerp\Cert\Table\Common\ConfigTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelArchiveTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelDocTable;
use Lerp\Cert\Table\Employee\EmployeeCertRelTable;
use Lerp\Cert\Table\Employee\EmployeeCertTableGroupAttributeDefTable;
use Lerp\Cert\Table\Employee\EmployeeCertTableGroupAttributeRelTable;
use Lerp\Cert\Table\Employee\EmployeeGroupCertTaskTable;
use Lerp\Cert\Table\Employee\EmployeeGroupRelTable;
use Lerp\Cert\Table\Employee\EmployeeGroupTable;
use Lerp\Cert\Table\Employee\EmployeeTable;
use Lerp\Cert\Tablex\Cert\CertTableTablex;
use Lerp\Cert\Tablex\Cert\CertTablex;
use Lerp\Cert\Tablex\Employee\EmployeeCertTableGroupAttributeTablex;
use Lerp\Cert\Tablex\Employee\EmployeeGroupTablex;
use Lerp\Cert\Tablex\Employee\EmployeeTablex;
use Lerp\Cert\View\Helper\Cert\CertDocUrlViewHelper;
use Lerp\Cert\View\Helper\Document\CertsTableGroupViewHelper;
use Lerp\Cert\View\Helper\Document\CertTableGroupAttributesViewHelper;
use Lerp\Cert\View\Helper\Document\EmployeeCertsAllViewHelper;
use Lerp\Cert\View\Helper\Element\Cert\CertSelect;
use Lerp\Cert\View\Helper\Element\Cert\CertTableGroupsUrlDropdown;
use Lerp\Cert\View\Helper\Element\Employee\EmployeeCertRelCertDefValueElement;
use Lerp\Cert\View\Helper\Employee\EmployeeCertRelViewHelper;
use Lerp\Cert\View\Helper\IdAssoc\IdCertDefViewHelper;
use Lerp\Cert\View\Helper\IdAssoc\IdCertGroupViewHelper;
use Lerp\Cert\View\Helper\IdAssoc\IdCertInfoViewHelper;
use Lerp\Cert\View\Helper\IdAssoc\Select\SimpleSelectViewHelper;
use Lerp\Cert\View\Helper\Table\CertTableTdViewHelper;
use Lerp\Cert\View\Helper\Table\CertTableViewHelper;

return [
    'router'          => [
        'routes' => [
            'lerp_cert_login'                                            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => Controller\Common\LoginController::class,
                        'action'     => 'login',
                    ],
                ],
            ],
            'lerp_cert_dashboard'                                        => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/dashboard',
                    'defaults' => [
                        'controller' => Controller\Common\DashboardController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            /**
             * Cert
             */
            'lerp_cert_manage_cert'                                      => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/manage-cert[/:cert_id][/]',
                    'constraints' => [
                        'cert_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'cert',
                    ],
                ],
            ],
            'lerp_cert_manage_cert_copy_cert'                            => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/copy-cert[/:cert_id][/]',
                    'constraints' => [
                        'cert_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'copyCert',
                    ],
                ],
            ],
            'lerp_cert_manage_cert_table_tds'                            => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/manage-cert-table-tds[/:cert_table_id][/]',
                    'constraints' => [
                        'cert_table_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certTableTds',
                    ],
                ],
            ],
            'lerp_cert_manage_cert_table'                                => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/manage-cert-table[/:cert_table_id][/]',
                    'constraints' => [
                        'cert_table_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certTable',
                    ],
                ],
            ],
            'lerp_cert_show_cert_table_groups'                           => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/show-cert-table-groups[/]',
                    'defaults' => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certTableGroups',
                    ],
                ],
            ],
            'lerp_cert_manage_cert_table_group'                          => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/manage-cert-table-group[/:cert_table_group_id][/]',
                    'constraints' => [
                        'cert_table_group_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certTableGroup',
                    ],
                ],
            ],
            'lerp_cert_cert_certs'                                       => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/certs',
                    'defaults' => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certs',
                    ],
                ],
            ],
            'lerp_cert_show_cert_tables'                                 => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/show-cert-tables[/]',
                    'defaults' => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certTables',
                    ],
                ],
            ],
            'lerp_cert_show_certgroups'                                  => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/show-certgroups',
                    'defaults' => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certGroups',
                    ],
                ],
            ],
            'lerp_cert_insert_certgroup'                                 => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/insert-certgroup',
                    'defaults' => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certGroup',
                    ],
                ],
            ],
            'lerp_cert_show_certdefs'                                    => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/show-certdefs',
                    'defaults' => [
                        'controller' => Controller\Manager\CertController::class,
                        'action'     => 'certDefs',
                    ],
                ],
            ],
            /**
             * CertChain
             */
            'lerp_cert_certchains_list'                                  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/list-certchains',
                    'defaults' => [
                        'controller' => Controller\Manager\CertChainController::class,
                        'action'     => 'certChains',
                    ],
                ],
            ],
            'lerp_cert_certchain_manage'                                 => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/manage-certchain[/:cert_chain_id]',
                    'constraints' => [
                        'cert_chain_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\CertChainController::class,
                        'action'     => 'certChain',
                    ],
                ],
            ],
            /*
             * INSERT
             */
            'lerp_cert_insert_cert'                                      => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/insert-cert',
                    'defaults' => [
                        'controller' => Controller\Manager\InsertController::class,
                        'action'     => 'cert',
                    ],
                ],
            ],
            'lerp_cert_insert_employee'                                  => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/insert-employee',
                    'defaults' => [
                        'controller' => Controller\Manager\InsertController::class,
                        'action'     => 'employee',
                    ],
                ],
            ],
            'lerp_cert_insert_employee_cert_rel'                         => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/insert-employee-cert-rel[/:employee_id][/]',
                    'constraints' => [
                        'employee_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\InsertController::class,
                        'action'     => 'employeeCertRel',
                    ],
                ],
            ],
            'lerp_cert_insert_cert_table'                                => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/insert-cert-table[/]',
                    'defaults' => [
                        'controller' => Controller\Manager\InsertController::class,
                        'action'     => 'certTable',
                    ],
                ],
            ],
            'lerp_cert_insert_cert_table_group'                          => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/insert-cert-table-group[/]',
                    'defaults' => [
                        'controller' => Controller\Manager\InsertController::class,
                        'action'     => 'certTableGroup',
                    ],
                ],
            ],
            /*
             * Employee
             */
            'lerp_cert_employee_employees'                               => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/show-employees',
                    'defaults' => [
                        'controller' => Controller\Manager\EmployeeController::class,
                        'action'     => 'employees',
                    ],
                ],
            ],
            'lerp_cert_manage_employee'                                  => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/manage-employee[/:employee_id][/]',
                    'constraints' => [
                        'employee_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\EmployeeController::class,
                        'action'     => 'employee',
                    ],
                ],
            ],
            'lerp_cert_manage_employee_certs'                            => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/manage-employee-certs[/:employee_id][/]',
                    'constraints' => [
                        'employee_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\EmployeeController::class,
                        'action'     => 'employeeCertsAll',
                    ],
                ],
            ],
            'lerp_cert_show_employee_certs_table_group'                  => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/show-employee-certs-table-group/:cert_table_group_id/:employee_id[/]',
                    'constraints' => [
                        'cert_table_group_id' => '[0-9]*',
                        'employee_id'         => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\EmployeeController::class,
                        'action'     => 'employeeCertsTableGroup',
                    ],
                ],
            ],
            /*
             * EmployeeGroup
             */
            'lerp_cert_manage_employeegroup_employeegroupcerttasks'      => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/employeegroupcerttasks',
                    'defaults' => [
                        'controller' => Controller\Manager\EmployeeGroupController::class,
                        'action'     => 'employeeGroupCertTasks',
                    ],
                ],
            ],
            'lerp_cert_manage_employeegroup_employeegroups'              => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/employeegroups',
                    'defaults' => [
                        'controller' => Controller\Manager\EmployeeGroupController::class,
                        'action'     => 'employeeGroups',
                    ],
                ],
            ],
            'lerp_cert_manage_employeegroup_employeegroup'               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/employeegroup[/:employee_group_id]',
                    'constraints' => [
                        'employee_group_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\EmployeeGroupController::class,
                        'action'     => 'employeeGroup',
                    ],
                ],
            ],
            /*
             * REST
             */
            'lerp_cert_rest_cert'                                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/rest-cert[/:id]',
                    'constraints' => [
                        'id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\CertRestController::class,
                    ],
                ],
            ],
            'lerp_cert_rest_employee'                                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/rest-employee[/:id]',
                    'constraints' => [
                        'id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\EmployeeRestController::class,
                    ],
                ],
            ],
            'lerp_cert_rest_employee_cert_rel'                           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/rest-employee-cert-rel[/:id]',
                    'constraints' => [
                        'id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\EmployeeCertRelRestController::class,
                    ],
                ],
            ],
            'lerp_cert_rest_employee_cert_rel_doc'                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/rest-employee-cert-rel-doc[/:id]',
                    'constraints' => [
                        'id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\EmployeeCertRelDocRestController::class,
                    ],
                ],
            ],
            'lerp_cert_rest_cert_table_td'                               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/rest-cert-table-td[/:id]',
                    'constraints' => [
                        'id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Rest\CertTableTdRestController::class,
                    ],
                ],
            ],
            /*
             * AJAX Helper
             */
            'lerp_cert_ajaxhelper_formelement_certrelcertdefvalueselect' => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/cert-rel-cert-def-value-select[/:cert_id][/]',
                    'constraints' => [
                        'cert_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Ajaxhelper\FormElementController::class,
                        'action'     => 'certRelCertDefValueSelect',
                    ],
                ],
            ],
            'lerp_cert_ajaxhelper_form_employeecertrelform_add'          => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/employee-cert-rel-form-add[/:cert_id][/]',
                    'constraints' => [
                        'cert_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Ajaxhelper\FormController::class,
                        'action'     => 'employeeCertRelFormAdd',
                    ],
                ],
            ],
            'lerp_cert_ajaxhelper_form_employeecertrelform_edit'         => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/employee-cert-rel-form-edit[/:employee_cert_rel_id][/]',
                    'constraints' => [
                        'employee_cert_rel_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Ajaxhelper\FormController::class,
                        'action'     => 'employeeCertRelFormEdit',
                    ],
                ],
            ],
            'lerp_cert_ajaxhelper_form_employeecertrelform_delete'       => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/employee-cert-rel-form-delete[/:employee_cert_rel_id][/]',
                    'constraints' => [
                        'employee_cert_rel_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Ajaxhelper\FormController::class,
                        'action'     => 'employeeCertRelFormDelete',
                    ],
                ],
            ],
            'lerp_cert_ajaxhelper_form_employeecertrelform_documents'    => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/employee-cert-rel-form-documents[/:employee_cert_rel_id][/]',
                    'constraints' => [
                        'employee_cert_rel_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Ajaxhelper\FormController::class,
                        'action'     => 'employeeCertRelFormDocuments',
                    ],
                ],
            ],
            'lerp_cert_ajaxhelper_form_employeecertrelupload_document'   => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/employee-cert-rel-upload-document[/:employee_cert_rel_id][/]',
                    'constraints' => [
                        'employee_cert_rel_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Ajaxhelper\FormController::class,
                        'action'     => 'employeeCertRelUploadDocument',
                    ],
                ],
            ],
            'lerp_cert_ajaxhelper_form_employeegrouprels_add'            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/form_employeegrouprels-add[/:employee_group_id]',
                    'constraints' => [
                        'employee_group_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Ajaxhelper\FormController::class,
                        'action'     => 'employeeGroupRelFormAdd',
                    ],
                ],
            ],
            'lerp_cert_ajaxhelper_certchain_certchainitemsform'          => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/certchainitemsform[/:cert_chain_id][/]',
                    'constraints' => [
                        'cert_chain_id' => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Ajaxhelper\CertChainAjaxController::class,
                        'action'     => 'certChainItemsForm',
                    ],
                ],
            ],
            /*
             * PDF
             */
            'lerp_cert_manager_pdf_table_group'                          => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/pdf-table-group[/:employee_id/:cert_table_group_id][/]',
                    'constraints' => [
                        'cert_table_group_id' => '[0-9]*',
                        'employee_id'         => '[0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Manager\PortableDocumentController::class,
                        'action'     => 'tableGroup',
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            CertChainAjaxController::class          => CertChainAjaxControllerFactory::class,
            FormController::class                   => FormControllerFactory::class,
            FormElementController::class            => FormElementControllerFactory::class,
            DashboardController::class              => DashboardControllerFactory::class,
            LoginController::class                  => LoginControllerFactory::class,
            CertChainController::class              => CertChainControllerFactory::class,
            CertController::class                   => CertControllerFactory::class,
            EmployeeController::class               => EmployeeControllerFactory::class,
            EmployeeGroupController::class          => EmployeeGroupControllerFactory::class,
            InsertController::class                 => InsertControllerFactory::class,
            PortableDocumentController::class       => PortableDocumentControllerFactory::class,
            // REST
            CertRestController::class               => CertControllerFactory::class,
            CertTableTdRestController::class        => CertTableTdRestControllerFactory::class,
            EmployeeCertRelDocRestController::class => EmployeeCertRelDocRestControllerFactory::class,
            EmployeeCertRelRestController::class    => EmployeeCertRelRestControllerFactory::class,
            EmployeeRestController::class           => EmployeeRestControllerFactory::class,
        ],
        'invokables' => [
        ],
    ],
    'service_manager' => [
        'factories'  => [
            // form
            CertChainForm::class                           => CertChainFormFactory::class,
            CertForm::class                                => CertFormFactory::class,
            CertGroupForm::class                           => CertGroupFormFactory::class,
            CertTableForm::class                           => CertTableFormFactory::class,
            CertTableGroupForm::class                      => CertTableGroupFormFactory::class,
            EmployeeCertRelForm::class                     => EmployeeCertRelFormFactory::class,
            EmployeeForm::class                            => EmployeeFormFactory::class,
            // Table
            CertChainItemTable::class                      => CertChainItemTableFactory::class,
            CertChainTable::class                          => CertChainTableFactory::class,
            CertDefTable::class                            => CertDefTableFactory::class,
            CertGroupTable::class                          => CertGroupTableFactory::class,
            CertTable::class                               => CertTableFactory::class,
            CertTableGroupTable::class                     => CertTableGroupTableFactory::class,
            CertTableTable::class                          => LerpCertTableFactory::class,
            CertTableTdTable::class                        => LerpCertTableFactory::class,
            ConfigTable::class                             => LerpCertTableFactory::class,
            EmployeeCertRelArchiveTable::class             => LerpCertTableFactory::class,
            EmployeeCertRelDocTable::class                 => LerpCertTableFactory::class,
            EmployeeCertRelTable::class                    => LerpCertTableFactory::class,
            EmployeeCertTableGroupAttributeDefTable::class => LerpCertTableFactory::class,
            EmployeeCertTableGroupAttributeRelTable::class => LerpCertTableFactory::class,
            EmployeeGroupCertTaskTable::class              => LerpCertTableFactory::class,
            EmployeeGroupRelTable::class                   => LerpCertTableFactory::class,
            EmployeeGroupTable::class                      => LerpCertTableFactory::class,
            EmployeeTable::class                           => LerpCertTableFactory::class,
            // tablex
            CertTableTablex::class                         => LerpCertTableFactory::class,
            CertTablex::class                              => LerpCertTableFactory::class,
            EmployeeCertTableGroupAttributeTablex::class   => LerpCertTableFactory::class,
            EmployeeGroupTablex::class                     => LerpCertTableFactory::class,
            EmployeeTablex::class                          => LerpCertTableFactory::class,
            // listener
            EmployeeCertRelListener::class                 => EmployeeCertRelListenerFactory::class,
            // observer
            EmployeeCertRelAddObserver::class              => EmployeeCertRelAddObserverFactory::class,
            // overwrite
            UserService::class                             => UserServiceFactory::class,
        ],
        'invokables' => [
        ],
    ],
    'view_helpers'    => [
        'factories'  => [
            CertDocUrlViewHelper::class               => CertDocUrlViewHelperFactory::class,
            CertsTableGroupViewHelper::class          => CertsTableGroupViewHelperFactory::class,
            CertTableGroupAttributesViewHelper::class => CertTableGroupAttributesViewHelperFactory::class,
            EmployeeCertsAllViewHelper::class         => EmployeeCertsAllViewHelperFactory::class,
            EmployeeCertRelViewHelper::class          => EmployeeCertRelViewHelperFactory::class,
            IdCertDefViewHelper::class                => IdCertDefViewHelperFactory::class,
            IdCertGroupViewHelper::class              => IdCertGroupViewHelperFactory::class,
            IdCertInfoViewHelper::class               => IdCertInfoViewHelperFactory::class,
            CertTableTdViewHelper::class              => CertTableTdViewHelperFactory::class,
            CertTableViewHelper::class                => CertTableViewHelperFactory::class,
            CertSelect::class                         => CertSelectFactory::class,
            CertTableGroupsUrlDropdown::class         => CertTableGroupsUrlDropdownFactory::class,
            EmployeeCertRelCertDefValueElement::class => EmployeeCertRelCertDefValueElementFactory::class,
        ],
        'invokables' => [
            'simpleSelect'             => SimpleSelectViewHelper::class,
            EmployeeCertRelForm::class => EmployeeCertRelForm::class,
        ],
        'aliases'    => [
            'certDocUrl'                  => CertDocUrlViewHelper::class,
            'certsTableGroup'             => CertsTableGroupViewHelper::class,
            'certTableGroupAttributes'    => CertTableGroupAttributesViewHelper::class,
            'employeeCertsAll'            => EmployeeCertsAllViewHelper::class,
            'employeeCertRel'             => EmployeeCertRelViewHelper::class,
            'idCertDef'                   => IdCertDefViewHelper::class,
            'idCertGroup'                 => IdCertGroupViewHelper::class,
            'idCertInfo'                  => IdCertInfoViewHelper::class,
            'certTableTd'                 => CertTableTdViewHelper::class,
            'certTable'                   => CertTableViewHelper::class,
            'certSelect'                  => CertSelect::class,
            'certTableGroupsUrlDropdown'  => CertTableGroupsUrlDropdown::class,
            'employeeCertDefValueElement' => EmployeeCertRelCertDefValueElement::class,
        ],
    ],
    'view_manager'    => [
        'template_map'        => [
            'document/employeeCerts'                      => __DIR__ . '/../view/lerp/cert/template/document/employeeCerts.phtml',
            'document/certsTableGroup'                    => __DIR__ . '/../view/lerp/cert/template/document/certsTableGroup.phtml',
            'document/certTableGroupAttributes'           => __DIR__ . '/../view/lerp/cert/template/document/certTableGroupAttributes.phtml',
            'employee/employeeCertRel'                    => __DIR__ . '/../view/lerp/cert/template/employee/employeeCertRel.phtml',
            'employee/employeeCertRelFormCertDefDefValue' => __DIR__ . '/../view/lerp/cert/template/employee/employeeCertRelFormCertDefDefValue.phtml',
            'table/certTable'                             => __DIR__ . '/../view/lerp/cert/template/table/certTable.phtml',
            'table/certTableTd'                           => __DIR__ . '/../view/lerp/cert/template/table/certTableTd.phtml',
            'cert/certSelect'                             => __DIR__ . '/../view/lerp/cert/template/cert/certSelect.phtml',
            'cert/certTableGroupsUrlDropdown'             => __DIR__ . '/../view/lerp/cert/template/cert/certTableGroupsUrlDropdown.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'lerp_cert'       => [
        'doc_path_web'          => '/documents', // make it writable
        'doc_path_absolute'     => __DIR__ . '/../../../public/documents', // make it writable
        'tmp_doc_path_absolute' => __DIR__ . '/../../../public/tmp', // make it writable
        'fqn_javajar_pdf'       => __DIR__ . '/../../../bin/GFMStaffPdf.jar', // make it executable
    ],
];
